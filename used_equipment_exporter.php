<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$autoloader = require_once 'autoload.php';

$kernel = new DrupalKernel('prod', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
$length = 2;
$start = $length * $page;
$query = \Drupal::entityQuery('products')
  ->condition('type', 'used_equipment')
  ->range($start, $length);
$entity_ids = $query->execute();

$outputs = [];
foreach ($entity_ids as $id) {
  $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
  $entity_arr = $entity->toArray();
  $output = [];
  
  $field_fork_lift_specification = $entity_arr['field_fork_lift_specification'];
  $fork_lift_specification = [];
  
  $field_listofimages = $entity_arr['field_listofimages'];
  $listofimages = [];
  foreach ($field_listofimages as $value) {
    $target_id = $value['target_id'];
    $file = file_load($target_id);
    $listofimages[] = file_create_url($file->uri->value);
  }

  $field_group = $entity_arr['field_group'][0]['target_id'];
  $group = taxonomy_term_load($field_group);

  $output = [
    'type' => $entity_arr['type'][0]['target_id'],
    'name' => $entity_arr['name'][0]['value'],
    'field_description' => $entity_arr['field_description'][0]['value'],
    'field_location' => $entity_arr['field_location'][0]['value'],
    'field_price' => $entity_arr['field_price'][0]['value'],
    'field_serial_number' => $entity_arr['field_serial_number'][0]['value'],
    'field_smu' => $entity_arr['field_smu'][0]['value'],
    'field_updated' => $entity_arr['field_updated'][0]['value'],
    'field_warranty' => $entity_arr['field_warranty'][0]['value'],
    'field_year' => $entity_arr['field_year'][0]['value'],
    'field_listofimages' => $listofimages,
    'field_group' => $group->name->value,
  ];
  $outputs[] = $output;
}
header('Content-Type: application/json');

$return = json_encode($outputs);
print $return;
exit();