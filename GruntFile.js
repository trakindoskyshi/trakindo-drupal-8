module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    autoprefixer: {
      options: {
        // We need to `freeze` browsers versions for testing purposes.
        browsers: ['opera 12', 'ff 15', 'chrome 25']
      },
      dist: { // Target
        files: {
          './themes/custom/trakindo_2016/css/style.css': './themes/custom/trakindo_2016/css/style.css'
        }
      }
    },
    watch: {
      sass: {
        files: './themes/custom/trakindo_2016/sass/**/*.scss',
        tasks: ['sass', 'concat:styles', 'autoprefixer']
      },
      livereload: {
        files: ['*.html', '*.php', './js/**/*.js', './themes/custom/trakindo_2016/*.css','./themes/custom/trakindo_2016/images/**/*.{png,jpg,jpeg,gif,webp,svg}'],
        options: {
          livereload: true
        }
      }
    },
    concat: {
      options: {
        separator: "\n\n"
      },
      styles: {
        src: './themes/custom/trakindo_2016/css/style.css',
        dest: './themes/custom/trakindo_2016/css/style.css'
      }
    },
    sass: {
      dist: {
        files: {
          './themes/custom/trakindo_2016/css/style.css' : './themes/custom/trakindo_2016/sass/style.scss'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-livereload');
  grunt.loadNpmTasks('grunt-autoprefixer');

  //tasks
  grunt.registerTask('default', ['concat','sass']);
  grunt.registerTask('autoPrefixer', ['autoprefixer']);
}