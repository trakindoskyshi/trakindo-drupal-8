<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\product_importer\Service\ProductService;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\field_collection\Plugin\Field\FieldType\FieldCollection;

$autoloader = require_once 'autoload.php';

$kernel = new DrupalKernel('prod', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$page = (isset($_GET['page'])) ? $_GET['page'] : 0;

$import_url = 'http://dev.trakindo.labs.skyshi.com/used_equipment_exporter.php?page='. $page;

try {
  $response2 = \Drupal::httpClient()->get($import_url, array('headers' => array('Accept' => 'application/json')));
  $body = (string) $response2->getBody();
  $body_obj = json_decode($body);
  if (count($body_obj)) {
    $host = \Drupal::request()->getHost();
    $page++;
    $redirect_url = "http://$host/used_equipment_importer.php?page=$page";
    print_r($body);
    print '<meta http-equiv="refresh" content="0; url='. $redirect_url. '" />';
  } else {
    print 'done';
  }
  
  foreach ($body_obj as $data) {
    $category = $data->field_group;
    $group_vid = 'group';
    $category_tid = ProductService::saveTerm($group_vid, $category);

    $listofimages = [];
    foreach ($data->field_listofimages as $url) {
      $file = ProductService::saveImage($url, 'public://product_images');
      $listofimages[] = $file->id();
    }
    

    $product_data = [
      'type' => $data->type,
      'name' => $data->name,
      'field_description' => [
        'value' => $data->field_description,
        'format' => 'full_html'
      ],
      'field_location' => $data->field_location,
      'field_price' => $data->field_price,
      'field_serial_number' => $data->field_serial_number,
      'field_smu' => $data->field_smu,
      'field_updated' => $data->field_updated,
      'field_warranty' => $data->field_warranty,
      'field_year' => $data->field_year,
      'field_listofimages' => $listofimages,
      'field_group' => $category_tid,
    ];
    $query = \Drupal::entityQuery('products')
      ->condition('type', 'used_equipment')
      ->condition('name', $product_data['name']);
    $entity_ids = $query->execute();
    if ($entity_ids) {
      $id = reset($entity_ids);
      $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
      foreach ($product_data as $field => $value) {
        if ($field == 'type') continue;
        $entity->{$field} = $value;
      }
    } else {
      $entity = \Drupal::entityTypeManager()->getStorage('products')->create($product_data);
    }
    $entity->save();
  }
}
catch (RequestException $e) {
  return FALSE;
}

exit();