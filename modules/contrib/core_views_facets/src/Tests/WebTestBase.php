<?php

namespace Drupal\core_views_facets\Tests;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Entity\Server;
use Drupal\simpletest\WebTestBase as SimpletestWebTestBase;
use Drupal\facets\Tests\BlockTestTrait;
use Drupal\facets\Tests\TestHelperTrait;
use Drupal\Core\Url;

/**
 * Provides the base class for web tests for Search API.
 */
abstract class WebTestBase extends SimpletestWebTestBase {

  use StringTranslationTrait;
  use TestHelperTrait;
  use BlockTestTrait {
    createFacet as traitCreateFacet;
  }

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  public static $modules = [
    'views',
    'node',
    'field',
    'search_api',
    'facets',
    'block',
    'facets_search_api_dependency',
    'taxonomy',
    'core_views_facets',
    'user',
    'core_views_facets_test_views',
  ];

  /**
   * An admin user used for this test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * A user without Search / Facet admin permission.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $unauthorizedUser;

  /**
   * The anonymous user used for this test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $anonymousUser;

  /**
   * A search index ID.
   *
   * @var string
   */
  protected $indexId = 'database_search_index';

  /**
   * The generated test entities, keyed by ID.
   *
   * @var \Drupal\entity_test\Entity\EntityTestMulRevChanged[]
   */
  protected $entities = [];

  /**
   * The exposed filters facet source ID.
   *
   * @var string
   */
  protected $exposedFiltersFacetSourceId;

  /**
   * The contextual filters facet source ID.
   *
   * @var string
   */
  protected $contextualFiltersFacetSourceId;

  /**
   * The CoreViewIntegrationTest view path.
   *
   * @var string
   */
  protected $facetUrl;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create the users used for the tests.
    $this->adminUser = $this->drupalCreateUser([
      'administer search_api',
      'administer facets',
      'access administration pages',
      'administer nodes',
      'access content overview',
      'administer content types',
      'administer blocks',
    ]);

    $this->unauthorizedUser = $this->drupalCreateUser(['access administration pages']);
    $this->anonymousUser = $this->drupalCreateUser();
    $this->exposedFiltersFacetSourceId = 'core_views_exposed_filter:core_views_facets_basic_integration__page_1';
    $this->contextualFiltersFacetSourceId = 'core_views_contextual_filter:core_views_facets_basic_integration__page_1';
    $this->facetUrl = 'core-views-facets-basic-integration';
  }

  /**
   * Creates several test entities.
   */
  protected function insertExampleContent() {
    $count = \Drupal::entityQuery('entity_test')
      ->count()
      ->execute();

    $entity_test_storage = \Drupal::entityTypeManager()
      ->getStorage('entity_test');
    $this->entities[1] = $entity_test_storage->create(array(
      'name' => 'foo bar baz',
      'body' => 'test test',
      'type' => 'item',
      'keywords' => array('orange'),
      'category' => 'item_category',
    ));
    $this->entities[1]->save();
    $this->entities[2] = $entity_test_storage->create(array(
      'name' => 'foo test',
      'body' => 'bar test',
      'type' => 'item',
      'keywords' => array('orange', 'apple', 'grape'),
      'category' => 'item_category',
    ));
    $this->entities[2]->save();
    $this->entities[3] = $entity_test_storage->create(array(
      'name' => 'bar',
      'body' => 'test foobar',
      'type' => 'item',
    ));
    $this->entities[3]->save();
    $this->entities[4] = $entity_test_storage->create(array(
      'name' => 'foo baz',
      'body' => 'test test test',
      'type' => 'article',
      'keywords' => array('apple', 'strawberry', 'grape'),
      'category' => 'article_category',
    ));
    $this->entities[4]->save();
    $this->entities[5] = $entity_test_storage->create(array(
      'name' => 'bar baz',
      'body' => 'foo',
      'type' => 'article',
      'keywords' => array('orange', 'strawberry', 'grape', 'banana'),
      'category' => 'article_category',
    ));
    $this->entities[5]->save();
    $count = \Drupal::entityQuery('entity_test')
        ->count()
        ->execute() - $count;
    $this->assertEqual($count, 5, "$count items inserted.");
  }

  /**
   * Add a facet trough the UI.
   *
   * @param string $name
   *   The facet name.
   * @param string $id
   *   The facet id.
   * @param string $field
   *   The facet field.
   * @param string $display_id
   *   The display id.
   * @param string $source
   *   Facet source.
   * @param string $source_type
   *   Either exposed or contextual.
   */
  protected function createFacet($name, $id, $field = 'type', $display_id = 'page_1', $source = 'core_views_facets_basic_integration', $source_type = 'exposed') {
    switch ($source_type) {
      case 'contextual':
        list($facet_source_id) = explode(':', $this->contextualFiltersFacetSourceId);
        break;

      case 'exposed':
      default:
        list($facet_source_id) = explode(':', $this->exposedFiltersFacetSourceId);
        break;
    }
    $facet_source = "{$facet_source_id}:{$source}__{$display_id}";

    $facet_source_edit_page = Url::fromRoute('entity.facets_facet_source.edit_form', [
      'source_id' => $facet_source,
    ])->toString();
    $this->drupalGet($facet_source_edit_page);
    $this->assertResponse(200);

    $url_processor_form_values = [
      'url_processor' => 'core_views_url_processor',
    ];
    $this->drupalPostForm($facet_source_edit_page, $url_processor_form_values, $this->t('Save'));

    $facet_add_page = Url::fromRoute('entity.facets_facet.add_form')->toString();
    $this->drupalGet($facet_add_page);
    $form_values = [
      'id' => $id,
      'name' => $name,
      'facet_source_id' => $facet_source,
      "facet_source_configs[{$facet_source_id}:{$source}__{$display_id}][field_identifier]" => $field,
    ];
    $this->drupalPostForm(NULL, ['facet_source_id' => $facet_source], $this->t('Configure facet source'));
    $this->drupalPostForm(NULL, $form_values, $this->t('Save'));
    $this->blocks[$id] = $this->createBlock($id);
  }

  /**
   * Creates or deletes a server.
   *
   * @param string $name
   *   (optional) The name of the server.
   * @param string $id
   *   (optional) The ID of the server.
   * @param string $backend_id
   *   (optional) The ID of the backend to set for the server.
   * @param array $backend_config
   *   (optional) The backend configuration to set for the server.
   * @param bool $reset
   *   (optional) If TRUE, delete the server instead of creating it. (Only the
   *   server's ID is required in that case).
   *
   * @return \Drupal\search_api\ServerInterface
   *   A search server.
   */
  public function getTestServer($name = 'WebTest server', $id = 'webtest_server', $backend_id = 'search_api_test_backend', $backend_config = array(), $reset = FALSE) {
    if ($reset) {
      $server = Server::load($id);
      if ($server) {
        $server->delete();
      }
    }
    else {
      $server = Server::create(array(
        'id' => $id,
        'name' => $name,
        'description' => $name,
        'backend' => $backend_id,
        'backend_config' => $backend_config,
      ));
      $server->save();
    }

    return $server;
  }

  /**
   * Creates or deletes an index.
   *
   * @param string $name
   *   (optional) The name of the index.
   * @param string $id
   *   (optional) The ID of the index.
   * @param string $server_id
   *   (optional) The server to which the index should be attached.
   * @param string $datasource_id
   *   (optional) The ID of a datasource to set for this index.
   * @param bool $reset
   *   (optional) If TRUE, delete the index instead of creating it. (Only the
   *   index's ID is required in that case).
   *
   * @return \Drupal\search_api\IndexInterface
   *   A search index.
   */
  public function getTestIndex($name = 'WebTest Index', $id = 'webtest_index', $server_id = 'webtest_server', $datasource_id = 'entity:node', $reset = FALSE) {
    if ($reset) {
      $index = Index::load($id);
      if ($index) {
        $index->delete();
      }
    }
    else {
      $index = Index::create(array(
        'id' => $id,
        'name' => $name,
        'description' => $name,
        'server' => $server_id,
        'datasources' => array($datasource_id),
      ));
      $index->save();
      $this->indexId = $index->id();
    }

    return $index;
  }

  /**
   * Retrieves the search index used by this test.
   *
   * @return \Drupal\search_api\IndexInterface
   *   The search index.
   */
  protected function getIndex() {
    return Index::load($this->indexId);
  }

  /**
   * Clears the test index.
   */
  protected function clearIndex() {
    $this->getIndex()->clear();
  }

}
