<?php

namespace Drupal\core_views_facets\Plugin\facets\core_views_facets\exposed_filter_type;

use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeBase;
use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeInterface;
use Drupal\facets\FacetInterface;
use Drupal\taxonomy\entity\Term as TaxonomyTerm;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter type "Term" for core_views_facets.
 *
 * @CoreViewsFacetsExposedFilterType(
 *   id = "taxonomy_index_tid",
 *   label = "Taxonomy Terms"
 * )
 */
class Term extends CoreViewsFacetsExposedFilterTypeBase implements CoreViewsFacetsExposedFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, FilterPluginBase $filter, FacetInterface $facet) {
    $result = parent::processDatabaseRow($row, $filter, $facet);

    $facet_configs = $facet->getFacetConfigs();
    if (!empty($facet_configs['override_label'])) {
      $label = TaxonomyTerm::load($result->getRawValue())->label();
      $result->setDisplayValue($label);
    }

    return $result;
  }

}
