<?php

namespace Drupal\core_views_facets\Plugin\facets\core_views_facets\exposed_filter_type;

use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeBase;
use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Filter type "Combine" for core_views_facets.
 *
 * @CoreViewsFacetsExposedFilterType(
 *   id = "combine",
 *   label = "Combined Field"
 * )
 */
class Combine extends CoreViewsFacetsExposedFilterTypeBase implements CoreViewsFacetsExposedFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter) {
    $form = parent::buildConfigurationForm($form, $form_state, $facet, $facet_source, $filter);

    $filter_id = $filter->options['id'];
    $facet_configs = $facet->getFacetConfigs();
    $primary_field = !empty($facet_configs['views_combined_field']['primary_field']) ? $facet_configs['views_combined_field']['primary_field'] : NULL;
    $options = [];

    foreach ($filter->options['fields'] as $field_id) {
      /** @var \Drupal\views\Plugin\views\field\Field $field */
      $field = $filter->displayHandler->getHandler('field', $field_id);
      $options[$field_id] = $field->adminLabel();
    }

    $form['views_combined_field_primary_field'][$filter_id] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $primary_field,
      '#title' => $this->t('Combine primary field'),
      '#description' => $this->t('Choose the field to facet by.'),
      '#states' => [
        'visible' => [
          ':input[name="facet_source_configs[' . $facet_source->getPluginId() . '][field_identifier]"]' => ['value' => $filter_id],
        ],
      ],
    ];

    $reduce_facet = !empty($facet_configs['views_combined_field']['reduce_facet']) ? TRUE : FALSE;
    $form['views_combined_field_reduce_facet'] = [
      '#type' => 'checkbox',
      '#default_value' => $reduce_facet,
      '#title' => $this->t('Reduce facet entries so only the primary field is used'),
      '#description' => $this->t('By default, the facet will contain an entry for every combination of fields and list these in the link, even though only the primary field is used as facet value.'),
      '#states' => [
        'visible' => [
          ':input[name="facet_source_configs[' . $facet_source->getPluginId() . '][field_identifier]"]' => ['value' => $filter_id],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter) {
    parent::submitConfigurationForm($data, $facet, $facet_source, $filter);
    $facet_configs = $facet->getFacetConfigs() ?: [];
    $facet_configs['views_combined_field'] = [
      'primary_field' => $data['views_combined_field_primary_field'][$data['field_identifier']],
      'reduce_facet' => empty($data['views_combined_field_reduce_facet']) ? FALSE : TRUE,
    ];
    $facet->setFacetConfigs($facet_configs);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareQuery(ViewExecutable &$view, FilterPluginBase $filter, FacetInterface $facet) {
    $filter_table_alias = [];
    foreach ($filter->options['fields'] as $field_id) {
      if (!empty($view->field[$field_id])) {
        $filter_table_alias[$field_id] = $view->field[$field_id]->ensureMyTable();
      }
    }

    if (empty($filter_table_alias)) {
      return NULL;
    }

    $facet_configs = $facet->getFacetConfigs();
    $primary_field = empty($facet_configs['views_combined_field']['primary_field']) ? FALSE : $facet_configs['views_combined_field']['primary_field'];
    $reduce_facet = empty($facet_configs['views_combined_field']['reduce_facet']) ? FALSE : TRUE;

    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = $view->query->query();

    // The countQuery() method removes everything from the query, that doesn't
    // alter the result count. Such as all the SELECT x,y,z stuff and LEFT joins
    // not in the where clause etc.
    // The countQuery itself though, only counts the whole view, so it's not
    // useful as such, but the subquery can be combined with the facet
    // conditions.
    $query = $query->countQuery();

    // The better alternative $query->prepareCountQuery() is protected, so work
    // around it.
    $query = $query->getTables()['subquery']['table'];

    if ($primary_field && !empty($filter_table_alias[$primary_field])) {
      $select_table_alias = $filter_table_alias[$primary_field];
      $select_field = $primary_field;
    }
    else {
      $select_table_alias = reset($filter_table_alias);
      $select_field = key($filter_table_alias);
    }

    $fields = &$query->getFields();
    $expressions = &$query->getExpressions();

    // Make sure to only group by facetrawvalue.
    $group_by = &$query->getGroupBy();
    foreach ($group_by as $alias => $group_entry) {
      unset($fields[$alias]);
      unset($expressions[$alias]);
    }
    $group_by = [];

    if ($reduce_facet) {
      $query->groupBy($select_table_alias . "." . $select_field);
    }
    else {
      $concat_string = '';
      foreach ($filter_table_alias as $field => $table) {
        $concat_string .= $table . "." . $field . ", ' ',";
        $query->groupBy($table . "." . $field);
      }
      $concat_string = trim($concat_string, ',');
      $query->addExpression("CONCAT(" . $concat_string . ")", "facetlabel");
    }

    $query->addField($select_table_alias, $select_field, "facetrawvalue");
    $query->addExpression("COUNT(DISTINCT " . $view->storage->get('base_table') . "." . $view->storage->get('base_field') . ")", "facetcount");

    // The INNER JOIN should reduce the result set to only the actually
    // available facet values. So we're overriding the default LEFT JOIN.
    foreach ($filter_table_alias as $alias) {
      $tables = &$query->getTables();
      if ($tables[$alias]) {
        if (!empty($tables[$alias]['join type'])) {
          $tables[$alias]['join type'] = 'INNER';
        }
      }
    }

    return $query;
  }

}
