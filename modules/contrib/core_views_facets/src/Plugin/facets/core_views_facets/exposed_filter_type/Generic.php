<?php

namespace Drupal\core_views_facets\Plugin\facets\core_views_facets\exposed_filter_type;

use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeBase;
use Drupal\core_views_facets\CoreViewsFacetsExposedFilterTypeInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\facets\FacetInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;

/**
 * A generic filter type for core views.
 *
 * @CoreViewsFacetsExposedFilterType(
 *   id = "generic",
 *   label = "Generic solution"
 * )
 */
class Generic extends CoreViewsFacetsExposedFilterTypeBase implements CoreViewsFacetsExposedFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function prepareQuery(ViewExecutable &$view, FilterPluginBase $filter, FacetInterface $facet) {
    try {
      return parent::prepareQuery($view, $filter, $facet);
    }
    catch (\Exception $e) {
      watchdog_exception(
        'facets',
        $e,
        t("The core_views_facets module tried at least once to generically handle the unknown views filter type %filter_type and failed."),
        ['%filter_type' => $filter->pluginId],
        RfcLogLevel::NOTICE
      );
      return NULL;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, FilterPluginBase $filter, FacetInterface $facet) {
    $result = parent::processDatabaseRow($row, $filter, $facet);
    $facet_configs = $facet->getFacetConfigs();
    $exception = NULL;
    if (!empty($facet_configs['override_label'])) {
      if (!empty($filter->getEntityType())) {
        try {
          $facet_result_ids = \Drupal::entityQuery($filter->getEntityType())
            ->condition($filter->realField, $row->facetrawvalue)
            ->execute();

          if (!empty($facet_result_ids)) {
            $entity = \Drupal::entityTypeManager()
              ->getStorage($filter->getEntityType())
              ->load(array_pop($facet_result_ids));
            if ($entity) {
              $result->setDisplayValue($entity->label());
            }
          }
        }
        catch (\Exception $e) {
          $exception = $e;
        }
      }
    }

    if ($exception) {
      watchdog_exception(
        'facets',
        $exception,
        t("The core_views_facets module tried at least once to generically handle the unknown views filter type %filter_type and failed."),
        ['%filter_type' => $filter->pluginId],
        RfcLogLevel::NOTICE
      );
    }

    return $result;
  }

}
