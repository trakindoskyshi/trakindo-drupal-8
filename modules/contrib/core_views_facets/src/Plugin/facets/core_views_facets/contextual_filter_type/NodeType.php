<?php

namespace Drupal\core_views_facets\Plugin\facets\core_views_facets\contextual_filter_type;

use Drupal\core_views_facets\CoreViewsFacetsContextualFilterTypeBase;
use Drupal\core_views_facets\CoreViewsFacetsContextualFilterTypeInterface;
use Drupal\facets\FacetInterface;
use Drupal\node\Entity\NodeType as NodeBundleType;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;

/**
 * Filter type "NodeType" for core_views_facets.
 *
 * @CoreViewsFacetsContextualFilterType(
 *   id = "node_type",
 *   label = "Node Bundle Type"
 * )
 */
class NodeType extends CoreViewsFacetsContextualFilterTypeBase implements CoreViewsFacetsContextualFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, ArgumentPluginBase $filter, FacetInterface $facet) {
    $result = parent::processDatabaseRow($row, $filter, $facet);

    $facet_configs = $facet->getFacetConfigs();
    if (!empty($facet_configs['override_label'])) {
      $label = NodeBundleType::load($result->getRawValue())->label();
      $result->setDisplayValue($label);
    }

    return $result;
  }

}
