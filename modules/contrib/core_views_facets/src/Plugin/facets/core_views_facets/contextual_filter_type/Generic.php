<?php

namespace Drupal\core_views_facets\Plugin\facets\core_views_facets\contextual_filter_type;

use Drupal\core_views_facets\CoreViewsFacetsContextualFilterTypeBase;
use Drupal\core_views_facets\CoreViewsFacetsContextualFilterTypeInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\facets\FacetInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\ViewExecutable;

/**
 * A generic filter type for core views.
 *
 * @CoreViewsFacetsContextualFilterType(
 *   id = "generic",
 *   label = "Generic solution"
 * )
 */
class Generic extends CoreViewsFacetsContextualFilterTypeBase implements CoreViewsFacetsContextualFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function prepareQuery(ViewExecutable &$view, ArgumentPluginBase $filter, FacetInterface $facet) {
    try {
      return parent::prepareQuery($view, $filter, $facet);
    }
    catch (\Exception $e) {
      watchdog_exception(
        'facets',
        $e,
        t("The core_views_facets module tried at least once to generically handle the unknown views filter type %filter_type and failed."),
        ['%filter_type' => $filter->pluginId],
        RfcLogLevel::NOTICE
      );
      return NULL;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, ArgumentPluginBase $argument, FacetInterface $facet) {
    $result = parent::processDatabaseRow($row, $argument, $facet);
    $facet_configs = $facet->getFacetConfigs();
    $exception = NULL;
    if (!empty($facet_configs['override_label'])) {
      if ($entity_type = $argument->getEntityType()) {
        try {
          switch ($entity_type) {
            case 'taxonomy_term':
              $entity_field = $argument->definition['field_name'];
              break;

            default:
              $entity_field = $argument->realField;
              break;

          }

          $facet_result_ids = \Drupal::entityQuery($entity_type)
            ->condition($entity_field, $row->facetrawvalue)
            ->execute();

          if (!empty(reset($facet_result_ids))) {
            $entity = \Drupal::entityTypeManager()
              ->getStorage($entity_type)
              ->load(reset($facet_result_ids));
            if ($entity) {
              $result->setDisplayValue($entity->label());
            }
          }
        }
        catch (\Exception $e) {
          $exception = $e;
        }
      }
    }

    if ($exception) {
      watchdog_exception(
        'facets',
        $exception,
        t("The core_views_facets module tried at least once to generically handle the unknown views filter type %filter_type and failed."),
        ['%filter_type' => $argument->pluginId],
        RfcLogLevel::NOTICE
      );
    }

    return $result;
  }

}
