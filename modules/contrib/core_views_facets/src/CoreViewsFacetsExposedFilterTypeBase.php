<?php

namespace Drupal\core_views_facets;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Base class for Core views facets filter type plugins.
 */
abstract class CoreViewsFacetsExposedFilterTypeBase extends CoreViewsFacetsCommonFilterTypeBase implements CoreViewsFacetsExposedFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter) {
    $form = parent::baseConfigurationForm($form, $form_state, $facet, $facet_source, $filter);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter) {
    parent::baseConfigurationFormSubmit($data, $facet, $facet_source, $filter);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareQuery(ViewExecutable &$view, FilterPluginBase $filter, FacetInterface $facet) {
    $query = parent::basePrepareQuery($view, $filter, $facet);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, FilterPluginBase $filter, FacetInterface $facet) {
    $result = parent::baseProcessDatabaseRow($row, $filter, $facet);

    return $result;
  }

}
