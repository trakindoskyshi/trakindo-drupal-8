<?php

namespace Drupal\core_views_facets;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Defines an interface for Core views facets filter type plugins.
 */
interface CoreViewsFacetsExposedFilterTypeInterface extends PluginInspectionInterface {

  /**
   * Alters the configuration form for this facet source.
   *
   * @param array $form
   *   The configuration form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\facets\FacetInterface $facet
   *   The facet being edited.
   * @param \Drupal\facets\FacetSource\FacetSourcePluginInterface $facet_source
   *   The facet source being edited.
   * @param \Drupal\views\Plugin\views\filter\FilterPluginBase $filter
   *   The loaded views filter handler.
   *
   * @return array
   *   The altered facet source form
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter);

  /**
   * Submit the configuration form.
   *
   * @param array $data
   *   The form data specific to this filter type.
   * @param \Drupal\facets\FacetInterface $facet
   *   The facet being edited.
   * @param \Drupal\facets\FacetSource\FacetSourcePluginInterface $facet_source
   *   The facet source being edited.
   * @param \Drupal\views\Plugin\views\filter\FilterPluginBase $filter
   *   The loaded views filter handler.
   */
  public function submitConfigurationForm(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, FilterPluginBase $filter);

  /**
   * Alters the facet query before execution.
   *
   * @param \Drupal\views\ViewExecutable $view
   *    The views executable the facet applies to.
   * @param \Drupal\views\Plugin\views\filter\FilterPluginBase $filter
   *    The loaded views filter handler.
   * @param \Drupal\facets\FacetInterface $facet
   *    The facet being executed.
   *
   * @return NULL|\Drupal\Core\Database\Query\Select $query
   *    The altered query object to be executed.
   */
  public function prepareQuery(ViewExecutable &$view, FilterPluginBase $filter, FacetInterface $facet);

  /**
   * Alters the result row before displaying the content.
   *
   * @param \stdClass $row
   *    The row as returned by fetchObject().
   * @param \Drupal\views\Plugin\views\filter\FilterPluginBase $filter
   *    The loaded views filter handler.
   * @param \Drupal\facets\FacetInterface $facet
   *    The facet being executed.
   *
   * @return \Drupal\facets\Result\Result
   *    A valid facet result entity.
   */
  public function processDatabaseRow(\stdClass $row, FilterPluginBase $filter, FacetInterface $facet);

}
