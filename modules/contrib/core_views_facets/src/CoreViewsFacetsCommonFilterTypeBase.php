<?php

namespace Drupal\core_views_facets;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\facets\Result\Result;
use Drupal\views\ViewExecutable;

/**
 * Base class for Core views facets filter type plugins.
 */
abstract class CoreViewsFacetsCommonFilterTypeBase extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function baseConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, $handler) {
    $facet_configs = $facet->getFacetConfigs();

    $override_label = !empty($facet_configs['override_label']) ? TRUE : FALSE;

    $form['override_label'] = [
      '#type' => 'checkbox',
      '#default_value' => $override_label,
      '#title' => $this->t('Replace raw value with respective label in facet link'),
      '#description' => $this->t('The result of this option depends on a working plugin for this facets views plugin type.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function baseConfigurationFormSubmit(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, $handler) {
    $facet_configs['override_label'] = empty($data['override_label']) ? FALSE : TRUE;
    $facet->setFacetConfigs($facet_configs);
  }

  /**
   * {@inheritdoc}
   */
  public function basePrepareQuery(ViewExecutable &$view, $handler, FacetInterface $facet) {
    $filter_table_alias = [];
    $relationship = NULL;
    if (!empty($handler->options['relationship']) && $handler->options['relationship'] != 'none') {
      /** @var \Drupal\views\Plugin\views\relationship\RelationshipPluginBase $relationship_handler */
      $relationship_handler = $view->getDisplay()->getHandler('relationship', $handler->options['relationship']);
      if ($relationship_handler) {
        $relationship = $relationship_handler->alias;
      }
    }

    /** @var \Drupal\views\Plugin\views\query\Sql $view_query */
    $view_query = $view->query;
    $filter_table_alias[] = $view_query->ensureTable($handler->table, $relationship);

    /** @var \Drupal\Core\Database\Query\Select $query */
    $query = $view_query->query();

    // The countQuery() method removes everything from the query, that doesn't
    // alter the result count. Such as all the SELECT x,y,z stuff and LEFT joins
    // not in the where clause etc.
    // The countQuery itself though, only counts the whole view, so it's not
    // useful as such, but the subquery can be combined with the facet
    // conditions.
    $query = $query->countQuery();

    // The better alternative $query->prepareCountQuery() is protected, so work
    // around it.
    $query = $query->getTables()['subquery']['table'];

    $select_table_alias = $filter_table_alias[0];
    $select_field = $handler->realField;
    $query->addField($select_table_alias, $select_field, "facetrawvalue");

    $fields = &$query->getFields();
    $expressions = &$query->getExpressions();

    // Make sure to only group by facetrawvalue.
    $group_by = &$query->getGroupBy();
    foreach ($group_by as $alias => $group_entry) {
      unset($fields[$alias]);
      unset($expressions[$alias]);
    }
    $group_by = [];
    $query->groupBy("facetrawvalue");

    // Remove unnecessary orders.
    $orders = &$query->getOrderBy();
    foreach ($orders as $alias => $order) {
      unset($fields[$alias]);
      unset($expressions[$alias]);
    }
    $orders = [];

    $query->addExpression("COUNT(DISTINCT " . $view->storage->get('base_table') . "." . $view->storage->get('base_field') . ")", "facetcount");

    // The INNER JOIN should reduce the result set to only the actually
    // available facet values. So we're overriding the default LEFT JOIN.
    foreach ($filter_table_alias as $alias) {
      $tables = &$query->getTables();
      if (!empty($tables[$alias]['join type'])) {
        $tables[$alias]['join type'] = 'INNER';
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function baseProcessDatabaseRow(\stdClass $row, $handler, FacetInterface $facet) {
    $value = $row->facetrawvalue;
    $count = $row->facetcount;
    if (!empty($row->facetlabel)) {
      $label = $row->facetlabel;
    }
    else {
      $label = $value;
    }

    return new Result($value, $label, $count);
  }

}
