<?php

namespace Drupal\core_views_facets;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Base class for Core views facets filter type plugins.
 */
abstract class CoreViewsFacetsContextualFilterTypeBase extends CoreViewsFacetsCommonFilterTypeBase implements CoreViewsFacetsContextualFilterTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, ArgumentPluginBase $argument) {
    $form = parent::baseConfigurationForm($form, $form_state, $facet, $facet_source, $argument);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, ArgumentPluginBase $argument) {
    parent::baseConfigurationFormSubmit($data, $facet, $facet_source, $argument);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareQuery(ViewExecutable &$view, ArgumentPluginBase $argument, FacetInterface $facet) {
    $query = parent::basePrepareQuery($view, $argument, $facet);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function processDatabaseRow(\stdClass $row, ArgumentPluginBase $filter, FacetInterface $facet) {
    $result = parent::baseProcessDatabaseRow($row, $filter, $facet);

    return $result;
  }

}
