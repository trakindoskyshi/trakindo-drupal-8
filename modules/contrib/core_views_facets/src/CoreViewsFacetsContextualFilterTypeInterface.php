<?php

namespace Drupal\core_views_facets;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetSource\FacetSourcePluginInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Defines an interface for Core views facets filter type plugins.
 */
interface CoreViewsFacetsContextualFilterTypeInterface extends PluginInspectionInterface {

  /**
   * Alters the configuration form for this facet source.
   *
   * @param array $form
   *   The configuration form definition.
   * @param FormStateInterface $form_state
   *   The current form state.
   * @param FacetInterface $facet
   *   The facet being edited.
   * @param FacetSourcePluginInterface $facet_source
   *   The facet source being edited.
   * @param ArgumentPluginBase $argument
   *   The loaded views contextual filter handler.
   *
   * @return array
   *   The altered facet source form
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet, FacetSourcePluginInterface $facet_source, ArgumentPluginBase $argument);

  /**
   * Submit the configuration form.
   *
   * @param array $data
   *   The form data specific to this filter type.
   * @param FacetInterface $facet
   *   The facet being edited.
   * @param FacetSourcePluginInterface $facet_source
   *   The facet source being edited.
   * @param ArgumentPluginBase $argument
   *   The loaded views contextual filter handler.
   */
  public function submitConfigurationForm(array $data, FacetInterface &$facet, FacetSourcePluginInterface $facet_source, ArgumentPluginBase $argument);

  /**
   * Alters the facet query before execution.
   *
   * @param ViewExecutable $view
   *    The views executable the facet applies to.
   * @param ArgumentPluginBase $argument
   *    The loaded views contextual filter handler.
   * @param FacetInterface $facet
   *    The facet being executed.
   *
   * @return NULL|\Drupal\Core\Database\Query\Select $query
   *    The altered query object to be executed.
   */
  public function prepareQuery(ViewExecutable &$view, ArgumentPluginBase $argument, FacetInterface $facet);

  /**
   * Alters the result row before displaying the content.
   *
   * @param \stdClass $row
   *    The row as returned by fetchObject().
   * @param ArgumentPluginBase $argument
   *    The loaded views contextual filter handler.
   * @param FacetInterface $facet
   *    The facet being executed.
   *
   * @return \Drupal\facets\Result\Result
   *    A valid facet result entity.
   */
  public function processDatabaseRow(\stdClass $row, ArgumentPluginBase $argument, FacetInterface $facet);

}
