<?php

/**
 * @file
 * Contains product\product.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Render\Markup;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\system\ActionConfigEntityInterface;

/**
* Implements hook_views_data().
*/
function product_views_data() {

    $data['views']['table']['group'] = t('Custom Global');
    $data['views']['table']['join'] = array(
      // #global is a special flag which allows a table to appear all the time.
      '#global' => array(),
    );

    $data['views']['productajaxlink_field'] = array(
        'title' => t('Product Compare AJAX Link Field'),
        'help' => t('Product Compare AJAX Link Field'),
        'field' => array(
            'id' => 'product_compare_ajax_field',
        ),
    );

    $data['views']['product_snapshot'] = array(
        'title' => t('Product Snapshot'),
        'help' => t('Product Snapshot'),
        'field' => array(
            'id' => 'product_snapshot',
        ),
    );

    $data['views']['product_url'] = array(
        'title' => t('Product URL'),
        'help' => t('Product URL'),
        'field' => array(
            'id' => 'product_url',
        ),
    );

    $data['views']['product_360'] = array(
        'title' => t('Product 360'),
        'help' => t('Product 360'),
        'field' => array(
            'id' => 'product_360',
        ),
    );

    return $data;
}
