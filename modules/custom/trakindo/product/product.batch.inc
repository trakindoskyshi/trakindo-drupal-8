<?php

use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;
use Drupal\views\Views;

function product_get_snapshot($id, &$context) {
  $user = \Drupal::currentUser();
  $entity = ProductService::productLoad($id);
  $product = $entity->toArray();
  product_snapshot_save($id, 'en');
  if ($entity->hasTranslation('id')) {
    product_snapshot_save($id, 'id');
  }
}

/*function product_get_snapshot($id, &$context) {
  $user = \Drupal::currentUser();
  $entity = ProductService::productLoad($id);
  $product = $entity->toArray();
  $product_type = $product['type'][0]['target_id'];
  $view_name = 'product_detail';
  $view_page = 'page_1';
  if ($product_type == 'cat_lift_trucks') {
    $view_name = 'lift_truck_detail';
  }
  if ($product_type == 'used_equipment') {
    $view_page = 'page_2';
  }
  $args = [$id];
  $view = Views::getView($view_name);
  if (is_object($view)) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $view->setArguments($args);
    $view->setDisplay($view_page);
    $lang_filter = $view->getHandler($view_page, 'filter', 'langcode');
    $view->setHandler($view_page, 'filter', 'langcode', $lang_filter);
    $view->preExecute();
    $view->execute();
    $content = $view->buildRenderable($view_page, $args);
    $html = \Drupal::service('renderer')->renderRoot($content)->__toString();

    $product_name = $product['name'][0]['value'];
    $filename = $id. '-en-'. date('H:i'). '.html';
    $destination = 'public://snapshot/'. date('Y-m');
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
    
    $file = file_save_data($html, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
    $fid = $file->id();

    $day_start = strtotime(date('Y-m-d 00:00:00'));
    $day_end = strtotime(date('Y-m-d 23:59:59'));
    $query_snapshot = \Drupal::entityQuery('products')
      ->condition('created', array($day_start, $day_end), 'BETWEEN')
      ->condition('field_product', $id)
      ->condition('type', 'snapshot');
    $snapshot_results = $query_snapshot->execute();

    if ($snapshot_results) {
      $snapshot_id = reset($snapshot_results);
      $snapshot = ProductService::productLoad($snapshot_id);
      $snapshot->created = time();
      $snapshot->changed = time();
      $snapshot->field_snapshot = $fid;
      $snapshot->name = $product_name. '-'. date('Y-m-d H:i');
      $snapshot->save();
    } else {
      $snapshot = \Drupal::entityTypeManager()->getStorage('products')->create([
        'type' => 'snapshot',
        'name' => $product_name. '-'. date('Y-m-d H:i'),
        'field_product' => $id,
        'field_snapshot' => $fid
      ]);
      $snapshot->save();
    }
    if ($entity->hasTranslation('id')) {
      $view = Views::getView($view_name);
      $view->setArguments($args);
      $view->setDisplay($view_page);
      $lang_filter = $view->getHandler($view_page, 'filter', 'langcode');
      $lang_filter['value'] = ['id' => 'id'];
      $view->setHandler($view_page, 'filter', 'langcode', $lang_filter);
      $view->preExecute();
      $view->execute();

      $content = $view->buildRenderable($view_page, $args);
      $html = \Drupal::service('renderer')->renderRoot($content)->__toString();
      $filename = $id. '-id-'. date('H:i'). '.html';
      $file = file_save_data($html, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
      $fid = $file->id();

      if ($snapshot->hasTranslation('id')) {
        $snapshot_translation = $snapshot->getTranslation('id');
      } else {
        $snapshot_translation = $snapshot->addTranslation('id');
      }
      $snapshot_translation->created = time();
      $snapshot_translation->changed = time();
      $snapshot_translation->field_snapshot = $fid;
      $snapshot_translation->name = $product_name. '-'. date('Y-m-d H:i');
      $snapshot_translation->user_id = $user->id();
      $snapshot_translation->save();
    }
  }
  $context['results'][] = $id;  
  $context['message'] = 'Saving snapshot for product: '. $id;
}*/

function product_import($tree_id, $group_id, $product_id, $product_path, $tree, $language, &$context) {
  $message =  t('Processing tree @tree_id : product ID: @product_id, language: @language', [
    '@tree_id' => $tree_id, 
    '@product_id' => $product_id,
    '@language' => $language
  ]);
  $context['results'][] = $product_id;  
  $context['message'] = $message;

  $product_service = new ProductService();
  $product_service->setParentId($tree_id);
  $product_service->setProductId($product_id);
  $product_service->setTree($tree);

  $product_data = $product_service->parseXML($product_path);
  $update = TRUE;
  $message = '';
  if ($update) {
    \Drupal::logger('product_importer')->notice($tree_id. ' : '. $product_id);
    \Drupal::logger('product_importer')->notice($product_path);
    $product_service->prepareData($product_data[0]);
    if ($language == 'en') {
      $product_service->createProduct();
    } else {
      $product_service->translateProduct();
    }
  }
}

function product_download_xml($salesChannelCode, $languageId, $basePath, $last_modified, &$context) {
  file_prepare_directory($basePath, FILE_CREATE_DIRECTORY);
  $basePath = \Drupal::service('file_system')->realpath($basePath);
  $cpc = new CPC($salesChannelCode, $languageId, $basePath, $last_modified);

  $context['results'][] = $languageId;  
  $context['message'] = 'Downloading XML for Language: '. $languageId;
}

function product_delete($id, $language, &$context) {
  $context['message'] = '';
  $context['message'] = 'Deleting product id: '. $id;

  $context['results'][] = $id;  
  $product = ProductService::productLoad($id);
  if ($language == 'en') {
    $product->status = 0;
  } else {
    $product->removeTranslation('id');
  }

  $product->save();
  
}

function product_batch_finished_callback($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $message = \Drupal::translation()->formatPlural(
      count($results),
      'One product processed.', '@count products processed.'
    );
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  //$_SESSION['disc_migrate_batch_results'] = $results;
}
