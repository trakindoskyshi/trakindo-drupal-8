<?php

namespace Drupal\product\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Class ProductThemeNegotiator.
 *
 * @package Drupal\product
 */
class ProductThemeNegotiator implements ThemeNegotiatorInterface {
	/**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
  	$current_path = \Drupal::service('path.current')->getPath();
		$match = \Drupal::service('path.matcher')->matchPath($current_path, '/batch');
    if ($match) {
    	//return TRUE;
    }
    return FALSE;    	
  }
 
  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return 'trakindo_2016';
  }

}
