<?php

namespace Drupal\product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;
use Drupal\product_importer\Service\ProductService;

/**
 * Class ProductSnapshotBatchForm.
 *
 * @package Drupal\product\Form
 */
class ProductSnapshotBatchForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_snapshot_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $host = \Drupal::request()->getHost();
    $product_types = [
      'equipment',
      'power_systems',
      'attachments',
      'cat_lift_trucks',
      'used_equipment'
    ];

    $query = \Drupal::entityQuery('products')
      ->condition('type', $product_types, 'IN');
    $results = $query->execute();
    $ids = array_keys($results);
    foreach ($ids as $id) {
      /*$entity = ProductService::productLoad($id);
      $product = $entity->toArray();
      $product_type = $product['type'][0]['target_id'];
      $page = 'product-detail';

      if ($product_type == 'cat_lift_trucks') {
        $page = 'lift-truck-detail';
      }
      if ($product_type == 'used_equipment') {
        $page = 'used-detail';
      }

      $url = "http://$host/en/$page/$id";
      $html = product_get_data($url);
      $data = gzencode($html, 9);

      $product_name = $product['name'][0]['value'];
      $filename = $id. '-en-'. date('H:i'). '.html.gz';
      $destination = 'public://snapshot/'. date('Y-m-d');
      file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
      
      $file = file_save_data($data, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
      $fid = $file->id();

      $day_start = strtotime(date('Y-m-d 00:00:00'));
      $day_end = strtotime(date('Y-m-d 23:59:59'));
      $query_snapshot = \Drupal::entityQuery('products')
        ->condition('created', array($day_start, $day_end), 'BETWEEN')
        ->condition('field_product', $id)
        ->condition('type', 'snapshot');
      $snapshot_results = $query_snapshot->execute();

      if ($snapshot_results) {
        $snapshot_id = reset($snapshot_results);
        $snapshot = ProductService::productLoad($snapshot_id);
        $snapshot->created = time();
        $snapshot->changed = time();
        $snapshot->field_snapshot = $fid;
        $snapshot->name = $product_name. '-'. date('Y-m-d H:i');
        $snapshot->save();
      } else {
        $snapshot = \Drupal::entityTypeManager()->getStorage('products')->create([
          'type' => 'snapshot',
          'name' => $product_name. '-'. date('Y-m-d H:i'),
          'field_product' => $id,
          'field_snapshot' => $fid
        ]);
        $snapshot->save();
      }
      if ($entity->hasTranslation('id')) {
        $url = "http://$host/id/$page/$id";

        $html = product_get_data($url);
        $data = gzencode($html, 9);
        $filename = $id. '-id-'. date('H:i'). '.html.gz';
        $file = file_save_data($data, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
        $fid = $file->id();

        if ($snapshot->hasTranslation('id')) {
          $snapshot_translation = $snapshot->getTranslation('id');
        } else {
          $snapshot_translation = $snapshot->addTranslation('id');
        }
        $snapshot_translation->created = time();
        $snapshot_translation->changed = time();
        $snapshot_translation->field_snapshot = $fid;
        $snapshot_translation->name = $product_name. '-'. date('Y-m-d H:i');
        $snapshot_translation->user_id = $user->id();
        $snapshot_translation->save();
      }*/
      $operations[] = array('product_get_snapshot', array($id));

    }
    $batch = array(
      'title' => 'Set Product Cache',
      'operations' => $operations,
      'finished' => 'product_batch_finished_callback',
      'file' => drupal_get_path('module', 'product') . '/product.batch.inc',
    );
  
    batch_set($batch);

  }

}
