<?php

namespace Drupal\product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;

/**
 * Class DownloadXML.
 *
 * @package Drupal\product\Form
 */
class DownloadXML extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_xml';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');

    $salesChannelCode = "J210";
    $languageId = "en";

    $basePath = $config->get('import_path');
    $last_modified = $config->get('import_time_range');

    $operations = [];
    $operations[] = array('product_download_xml', array($salesChannelCode, $languageId, $basePath, $last_modified));
    $languageId = "id";
    $operations[] = array('product_download_xml', array($salesChannelCode, $languageId, $basePath, $last_modified));

    $batch = array(
      'title' => 'Download CAT XML',
      'operations' => $operations,
      'finished' => 'product_batch_finished_callback',
      'file' => drupal_get_path('module', 'product') . '/product.batch.inc',
    );
  
    batch_set($batch);
  }

}
