<?php
namespace Drupal\product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class XLSExportForm.
 *
 * @package Drupal\product\Form
 */
class ProductCronImportForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_cron_import_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cron_import.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  	$config = $this->config('cron_import.settings');
    $form['import_time_range'] = [
      '#type' => 'textfield',
      '#title' => 'Time Range for Product Last Modified',
      '#description' => 'Use PHP strtotime() function, example: -3 days',
      //'#default_value' => ($config->get('import_time_range')) ? $config->get('import_time_range') : '-3 days',
    ];
  	$form['import_path'] = [
  		'#type' => 'textfield',
  		'#title' => 'Import Path',
  		'#description' => 'Should be drupal internal path',
  		'#default_value' => ($config->get('import_path')) ? $config->get('import_path') : 'public://product_xml/unprocessed',
  	];
    $form['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  	$values = $form_state->getValues();
    file_prepare_directory($values['import_path'], FILE_CREATE_DIRECTORY);
    file_prepare_directory($values['import_path'], FILE_MODIFY_PERMISSIONS);
  	$this->config('cron_import.settings')
      ->set('import_time_range', $values['import_time_range'])
      ->set('import_path', $values['import_path'])
//      ->set('import_interval', $values['import_interval'])
      ->save();
  }

}
