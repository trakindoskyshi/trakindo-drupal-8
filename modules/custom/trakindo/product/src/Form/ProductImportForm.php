<?php

namespace Drupal\product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\product_importer\Service\ProductService;
use Sabre\Xml;

/**
 * Class ProductImportForm.
 *
 * @package Drupal\product\Form
 */
class ProductImportForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    $files = file_scan_directory($config->get('import_path'), '/[\w]+tree_en\.xml/');
    $options = [];
    foreach($files as $file) {
      $options[$file->filename] = $this->t($file->filename);
    }
    
    $form['action'] = [
      '#type' => 'radios',
      '#options' => [
        'delete_product' => $this->t('Unpublish Unused Product'),
        'import_all_product' => $this->t('Import All Product'),
        'import_product' => $this->t('Import New Product Only'),
      ],
      '#default_value' => 'import_all_product',
      '#title' => $this->t('Action'),
      '#description' => $this->t('Select XML file to import'),
    ];

    $form['import_type'] = [
      '#type' => 'checkboxes',
      '#options' => [
        402 => 'Engines',
        406 => 'Machines',
        405 => 'Work Tools'
      ],
      '#title' => $this->t('Product Type'),
      '#description' => $this->t('Product type to be imported'),
      '#required' => TRUE
    ];

    $form['language'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'en' => 'English',
        'id' => 'Indonesia',
      ],
      '#title' => $this->t('Language'),
      '#required' => TRUE
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  private static function getProducts($import_type, $type, $language) {
    $output = [];
    $products_service = new ProductService();
    $config = \Drupal::config('cron_import.settings');

    $tree_files = file_scan_directory($config->get('import_path'), '/[\w]+'. $type.'\.xml/');
    foreach ($tree_files as $tree_path => $tree_value) {
      $tree_id = str_replace($type, '', $tree_value->name);
      if (!in_array($tree_id, $import_type)) continue;

      $tree_data = $products_service->parseXML($tree_path, ['elementMap' => '{}product_group']);
      $tree_name_sources = [];
      $level1_name_sources = [];
      $level2_name_sources = [];
      if ($language == 'id') {
        $tree_path_source = str_replace('id', 'en', $tree_path);
        $tree_data_source = $products_service->parseXML($tree_path_source, ['elementMap' => '{}product_group']);
        
        $tree_id_source = $tree_data_source[0]['attributes']['id'];
        $tree_name_sources[$tree_id_source] = $tree_data_source[0]['value']['{}name']. '|';
        foreach ($tree_data_source[0]['value']['{}listofgroups'] as $level1_source) {
          $level1_id_source = $level1_source['attributes']['id'];
          $level1_name_sources[$level1_id_source] = $level1_source['value']['{}name']. '|';
          foreach ($level1_source['value']['{}listofgroups'] as $level2_source) {
            $level2_id_source = $level2_source['attributes']['id'];
            $level2_name_sources[$level2_id_source] = $level2_source['value']['{}name']. '|';
          }
        }  
      }
      $tree_name = $tree_data[0]['value']['{}name'];
      $tree_id = $tree_data[0]['attributes']['id'];
      foreach ($tree_data[0]['value']['{}listofgroups'] as $level1) {
        $level1_name = $level1['value']['{}name'];
        $level1_id = $level1['attributes']['id'];
        foreach ($level1['value']['{}listofgroups'] as $level2) {
          $level2_name = $level2['value']['{}name'];
          $level2_id = $level2['attributes']['id'];
          foreach ($level2['value']['{}listofproducts'] as $level3) {
            $product_id = $level3['attributes']['id'];
            $product_path = $config->get('import_path'). "/$language/". $tree_id. '/'. $level1_id. '/'. $level2_id. '/'. $product_id. "_{$language}.xml";

            $group_id = $level2_id;
            
            if ($language == 'id') {
              $tree_name_source = $tree_name_sources[$tree_id];
              $level1_name_source = $level1_name_sources[$level1_id];
              $level2_name_source = $level2_name_sources[$level2_id];
            } else {
              $tree_name_source = '';
              $level1_name_source = '';
              $level2_name_source = '';
            }
            $tree = [
              $tree_name_source. $tree_name,
              $level1_name_source. $level1_name,
              $level2_name_source. $level2_name
            ];
            
            $output[] = array(
              'tree_id' => $tree_id, 
              'group_id' => $group_id, 
              'product_id' => $product_id, 
              'product_path' => $product_path, 
              'tree' => $tree
            );
          }
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    // Display result.
    $operations = [];
    $action = $form_state->getValue('action');
    $import_type = array_values(array_filter($form_state->getValue('import_type')));
    $languages = array_values(array_filter($form_state->getValue('language')));

    $tids = [];
    $group_cache = [];

    $map = [
      402 => 'power_systems',
      406 => 'equipment',
      405 => 'attachments'
    ];
    
    if ($action == 'import_all_product') {
      $products_service = new ProductService();

      foreach ($languages as $language) {
        $type = "tree_$language";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $tree_id = $product['tree_id'];
          $group_id = $product['group_id'];
          $product_id = $product['product_id'];
          $product_path = $product['product_path'];
          $tree = $product['tree'];
          
          $operations[] = array('product_import', array($tree_id, $group_id, $product_id, $product_path, $tree, $language));
        }

        $type = "tree_{$language}_nc";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $tree_id = $product['tree_id'];
          $group_id = $product['group_id'];
          $product_id = $product['product_id'];
          $product_path = $product['product_path'];
          $tree = $product['tree'];

          $operations[] = array('product_import', array($tree_id, $group_id, $product_id, $product_path, $tree, $language));
        }
      }

      $batch = array(
        'title' => 'Import Product',
        'operations' => $operations,
        'finished' => 'product_batch_finished_callback',
        'file' => drupal_get_path('module', 'product') . '/product.batch.inc',
      );

      batch_set($batch);
    } else if ($action == 'import_product') {
      $product_types = [];
      foreach ($import_type as $product_type) {
        if (in_array($product_type, array_keys($map))) $product_types[] = $map[$product_type];
      }
      foreach ($languages as $language) {
        $query = \Drupal::entityQuery('products')
          ->condition('langcode', $language)
          ->condition('type', $product_types, 'IN');
        $results = $query->execute();
        $current_ids = array_keys($results);
        $entity_ids = [];
        if ($current_ids) {
          $query_entity = \Drupal::database()->select('products__field_product_id', 'pid')
            ->fields('pid', ['field_product_id_value'])
            ->condition('pid.entity_id', $current_ids, 'IN')
            ->execute()->fetchAllAssoc('field_product_id_value');
          $entity_ids = array_keys($query_entity);
        }

        $type = "tree_$language";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $tree_id = $product['tree_id'];
          $group_id = $product['group_id'];
          $product_id = $product['product_id'];
          $product_path = $product['product_path'];
          $tree = $product['tree'];
          
          if (in_array($product_id, $entity_ids)) continue;
          $operations[] = array('product_import', array($tree_id, $group_id, $product_id, $product_path, $tree, $language));
        }
        
        $type = "tree_{$language}_nc";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $tree_id = $product['tree_id'];
          $group_id = $product['group_id'];
          $product_id = $product['product_id'];
          $product_path = $product['product_path'];
          $tree = $product['tree'];
          
          if (in_array($product_id, $entity_ids)) continue;
          $operations[] = array('product_import', array($tree_id, $group_id, $product_id, $product_path, $tree, $language));
        }
      }

      $batch = array(
        'title' => 'Import Product',
        'operations' => $operations,
        'finished' => 'product_batch_finished_callback',
        'file' => drupal_get_path('module', 'product') . '/product.batch.inc',
      );
      batch_set($batch);
    } else if ($action == 'delete_product') {
      $product_types = [];
      foreach ($import_type as $product_type) {
        if (in_array($product_type, array_keys($map))) $product_types[] = $map[$product_type];
      }

      $products_service = new ProductService();
      
      foreach ($languages as $language) {
        $keep_ids = [];
        $type = "tree_$language";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $product_id = $product['product_id'];
          $keep_ids[] = $product_id;
        }

        $type = "tree_{$language}_nc";
        $products = self::getProducts($import_type, $type, $language);
        foreach ($products as $product) {
          $product_id = $product['product_id'];
          $keep_ids[] = $product_id;
        }
        $query = \Drupal::entityQuery('products')
          ->condition('field_product_id', $keep_ids, 'NOT IN')
          ->condition('langcode', $language)
          ->condition('type', $product_types, 'IN');
        $results = $query->execute();
        $ids = array_keys($results);
        foreach ($ids as $id) {
          $operations[] = array('product_delete', array($id, $language));
        }
      }

      $batch = array(
        'title' => 'Delete Unused Product',
        'operations' => $operations,
        'finished' => 'product_batch_finished_callback',
        'file' => drupal_get_path('module', 'product') . '/product.batch.inc',
      );
      batch_set($batch);
    }

  }

}
