<?php

namespace Drupal\product\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a child category processor.
 *
 * @FacetsProcessor(
 *   id = "child_result_processor",
 *   label = @Translation("Child category"),
 *   description = @Translation("Display only child category."),
 *   stages = {
 *     "build" = 40
 *   }
 * )
 */
class ChildResultProcessor extends ProcessorPluginBase implements BuildProcessorInterface {
  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $active_filter = \Drupal::request()->query->get('filter');
    $active_group = [];
    
    if ($active_filter) {
      foreach ($active_filter as $filter) {
        if (strpos($filter, 'group:') !== FALSE) $filter = str_replace('group:', '', $filter);
        //if (strpos($filter, 'child_') !== FALSE) $filter = str_replace('child_', '', $filter);
        $active_group[] = $filter;
      }
    }

    $level1 = [];
    $level2 = [];
    $level3 = [];
    $sorted_result = [];

    foreach ($results as $result) {
      $term_id = $result->getRawValue();
      $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
      if (!$parent) {

        $level1[$term_id] = $result;
      } else {
        $children = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term_id);
        if ($children) {
          $level2[$term_id] = $result;
        } else {
          //assume level 3 but we should have relation with level 2
          //get the parent first
          $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
          $parent_term_id = key($parent);
          if (!isset($level3[$parent_term_id])) {
            $level3[$parent_term_id] = [];
          }
          $level3[$parent_term_id][] = $result;
        }
      }
    }

    //$sorted_result[] = $level1[0];
    foreach ($level2 as $parent_id => &$level2_data) {
    
      if (isset($level3[$parent_id]) && in_array($parent_id, $active_group)) {
        $childrenData = [];
        foreach ($level3[$parent_id] as $level3_data) {
          //$sorted_result[] = $level3_data;
          $sorted_result[] = $level3_data;
        }
      }
    }

    return $sorted_result;
  }

}
