<?php

namespace Drupal\product\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a parent category processor.
 *
 * @FacetsProcessor(
 *   id = "parent_result_processor",
 *   label = @Translation("Parent category"),
 *   description = @Translation("Display only parent category."),
 *   stages = {
 *     "build" = 40
 *   }
 * )
 */
class ParentResultProcessor extends ProcessorPluginBase implements BuildProcessorInterface {
  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $level1 = [];
    $level2 = [];
    $level3 = [];
    $sorted_result = [];
    //hardcode for cat lift truck facet tree
    $route_name = \Drupal::routeMatch()->getRouteName();

    foreach ($results as $result) {
      $term_id = $result->getRawValue();
      $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
      if (!$parent) {
        if ($route_name == 'view.products_search.page_7') {

          if ($result->getRawValue() == '313') continue;
        }
        $level1[$term_id] = $result;
      } else {
        $children = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term_id);
        if ($children) {
          if (!isset($level2[$term_id])) {
            $level2[$term_id] = $result;
          }
        } else {
          //assume level 3 but we should have relation with level 2
          //get the parent first
          $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
          $parent_term_id = key($parent);

          if (!isset($level3[$parent_term_id])) {
            $level3[$parent_term_id] = [];
          }
          $level3[$parent_term_id][] = $result;
        }
      }
    }

    
    if ($route_name == 'view.products_search.page_4') {
      foreach ($level3 as $parent_id => &$level3_data) {
        foreach ($level3_data as $value) {
          $sorted_result[] = $value;
        }
      }
    } else if ($route_name == 'view.products_search.page_7') {
      foreach ($level1 as $parent_id => &$level1_data) {
        $sorted_result[] = $level1_data;
      }
      foreach ($level2 as $parent_id => &$level2_data) {
        $sorted_result[] = $level2_data;
      }
    } else {
      foreach ($level2 as $parent_id => &$level2_data) {
        $sorted_result[] = $level2_data;
      }
    }
    

    return $sorted_result;
  }

}
