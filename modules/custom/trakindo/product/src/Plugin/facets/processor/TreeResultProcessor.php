<?php

namespace Drupal\product\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a tree processor.
 *
 * @FacetsProcessor(
 *   id = "tree_result_processor",
 *   label = @Translation("Tree results"),
 *   description = @Translation("Tree results."),
 *   stages = {
 *     "build" = 40
 *   }
 * )
 */
class TreeResultProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $level1 = [];
    $level2 = [];
    $level3 = [];
    $sorted_result = [];

    foreach ($results as $result) {
      $term_id = $result->getRawValue();
      $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
      if (!$parent) {
        $level1[] = $result;
      } else {
        $children = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term_id);
        if ($children) {
          if (!isset($level2[$term_id])) {
            $level2[$term_id] = $result;
          }
        } else {
          //assume level 3 but we should have relation with level 2
          //get the parent first
          $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
          $parent_term_id = key($parent);
          if (!isset($level2[$parent_term_id])) {
            $level3[$parent_term_id] = [];
          }
          $level3[$parent_term_id][] = $result;
        }
      }
    }

    //$sorted_result[] = $level1[0];
    foreach ($level2 as $parent_id => &$level2_data) {
      $sorted_result[] = $level2_data;
      if (isset($level3[$parent_id])) {
        $childrenData = [];
        foreach ($level3[$parent_id] as $level3_data) {
          //$sorted_result[] = $level3_data;
          $childrenData[] = $level3_data;
        }
        $level2_data->setChildren($childrenData);
      }
    }

    return $sorted_result;
  }

}
