<?php

namespace Drupal\product\Plugin\facets\widget;

use Drupal\facets\FacetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\Plugin\facets\widget\LinksWidget;
use Drupal\facets\Result\ResultInterface;
use Drupal\Core\Url;
/**
 * The checkbox ajax widget.
 *
 * @FacetsWidget(
 *   id = "tree_checkboxes",
 *   label = @Translation("List of checkboxes with tree"),
 *   description = @Translation("A configurable widget that shows a list of checkboxes"),
 * )
 */
class CheckboxTreeWidget extends LinksWidget {
	/**
	* {@inheritdoc}
	*/
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);

    $build['#attributes']['class'][] = 'js-facets-checkbox-links';
    $build['#attached']['library'][] = 'facets/drupal.facets.checkbox-widget';
    return $build;
  }


  /**
   * Builds a renderable array of result items.
   *
   * @param \Drupal\facets\Result\ResultInterface $result
   *   A result item.
   *
   * @return array
   *   A renderable array of the result.
   */
  protected function buildListItems(ResultInterface $result) {

    $classes = ['facet-item'];
    //$item = $this->prepareLink($result);
    $term_id = $result->getRawValue();
    $children = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term_id);
    $active_filter = \Drupal::request()->query->get('filter');
    $active_group = [];
    if ($active_filter) {
      foreach ($active_filter as $filter) {
        if (strpos($filter, 'child_group:') !== FALSE) $active_group[] = str_replace('child_group:', '', $filter);
      }
    }
    if ($children) {
      $intersect = array_intersect($active_group, array_keys($children));
        /*dpm($active_group);
        dpm($children);*/
      if ($intersect) {
        $new_filter = [];
        foreach ($active_filter as $key => $filter) {
          $tmp = $filter;
          if (strpos($filter, 'child_group:') !== FALSE) $tmp = str_replace('child_group:', '', $tmp);
          if (strpos($filter, 'group:') !== FALSE) $tmp = str_replace('group:', '', $tmp);
          if (!in_array($tmp, $new_filter) && !in_array($tmp, $intersect) && !in_array($tmp, [$term_id])) {
            $new_filter[] = $filter;
          }
        }
        $url = Url::fromRoute(\Drupal::routeMatch()->getRouteName(), array('filter' => $new_filter));
        $result->setUrl($url);
      }
    }
    if ($children = $result->getChildren()) {
      $term_id = $result->getRawValue();
      $item = $this->prepareLink($result);
      $classes[] = 'facet-parent-'. $term_id;
      $children_markup = [];
      foreach ($children as $child) {
        $term_id = $child->getRawValue();
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term_id);
        $term_name = $term->name->value;
        $child->setDisplayValue($term_name);
        $child_classes = ['facet-item'];
        $children_link = $this->prepareLink($child);
        $child_classes[] = 'facet-child-'. $term_id;
        if ($child->isActive()) {
          $child_classes[] = 'active';
          $children_link['#attributes']['class'][] = 'is-active';
        }
        $children_link['#wrapper_attributes']['class'] = $child_classes;
        $children_markup[] = $children_link;
      }

      $item['children'] = $children_markup;

    }
    else {
      $item = $this->prepareLink($result);
    }
        
    if ($result->isActive()) {
      $classes[] = 'active';
      $item['#attributes']['class'][] = 'is-active';
    }

    //dpm($result);
    $item['#wrapper_attributes']['class'] = $classes;
    $item['#attributes']['data-drupal-facet-item-value'][] = $result->getRawValue();
    $item['#attributes']['data-drupal-facet-item-id'][] = 'group-'. $result->getRawValue();

    return $item;
  }
}
