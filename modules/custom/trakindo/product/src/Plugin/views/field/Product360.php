<?php

namespace Drupal\product\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\product_importer\Service\ProductService;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("product_360")
 */
class Product360 extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $config = \Drupal::config('cron_import.settings');
    $basePath = \Drupal::service('file_system')->realpath($config->get('import_path'));

    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
      $id = $entity->id();
    } else {
      $id = $values->id;
    }

    $entityArr = ProductService::productLoad($id)->toArray();
    $product_id = $entityArr['field_product_id'][0]['value'];
    $product_type = $entityArr['type'][0]['target_id'];
    $map = [
      'power_systems' => 402,
      'equipment' => 406,
      'attachments' => 405
    ];
    $product_file = $entityArr['field_xml_path'][0]['value'];
    
    $build = [];
    $counter = 0;
    if ($product_file) {
      $tmp_path = explode('/', $product_file);

      $products_service = new ProductService();
      $product_xml =  $products_service->parseXML($product_file, ['elementMap' => '{}product']);
      $marketing_content = $product_xml[0]['value']['{}marketing_content'];
      
      foreach ($marketing_content as $data) {
        $attributes = $data['attributes'];
        if (strpos($attributes['type'], 'vpt') !== FALSE) {
          foreach ($data['value'] as $child) {
            $attributes = $child['attributes'];
            if (isset($attributes['type'])) {
              if (strpos($attributes['type'], 'application/shockwave-flash') !== FALSE) {
                $value = $child['value'][0]['value'];
                $counter++;
              }
            }
          }

        }
      }
    }
    if ($counter) {
      $build['product_360'] = array(
        '#markup' => "<div class='hidden exterior-360'>http://h-cpc.cat.com/cmms/vpt-widget?groupid={$tmp_path[7]}&prodid={$product_id}&langid={$language}&view=e&media=f&width=620&sc=J210&hotspot=y</div>",
      );
      if ($counter > 1) {
        $build['product_360_i'] = array(
          '#markup' => "<div class='hidden interior-360'>http://h-cpc.cat.com/cmms/vpt-widget?groupid={$tmp_path[7]}&prodid={$product_id}&langid={$language}&view=i&media=f&width=620&sc=J210&hotspot=y</div>",
        );
      }
    }

    if (!$counter) {
      foreach ($marketing_content as $data) {
        $attributes = $data['attributes'];
        if (strpos($attributes['type'], 'spinset') !== FALSE) {
          foreach ($data['value'] as $child) {
            if ($child['name'] == '{}media') {
              $value = $child['value'][0]['value'];
              if (strpos($value, 'SpinViewer') !== FALSE) {
                $value = $child['value'][0]['value'];
                $value2 = strtolower($child['value'][1]['value']);
                if (strpos($value2, 'exterior')) {
                  $type = 'exterior';
                } else {
                  $type = 'interior';
                }
                $build['product_360_'. $type] = array(
                  '#markup' => "<div class='hidden $type-360'>$value</div>",
                );
              }
            }
          }

        }
      }
    }

    $renderer = $this->getRenderer();
    return $renderer->render($build);
  }

}
