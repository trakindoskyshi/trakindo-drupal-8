<?php

namespace Drupal\product\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("product_compare_ajax_field")
 */
class ProductAjaxLinkField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
      $id = $entity->id();
    } else {
      $id = $values->id;
    }

    $build = [];
    $build['ajax_edit_link'] = array(
      '#theme' => 'links',
      '#links' => array(
        'ajax_edit_link' => array(
          'title' => t('Compare'),
          'url' => Url::fromRoute('product.product_compare_append', ['id' => $id]),
          'attributes' => array(
            'class' => array('use-ajax btn btn-default btn-product')
          ),
        ),
      ),
      '#attached' => array('library' => array('core/drupal.ajax')),
    );

    $renderer = $this->getRenderer();
    return $renderer->render($build);
  }

}
