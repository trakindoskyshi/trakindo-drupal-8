<?php

namespace Drupal\product\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;
use Drupal\product_importer\Service\ProductService;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("product_snapshot")
 */
class ProductSnapshot extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
      $id = $entity->id();
    } else {
      $id = $values->id;
    }

    $entity = ProductService::productLoad($id)->toArray();
    $snapshot_id = $entity['field_snapshot'][0]['target_id'];
    $snapshot_file = file_load($snapshot_id)->toArray();
    $snapshot_file_uri = $snapshot_file['uri'][0]['value'];

    $build = [];
    $build['ajax_edit_link'] = array(
      '#markup' => file_get_contents($snapshot_file_uri),
    );

    $renderer = $this->getRenderer();
    return $renderer->render($build);
  }

}
