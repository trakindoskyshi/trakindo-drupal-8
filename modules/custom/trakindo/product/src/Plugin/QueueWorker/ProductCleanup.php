<?php

namespace Drupal\product\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "product_cleanup",
 *   title = @Translation("Product Cleanup"),
 *   cron = {"time" = 300}
 * )
 */
class ProductCleanup extends QueueWorkerBase {
  /**
  * {@inheritdoc}
  */
  public function processItem($data) {
    $id = $data['id'];
    \Drupal::logger('ProductCleanup')->notice($id);
    $product_type = $data['product_type'];
    $product = ProductService::productLoad($id);
    if ($product && $product->get('type')->target_id == $product_type) {
      $product->status = 0;
      $product->save();
    }
  }
}