<?php

namespace Drupal\product\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product_importer\Service\ProductService;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "product_cache",
 *   title = @Translation("Product Cache"),
 *   cron = {"time" = 300}
 * )
 */
class ProductCache extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
  	$product_id = $data['id'];

  	$product = ProductService::productLoad($product_id);
    $marketing_content = $product->get('field_marketing_content')->getValue();
    foreach ($marketing_content as $key => $value) {
      $id = $value['target_id'];

      $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($id);
      $field_content = $fc->get('field_content')->getValue();
      foreach ($field_content as $content) {
        $content_id = $content['target_id'];
        $fc_content = \Drupal\field_collection\Entity\FieldCollectionItem::load($content_id);
        $title = $fc_content->get('field_title')->getValue();
        $title = $title[0]['value'];

        $media = $fc_content->get('field_media_raw')->getValue();
        if ($media) {
          $media = $media[0]['value'];      
          if (strpos($media, '.com') !== FALSE) {
            $http = substr($media, 0,4);
            if($http != 'http'){
              $media = 'http://'. $media;
            }
          }
          $cid = 'functionality:file_info:'. $media;
          if ($cache = \Drupal::cache()->get($cid)) {
          } else {
            $file_info = functionality_getFileInfo($media);
            \Drupal::cache()->set($cid, $file_info);
          }
        }
      }
    }
  }

}
