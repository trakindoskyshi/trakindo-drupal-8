<?php

namespace Drupal\product\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "product_snapshot",
 *   title = @Translation("Product Snapshot"),
 *   cron = {"time" = 300}
 * )
 */
class ProductSnapshot extends QueueWorkerBase {
  /**
  * {@inheritdoc}
  */
  public function processItem($data) {
    $id = $data['id'];
    $langcode = $data['langcode'];
    //product_snapshot_save($id, $langcode);
  }
}