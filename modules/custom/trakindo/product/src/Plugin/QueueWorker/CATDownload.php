<?php

namespace Drupal\product\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "product_cat_download",
 *   title = @Translation("Product CAT Download"),
 *   cron = {"time" = 500}
 * )
 */
class CATDownload extends QueueWorkerBase {
  /**
  * {@inheritdoc}
  */
  public function processItem($data) {
  	//$languageId = $data['languageId'];

  	$allowed_groups = $data['allowed_groups'];
  	$languageId = 'en';
  	\Drupal::logger('product_cron_cat_import')->notice('start import en');
		$config = \Drupal::config('cron_import.settings');
		$salesChannelCode = "J210";
		//$basePath="../dev_drupal/sites/default/files/product_xml";
		$basePath = \Drupal::service('file_system')->realpath($config->get('import_path'));
		$last_modified = $config->get('import_time_range');

		$cpc = new CPC($salesChannelCode, $languageId, $basePath, $last_modified, $allowed_groups, TRUE);
		$languageId = 'id';
		\Drupal::logger('product_cron_cat_import')->notice('start import id');
		$cpc = new CPC($salesChannelCode, $languageId, $basePath, $last_modified, $allowed_groups, TRUE);
		\Drupal::logger('product_cron_cat_import')->notice('finish import');

		$deleteQueue = \Drupal::queue('product_cleanup');
		$map = [
			402 => 'power_systems',
			406 => 'equipment',
			405 => 'attachments'
		];

		$tree_id = $allowed_groups[0];
		$product_type = $map[$tree_id];
		
    //get current product
		$tree_path = $config->get('import_path'). '/J210/'. $tree_id. 'tree_en.xml';

    $products_service = new ProductService();
  	$tree = $products_service->parseXML($tree_path, ['elementMap' => '{}product_group']);
  	foreach ($tree[0]['value']['{}listofgroups'] as $level1) {
  	  $level1_name = $level1['value']['{}name'];
  	  $level1_id = $level1['attributes']['id']; 
  	  foreach ($level1['value']['{}listofgroups'] as $level2) {
  	    $level2_name = $level2['value']['{}name'];
  	    $level2_id = $level2['attributes']['id'];
  	    foreach ($level2['value']['{}listofproducts'] as $level3) {
  	      $product_id = $level3['attributes']['id'];
  	      $keep_ids[] = $product_id;
  	    }
  	  }
  	}

    //get non-current product
    $tree_path = $config->get('import_path'). '/J210/'. $tree_id. 'tree_en_nc.xml';

    $products_service = new ProductService();
    $tree = $products_service->parseXML($tree_path, ['elementMap' => '{}product_group']);
    foreach ($tree[0]['value']['{}listofgroups'] as $level1) {
      $level1_name = $level1['value']['{}name'];
      $level1_id = $level1['attributes']['id']; 
      foreach ($level1['value']['{}listofgroups'] as $level2) {
        $level2_name = $level2['value']['{}name'];
        $level2_id = $level2['attributes']['id'];
        foreach ($level2['value']['{}listofproducts'] as $level3) {
          $product_id = $level3['attributes']['id'];
          $keep_ids[] = $product_id;
        }
      }
    }
  	$query = \Drupal::entityQuery('products')
  	  ->condition('field_product_id', $keep_ids, 'NOT IN')
      ->condition('type', $product_type)
  	  ->condition('status', 1);
  	$results = $query->execute();
  	$ids = array_keys($results);
  	foreach ($ids as $id) {
  		$data = [ 
	      'id' => $id,
	      'product_type' => $product_type
	    ]; 
  	  $deleteQueue->createItem($data);
  	}
    
  }
}