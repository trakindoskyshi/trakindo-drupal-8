<?php

namespace Drupal\product\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product_importer\Service\ProductService;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "product_import_cron",
 *   title = @Translation("Product Import Cron"),
 *   cron = {"time" = 300}
 * )
 */
class ProductImportCron extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
  	$queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('product_cat_download');
    $queueItems = $queue->numberOfItems();
  	if ($data) {
  		$tree_id = $data['tree_id'];
	  	$group_id = $data['group_id'];
	  	$product_id = $data['product_id'];
	  	$product_path = $data['product_path'];
	  	$tree = $data['tree'];
	  	$language = $data['language'];
	  	
	  	\Drupal::logger('ProductImportCron')->notice('start process id: '. $product_id);

	    $product_service = new ProductService();
		$product_service->setParentId($tree_id);
		$product_service->setProductId($product_id);
		$product_service->setTree($tree);

		$product_data = $product_service->parseXML($product_path);
		$product_service->prepareData($product_data[0]);
		if ($language == 'en') {
			$product_service->createProduct();
		} else {
			$product_service->translateProduct();
		}
		
/*		$product_translate_service = new ProductService();
		$product_translate_service->setParentId($tree_id);
		$product_translate_service->setProductId($product_id);
		$product_translate_service->setTree($tree);
		$product_translate_path = str_replace('en', 'id', $product_path);
		if (file_exists($product_translate_path)) {
			$product_translate_data = $product_translate_service->parseXML($product_translate_path);
			$product_translate_service->prepareData($product_translate_data[0]);
		    $product_translate_service->translateProduct();
		}*/
//		drupal_unlink($product_path);

		//$source = $product_path;
		//$destination = str_replace('unprocessed/', '', $source);
		//file_unmanaged_move($source, $destination, FILE_EXISTS_REPLACE);

		//$source = $product_translate_path;
		//$destination = str_replace('unprocessed/', '', $source);
		//file_unmanaged_move($source, $destination, FILE_EXISTS_REPLACE);
  	}
  	
  }

}
