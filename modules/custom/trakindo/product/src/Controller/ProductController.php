<?php
namespace Drupal\product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\product_importer\Service\ProductService;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\views\Views;

/**
 * Class ProductController.
 *
 * @package Drupal\product\Controller
 */
class ProductController extends ControllerBase {
  public function snapshot($id) {
    $entity = ProductService::productLoad($id)->toArray();
    $snapshot_id = $entity['field_snapshot'][0]['target_id'];
    $snapshot_file = file_load($snapshot_id)->toArray();
    $snapshot_file_uri = $snapshot_file['uri'][0]['value'];
    $data = file_get_contents($snapshot_file_uri);
    print gzdecode($data);
    exit();
  }

  public function compareRemoveAll() {
    unset($_SESSION['product']);
    $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
    if ($previousUrl) {
      return new RedirectResponse($previousUrl);
    } else {
      return new RedirectResponse(\Drupal::url('<front>'));
    }

  }

  public function compareRemoveItem($id) {
    if(($key = array_search($id, $_SESSION['product'])) !== false) {
      unset($_SESSION['product'][$key]);
      $_SESSION['product'] = array_values($_SESSION['product']);
    }
    return new RedirectResponse('/product/compare');
  }

  public function compareRemove($id) {
    $response = new AjaxResponse();
    if(($key = array_search($id, $_SESSION['product'])) !== false) {
      unset($_SESSION['product'][$key]);
      $_SESSION['product'] = array_values($_SESSION['product']);

      for ($i=0; $i < 4; $i++) { 
        if (isset($_SESSION['product'][$i])) {
          $entity_id = $_SESSION['product'][$i];
          $product = ProductService::productLoad($entity_id)->toArray();
          $name = $product['name'][0]['value'];
          $entity_type = $product['type'][0]['target_id'];
          if ($entity_type == 'cat_lift_trucks') {
            $image_id = $product['field_listofimages'][0]['target_id'];
            $file = file_load($image_id);
            $image_uri = $file->get('uri')->value;
            $image_url = ImageStyle::load('product_list')->buildUrl($image_uri);

          } else {
            $image_url = $product['field_image_raw'][0]['value']. '?wid=358&hei=269&op_sharpen=1';
            
          }
          /*$image_uri = $file->get('uri')->value;
          $image_url = ImageStyle::load('product_list')->buildUrl($image_uri);*/
          //$image_url = file_create_url($image_uri);

          $this->ajaxAppendCompare($response, $i, [
            'name' => $name, 
            'image_url' => $image_url,
            'entity_id' => $entity_id,
          ]);
        } else {
          $response->addCommand(new HtmlCommand('.compare-item.js-compare-'. $i,'<div class="compare-empty text-center img-full">
            <img src="/themes/custom/trakindo_2016/images/compare-empty.jpg" alt="">
            <div class="compare-empty-text">
              Add one more item to compare
            </div>
          </div>'));
        }
      }      
    }

    return $response;
  }

  private function ajaxAppendCompare(&$response, $key, $data) {
    $name = $data['name'];
    $entity_id = $data['entity_id'];
    $image_url = $data['image_url'];
    $remove_link = array(
      '#theme' => 'links',
      '#links' => array(
        'ajax_edit_link' => array(
          'title' => t('Remove'),
          'url' => Url::fromRoute('product.product_compare_remove', ['id' => $entity_id]),
          'attributes' => array(
            'class' => array('use-ajax compare-remove')
          ),
        ),
      ),
      '#attached' => array('library' => array('core/drupal.ajax')),
    );
    $response->addCommand(new HtmlCommand('.compare-item.js-compare-'. $key, '<div class="compare-image img-full">
              <img src="'. $image_url. '" alt="">
            </div>
            <div class="compare-title"><div>'. $name. '</div></div>
            <div class="compare-close">
              '. render($remove_link). '
            </div>'));    
  }
//PM200
  public function compareAppend($id) {
//    $_SESSION['product'] = [];
//    if (isset($_SESSION['product']) && count($_SESSION['product']) == 4) $_SESSION['product'] = [];
    if (\Drupal::request()->query->get('_wrapper_format') == 'drupal_ajax') {
      if (!isset($_SESSION['product'])) $_SESSION['product'] = [];

      $response = new AjaxResponse();
      $response->addCommand(new InvokeCommand('.compare-products', 'addClass' ,  ['show-up']));
      if (!in_array($id, $_SESSION['product']) && count($_SESSION['product']) < 4) {
        $_SESSION['product'][] = $id;
      } else {
        $response->addCommand(new HtmlCommand('.compare-warning', 'Max 4 items to compare.'));
        
        /*\Drupal::logger('exclude adding')->notice('<pre>'. print_r($id, true). '</pre>');
        \Drupal::logger('exclude adding')->notice('<pre>'. print_r($_SESSION['product'], true). '</pre>');*/
      }
      foreach ($_SESSION['product'] as $key => $entity_id) {
        $addToCompare = true;
        $product = ProductService::productLoad($entity_id)->toArray();
        $entity_type = $product['type'][0]['target_id'];
        if ($entity_type == 'cat_lift_trucks') {
          $term_id = $product['field_category'][0]['target_id'];
        } else {
          $term_id = $product['field_group'][0]['target_id'];
        }
        \Drupal::logger('product')->notice('<pre>'. print_r($product, true). '</pre>');
        $name = $product['name'][0]['value'];

        $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
        $parent_term = reset($parent);
        if ($key > 0 && $parent_term->id() != $parent_tid) {
          unset($_SESSION['product'][$key]);
          $_SESSION['product'] = array_values($_SESSION['product']);
          $addToCompare = false;
        } else {
          $parent_tid = $parent_term->id();
          //\Drupal::logger('parent_tid')->notice('<pre>'. print_r($parent_tid, true). '</pre>');
        }

        if ($addToCompare) {
          $response->addCommand(new HtmlCommand('.compare-warning', ''));
          if ($entity_type == 'cat_lift_trucks') {
            $image_id = $product['field_listofimages'][0]['target_id'];
            $file = file_load($image_id);
            $image_uri = $file->get('uri')->value;
            $image_url = ImageStyle::load('product_list')->buildUrl($image_uri);

          } else {
            $image_url = $product['field_image_raw'][0]['value']. '?wid=358&hei=269&op_sharpen=1';
            
          }
          //$image_url = file_create_url($image_uri);
          
          $this->ajaxAppendCompare($response, $key, [
            'name' => $name, 
            'image_url' => $image_url,
            'entity_id' => $entity_id,
          ]);
        }
      }
      if (!$addToCompare) {
        $response->addCommand(new HtmlCommand('.compare-warning', 'Only item with same group can be added to compare.'));
      }
      return $response;
    }
  }

  public function compare() {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $ids = $_SESSION['product'];
    $previousUrl = \Drupal::request()->server->get('HTTP_REFERER');
    if (count($ids) == 1) {
      drupal_set_message(t('Compare should have more than 1 product'), 'error');
      if ($previousUrl) {
        return new RedirectResponse($previousUrl);
      } else {
        return new RedirectResponse(\Drupal::url('<front>'));
      }
    }

    $header = [];

    $content = [];
    $products_data = [];
    $data_table = [];
    $entity_type = '';
    foreach ($ids as $id) {
      $products_data['product_id'][] = $id;
      $product = ProductService::productLoad($id)->toArray();
      $name = $product['name'][0]['value'];
      $entity_type = $product['type'][0]['target_id'];
      $path = '/admin/structure/products/'. $id;
      $path_alias = \Drupal::service('path.alias_manager')->getAliasByPath($path, $langcode); 
      if ($path == $path_alias) {
        $path_alias = '/'. $langcode. '/product-detail/'. $id;
      } else {
        $path_alias = '/'. $langcode. '/product-detail'. $path_alias;
      }
      if ($entity_type == 'cat_lift_trucks') {
        $image_id = $product['field_listofimages'][0]['target_id'];
        $file = file_load($image_id);
        $image_uri = $file->get('uri')->value;
        $image_url = ImageStyle::load('product_list')->buildUrl($image_uri);
      } else {
        $image_url = $product['field_image_raw'][0]['value']. '?wid=358&hei=269&op_sharpen=1';
      }
      $header[] = [
        'name' => $name,
        'id' => $id,
        'image' => $image_url,
        'url' => $path_alias
      ];
      if ($entity_type == 'cat_lift_trucks') {
        $field_specifications = $product['field_fork_lift_specification'];
        $field_spec = 'field_fork_lift_spec_detail';
        $field_spec_label = 'field_detail_spec_label';
        $field_spec_data = 'field_detail_value_spec';
      } else {
        $field_specifications = $product['field_specifications'];
        $field_spec = 'field_spec_list';
        $field_spec_label = 'field_spec_label';
        $field_spec_data = 'field_spec_data';
      }
      $target_id = $field_specifications[0]['target_id'];

      $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($target_id);

      $spec_list = $fc->get($field_spec)->getValue();
      
      foreach ($spec_list as $spec_key => $spec) {
        $spec_id = $spec['target_id'];
        $fc_spec = \Drupal\field_collection\Entity\FieldCollectionItem::load($spec_id);
        $spec_data = $fc_spec->get($field_spec_data)->getValue();
        $spec_label_tmp = $fc_spec->get($field_spec_label)->getValue();

        $spec_label = '';
        if (isset($spec_label_tmp[0])) {
          $spec_label = $spec_label_tmp[0]['value'];
        }
        if ($entity_type == 'cat_lift_trucks') {
          $spec_label = $fc_spec->get($field_spec_label)->getValue();
          $spec_label = $spec_label[0]['value'];

          $data_value = (isset($spec_data[0])) ? $spec_data[0]['value'] : '';
          if (!isset($data_table[$spec_label])) {
            $data_table[$spec_label][] = [
              $id => $data_value
            ];
          } else {
            $data_table[$spec_label][0][$id] = $data_value;
          }
        } else { 
          foreach ($spec_data as $data_key => $data) {
            $data_value = $data['value'];
            if (!isset($data_table[$spec_label])) {
              $data_table[$spec_label] = [];
            } else {
              //unset($data_value[0]);
            }
            foreach ($data_value[0] as $field_key => $field_value) {
              if (!isset($data_table[$spec_label][$field_value])) {
                $data_table[$spec_label][$field_value] = [];
              }
              $data_table[$spec_label][$field_value][$id] = $data_value[1][$field_key];
            }
          }
        }
      }
    }

    $form = array(
      '#type' => 'table',
      '#caption' => 'Sample Table'
    );
    for ($i = 1; $i <= 4; $i++) {
      $form[$i]['name'] = array(
        '#markup' => 'Name'.$i,
      );

      $form[$i]['phone'] = array(
        '#markup' => 'Phone'.$i,
      );
    }

    $product_spec = [];
    if ($entity_type != 'cat_lift_trucks') {
      foreach ($data_table as $table_name => &$table_data) { 
        foreach ($table_data as $key => &$value) {
          if ($key > 0) {
            $header_count = count($table_data[0]);
            $value = array_slice($value, 0, $header_count);

          }
        }
      }
    }


    foreach ($data_table as $key => $data_value) {
      //if (count($data_value) == 4) {
      $new_data_value = [];
      foreach ($data_value as $data_key => $value) {
        $new_value = [];
        foreach ($ids as $id) {
          $new_value[] = (isset($value[$id])) ? $value[$id] : '-';
        }
        $row_key = $data_key;
        if ($entity_type == 'cat_lift_trucks') { 
          $row_key = $key;
        }
        $new_data_value[] = array_merge([$row_key], $new_value);
      }
      if (!$new_data_value) continue;

      $product_spec[] = [
        'label' => $key,
        'table' => $new_data_value
      ]; 
      //}
    }

    $products_data['product_image'] = $header;
    $products_data['product_spec'] = $product_spec;
    
    return array(
      '#theme' => 'compare_page',
      '#data' => $products_data
    );
  }
}
