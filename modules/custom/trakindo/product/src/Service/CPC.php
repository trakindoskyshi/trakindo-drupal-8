<?php
namespace Drupal\product\Service;
use Drupal\product_importer\Service\ProductService;

class CPC {
  var $salesChannelCode, $languageId, $basePath, $client, $deletefiles;
  var $productFolder;
  protected $filter_modified;
  protected $addToQueue;

  function __construct($salesChannelCode = "", $languageId = "en", $basePath, $filter_modified = FALSE, $allowed_groups = ['402', '405', '406'], $addToQueue = FALSE) {
    $this->filter_modified = $filter_modified;
    if (!$basePath) {
      throw new \Exception("Please enter a value for basePath in the cpc.php file", 1);
    }
    if (!$salesChannelCode) {
      throw new \Exception("Please enter a value for salesChannelCode in the cpc.php file", 1);
    } else {
      $this->addToQueue = $addToQueue;
      $this->salesChannelCode = $salesChannelCode;
      $this->languageId = $languageId;
      $this->basePath = $basePath;
      $this->client = new \SoapClient("https://cpc.cat.com/ws/services/XmlUpdate?wsdl", array("trace" => 1, "exception" => 0));

      $this->makeFolder($this->basePath);
      $this->makeFolder($this->basePath."/".$this->salesChannelCode);
      $this->makeFolder($this->basePath."/".$this->languageId);

      $this->saveFile($this->basePath."/log_".$this->languageId.".txt","");

      $this->deletefiles=glob($this->basePath."/".$this->languageId."/*/*.xml");

      $this->getDisplayConstants();
      $this->getClasses($allowed_groups);

      $this->logToFile("\r\n");

      foreach($this->deletefiles as $f) {
       //unlink($f);
       $this->logToFile("\r\nDeleted: ".$f);
      }
    }
  }

  function getDisplayConstants() {

    $result = $this->client->__soapCall("getDisplayConstants", array(
      "getDisplayConstants" => array(
      "LanguageId"=>$this->languageId
    )));

    $this->saveFile($this->basePath."/".$this->languageId."/displayconstants_".$this->languageId.".xml",$this->removeSOAPEnvelope($this->client->__getLastResponse()));

  }

  function getClasses($allowed_groups) {
    $result = $this->client->__soapCall("getClasses", array(
      "getClasses" => array(
      "SalesChannelCode" => $this->salesChannelCode,
      "LanguageId" => $this->languageId
    )));

    $this->saveFile($this->basePath."/".$this->salesChannelCode."/classes_".$this->languageId.".xml",$this->removeSOAPEnvelope($this->client->__getLastResponse()));
    foreach ($result->cmms->listofgroups->product_group as $class) {
      if (!in_array($class->id, $allowed_groups)) continue;
      $this->logToFile("\r\n".$class->name->_." (".$class->id.")\r\n");
      $this->getTree($class->id);
      $this->getTree($class->id,"N");
    }
  }

  function getTree($id, $current = "Y") {
    $result = $this->client->__soapCall("getTree", array(
      "getTree" => array(
      "SalesChannelCode" => $this->salesChannelCode,
      "LanguageId" => $this->languageId,
      "id" => $id,
      "current" => $current
    )));

    $treeResponse = $this->removeSOAPEnvelope($this->client->__getLastResponse());

    if ($current == "Y") { 
      $currentFlag = ""; 
    } else { 
      $currentFlag = "_nc"; 
    }
    $path = $this->basePath."/".$this->salesChannelCode."/".$id."tree_".$this->languageId.$currentFlag.".xml";

    if ($treeResponse == "") {
      if (file_exists($path)) {
        unlink($path);
      }
      $this->logToFile("Deleted: ".$path."\r\n");
      return;
    }

    $level1current = array();
    $level2current = array();
    $level3current = array();
    $level4current = array();

    if (file_exists($path)) {
      $xml = simplexml_load_file($path);
      $level1id = (string) $xml->product_group->attributes()->id;
      if (file_exists($this->basePath."/".$this->languageId. "/". $level1id."_".$this->languageId.".xml")) {
        $level1current = array($level1id."\t".(string) $xml->product_group->name."\t".(string) $xml->product_group->last_modified);
      }
      $groups = $xml->product_group->listofgroups->product_group;

      foreach ($groups as $level2) {
        $level2id = (string) $level2->attributes()->id;
        if (file_exists($this->basePath."/".$this->languageId."/". $id ."/".$level2id."_".$this->languageId.".xml")) {
          array_push($level2current,$level2id."\t".(string) $level2->name."\t".(string) $level2->last_modified);
        }
        $groups2 = $level2->listofgroups->product_group;

        foreach ($groups2 as $level3) {
          $level3id = (string) $level3->attributes()->id;
          if (file_exists($this->basePath."/".$this->languageId. "/". $id . "/". $level2id. "/". $level3id."_".$this->languageId.".xml")) {
            array_push($level3current,$level3id."\t".(string) $level3->name."\t".(string) $level3->last_modified);
          }
          $products = $level3->listofproducts->product;
          foreach ($products as $level4) {
            $level4id = (string) $level4->attributes()->id;
            if (file_exists($this->basePath."/".$this->languageId. "/". $id . "/". $level2id. "/".$level3id."/".$level4id."_".$this->languageId.".xml")) {
              array_push($level4current,$level4id."\t".(string) $level4->nondisplayname."\t".(string) $level4->last_modified."\t".(string) $level3id);
            }
          }
        }
      }
    }

    $level1new = array();
    $level2new = array();
    $level3new = array();
    $level4new = array();

    $level1id = $result->cmms->product_group->id;
    $level = 1;
    $level1sort = $result->cmms->product_group->sort;
    $level1name = $result->cmms->product_group->name->_;
    $null = null;

    $level1new=array($level1id."\t".$level1name."\t".$result->cmms->product_group->last_modified);
    $this->keepFile($level1id);

    $groups = $result->cmms->product_group->listofgroups->product_group;
    if (gettype($groups) != "array") {
      $groups = (array) $result->cmms->product_group->listofgroups;
    }

    $folder = [];
    $productFolder[$id] = [];
    foreach ($groups as $level2) {
      $level = 2;
      $level2id = $level2->id;
      $folder[$level2id] = [$id];
      $productFolder[$level2id] = [
      $id
      ];
      $level2sort = $level2->sort;
      $level2name = $level2->name->_;

      array_push($level2new,$level2id."\t".$level2name."\t".$level2->last_modified);
      $this->keepFile($level2id);

      $groups2 = $level2->listofgroups->product_group;
      if (gettype($groups2) != "array") {
        $groups2 = (array) $level2->listofgroups;
      }

      foreach ($groups2 as $level3) {
        $level = 3;
        $level3id = $level3->id;
        $folder[$level2id][$level3id] = [];
        $productFolder[$level3id] = [
          $id,
          $level2id,
        ];

        $level3sort = $level3->sort;
        $level3name = $level3->name->_;

        array_push($level3new,$level3id."\t".$level3name."\t".$level3->last_modified);
        $this->keepFile($level3id);

        $products = $level3->listofproducts->product;
        if (gettype($products) != "array") {
          $products = (array) $level3->listofproducts;
        }

        foreach ($products as $level4) {
          $level = 4;
          $level4id = $level4->id;
          //$folder[$level2id][$level3id][] = $level4id;
          $productFolder[$level4id] = [
            $id,
            $level2id,
            $level3id
          ];
          $level4sort = $level4->sort;
          $level4name = $level4->nondisplayname->_;

          if ($this->filter_modified) {
            $filter_modified = $this->filter_modified;
            $last_modified = strtotime($level4->last_modified);
            if ($last_modified > strtotime($filter_modified)) {
              array_push($level4new,$level4id."\t".$level4name."\t".$level4->last_modified."\t".$level3id);
              $this->keepFile($level4id,$level3id);
            }
          } else {
            array_push($level4new,$level4id."\t".$level4name."\t".$level4->last_modified."\t".$level3id);
            $this->keepFile($level4id,$level3id);
          }
        }
      }
    }

    $this->productFolder = $productFolder;
    $this->getGroupDetail(array_diff($level1new,$level1current));
    $this->getGroupDetail(array_diff($level2new,$level2current));
    $this->getGroupDetail(array_diff($level3new,$level3current));
    $this->getProductDetail(array_diff($level4new,$level4current));
    $this->saveFile($path,$treeResponse);
  }

  function getGroupDetail($a) {
    foreach($a as $val) {
      $pos = strpos($val,"\t");
      $id = substr($val,0,$pos);
      $productFolder = $this->productFolder;
      //$productFolder[$id][] = $id;

      $this->logToFile($id."\r\n");

      $last_folder = '';
      $flag = 0;
      foreach ($productFolder[$id] as $folder) {
        $last_folder .= $folder . '/';
        $path = $this->basePath."/".$this->languageId."/".$last_folder;
        $this->makeFolder($path);
      }
      $complete_path = implode('/', $productFolder[$id]);

      try {
        $result = $this->client->__soapCall("getGroupDetail", array(
          "getGroupDetail" => array(
          "SalesChannelCode" => $this->salesChannelCode,
          "LanguageId" => $this->languageId,
          "id" => $id
        )));
      } catch(\SoapFault $sf) {}

      $pth = $this->basePath."/".$this->languageId."/".$complete_path."/".$id."_".$this->languageId.".xml";
      $rsp = $this->removeSOAPEnvelope($this->client->__getLastResponse());
      if ($rsp=="") {
        if (file_exists($pth)) {
          unlink($pth);
        }
        $this->logToFile("Deleted: ".$pth."\r\n");
      } else {
        $this->saveFile($pth,$rsp);
      }
    }
  }

  function getProductDetail($a) {
    foreach($a as $val) {
      $pos = strpos($val,"\t");
      $id = substr($val,0,$pos);
      $productFolder = $this->productFolder;

      $last_folder = '';
      foreach ($productFolder[$id] as $folder) {
        $last_folder .= $folder . '/';
        $path = $this->basePath."/".$this->languageId."/".$last_folder;
        $this->makeFolder($path);
      }
      $complete_path = implode('/', $productFolder[$id]);

      $pos = strrpos($val,"\t")+1;
      $group = substr($val,$pos);

      $this->logToFile($group."/".$id."\r\n");

      //$this->makeFolder($this->basePath."/".$this->languageId."/".$group);

      try {
        $result = $this->client->__soapCall("getProductDetail", array(
          "getProductDetail" => array(
          "SalesChannelCode" => $this->salesChannelCode,
          "LanguageId" => $this->languageId,
          "GroupId" => $group,
          "id" => $id
        )));
      } catch(\SoapFault $sf) {}

      $pth = $this->basePath."/".$this->languageId."/".$complete_path."/".$id."_".$this->languageId.".xml";
      $rsp = $this->removeSOAPEnvelope($this->client->__getLastResponse());
      if ($rsp == "") {
        if (file_exists($pth)) {
          unlink($pth);
        }
        $this->logToFile("Deleted: ".$pth."\r\n");
      } else {
        $this->saveFile($pth, $rsp);
        if ($this->addToQueue) {
          $tree_id = $productFolder[$id][0];
          $group_id = $productFolder[$id][2];

          $product_id = $id;
          
          $products_service = new ProductService();
          $tree = [];
          $drupal_uri = $this->basePath. '/'. $this->languageId. '/';
          $group_folder = $drupal_uri;
          foreach ($productFolder[$id] as $parent_xml) {
            $xml_file = $group_folder. $parent_xml. '_'. $this->languageId. '.xml';
            if (!isset($group_cache[$parent_xml])) {
              $xml = $products_service->parseXML($xml_file, ['elementMap' => '{}product_group']);
              $group_name = $xml[0]['value']['{}name'];
              $group_cache[$parent_xml] = $group_name;
            } else {
              $group_name = $group_cache[$parent_xml];
            }
            $tree[] = $group_name;
            $group_folder .= $parent_xml .'/';
          }

          
          $queue = \Drupal::queue('product_import_cron');
          $data = [ 
            'tree_id' => $tree_id, 
            'group_id' => $group_id, 
            'product_id' => $product_id, 
            'product_path' => $pth, 
            'tree' => $tree,
            'language' => $this->languageId
          ];
          \Drupal::logger('product_import_cron data')->notice(print_r($data, true));
          $queue->createItem($data);
        }
      }
    }
  }

  function makeFolder($path) {
    if (!is_dir($path)) {
      mkdir($path);
    }
  }

  function saveFile($path,$content) {
    file_put_contents($path, $content);
  }

  function removeSOAPEnvelope($s) {
    if (strpos($s,"<cmms/>") !== false) {
      return "";
    }

    $pos = strrpos($s,"</cmms>")+7;
    $s = substr($s,0,$pos);
    $pos = strpos($s,"<cmms>");
    $s = substr($s,$pos);

    return '<?xml version="1.0" encoding="UTF-8"?>'.$s;
  }

  function keepFile($id, $group=NULL) {
    if (is_null($group)) {
      $group = $id;
    }
    $productFolder = $this->productFolder;

    if (isset($productFolder[$id])) {
      $productFolder[$id][] = $id;
      $complete_path = implode('/', $productFolder[$id]);
    } else {
      $complete_path = $group;
    }

    $path = $this->basePath."/".$this->languageId."/".$complete_path."/".$id."_".$this->languageId.".xml";

    if(($key = array_search($path, $this->deletefiles)) !== false) {
      unset($this->deletefiles[$key]);
    }
  }

  function logToFile($data) {
    //file_put_contents($this->basePath."/log_".$this->languageId.".txt", $data, FILE_APPEND);
  }
}