<?php
namespace Drupal\product\Service;

class ProductMedia {
  public static function getMediaInfo($media) {
    $db = \Drupal::database();
    $data = $db->select('product_media_info', 't')
      ->fields('t')
      ->condition('url', $media)
      ->execute();
    return $data->fetchAssoc();
  }
	public static function setMediaInfo($media) {
    $check = self::getMediaInfo($media);
    if ($check) {
      $file_info = self::getFileInfo($media);
      $query = \Drupal::database()->update('product_media_info');
      $query->fields([
        'url' => $media,
        'size' => $file_info['size'],
        'type' => $file_info['type']
      ]);
      $query->condition('event', 'My event');
      $query->execute();
    } else {
      $file_info = self::getFileInfo($media);
      $query = \Drupal::database()->insert('product_media_info');
      $query->fields([
        'url',
        'size',
        'type'
      ]);
      $query->values([
        $media,
        $file_info['size'],
        $file_info['type']
      ]);
      $query->execute();
    }

    
	}
  private static function getFileInfo($url) {
    if (strpos($url, 'youtu') !== FALSE) {
      $type = 'video';
      $size = '~';
      return [
        'size' => $size,
        'type' => $type
      ];
    } else {
      $ch = curl_init(); 
      curl_setopt($ch, CURLOPT_HEADER, true); 
      curl_setopt($ch, CURLOPT_NOBODY, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
      curl_setopt($ch, CURLOPT_URL, $url); //specify the url
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

      $head = curl_exec($ch);

      $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
      $type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
      $type = str_replace('application/', '', $type);
      $type = str_replace('image/', '', $type);
      
      $clen = $size;
      switch ($clen) {
          case $clen < 1024:
              $size = $clen .' B'; break;
          case $clen < 1048576:
              $size = round($clen / 1024, 2) .' KiB'; break;
          case $clen < 1073741824:
              $size = round($clen / 1048576, 2) . ' MiB'; break;
          case $clen < 1099511627776:
              $size = round($clen / 1073741824, 2) . ' GiB'; break;
      }
      curl_close($ch);
      return [
        'size' => $size,
        'type' => $type
      ];

    }
  }
}