/**
 * @file
 * Handles AJAX fetching of views, including filter submission and response.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';
  var flag = 0;
  Drupal.ajax_facets = Drupal.ajax_facets || {viewDomId: null, viewSettings: null};
  Drupal.behaviors.facetsAjaxWidget = {
    attach: function (context, drupalSettings) {
      console.log('attach');
      drupalSettings.flag = flag;
      flag++;
      Drupal.ajax_facets.attach(context, drupalSettings);
    }
  };

  /**
   * Trying to find DomID for our view.
   */
  Drupal.ajax_facets.attach = function (context, drupalSettings) {
    console.log('ajax_facets.attach');
    $.each(drupalSettings.views.ajaxViews, function( key, value ) {
      if (drupalSettings.ajaxFacets.view_name.indexOf(value.view_name) > -1) {
        Drupal.ajax_facets.viewDomId = key;
        Drupal.ajax_facets.viewSettings = value;
      }
    });

    var $links = $('.js-facets-ajax-links-checkboxes .facet-item a');
    $links.once('facets-ajax-ajaxify-'+ drupalSettings.flag).each(Drupal.ajax_facets.makeAjaxCheckboxFacet);

  };

  /**
   * Replace a link with a checked checkbox.
   */
  Drupal.ajax_facets.makeAjaxCheckboxFacet = function () {
    console.log('makeAjaxCheckboxFacet');
    var $link = $(this);
    var active = $link.hasClass('is-active');
    console.log('$link', $link);
    console.log('active', active);
    var description = $link.html();
    var href = $link.attr('href');
    var id = $link.data('drupal-facet-item-id');

    $("input:checkbox[id='"+ id + "']").remove();
    var checkbox = $('<input type="checkbox" class="facets-checkbox" id="' + id + '" data-facet-query="' + $(this).attr('data-facet-query') + '" />');
    $("label[for='"+ id+ "']").remove();
    var label = $('<label for="' + id + '">' + description + '</label>');

    //$link.removeAttr('href');
    checkbox.on('change', function (e) {
      $(this).toggleClass('active');
      var facet_query = [];
      $('.js-facets-ajax-links .active').each(function(){
        facet_query.push($(this).attr('data-facet-query'));
      });
      Drupal.ajax_facets.ajaxView(Drupal.ajax_facets.viewSettings, facet_query);
      e.preventDefault();
    });

    $link.before(checkbox).before(label).hide();
  }

  /**
   * Javascript object for a certain view.
   *
   * @constructor
   *
   * @param {object} settings
   *   Settings object for the ajax view.
   * @param {string} settings.view_dom_id
   *   The DOM id of the view.
   */
  Drupal.ajax_facets.ajaxView = function (settings, facet_query) {
    var selector = '.js-view-dom-id-' + settings.view_dom_id;
    this.$view = $(selector);
    
    // Retrieve the path to use for views' ajax.
    var ajax_path = drupalSettings.views.ajax_path;

    // If there are multiple views this might've ended up showing up multiple
    // times.
    if (ajax_path.constructor.toString().indexOf('Array') !== -1) {
      ajax_path = ajax_path[0];
    }
    console.log('ajax_path', ajax_path);

    // Check if there are any GET parameters to send to views.
    var queryString = window.location.search || '';
    if (queryString !== '') {
      // Remove the question mark and Drupal path component if any.
      queryString = queryString.slice(1).replace(/q=[^&]+&?|&?render=[^&]+/, '');
      if (queryString !== '') {
        // If there is a '?' in ajax_path, clean url are on and & should be
        // used to add parameters.
        queryString = ((/\?/.test(ajax_path)) ? '&' : '?') + queryString;
      }
    }

    // Add facets to query.
    settings.filter = facet_query;

    this.element_settings = {
      url: ajax_path + queryString,
      submit: settings,
      setClick: true,
      event: 'click',
      selector: selector,
      progress: {type: 'fullscreen'}
    };

    this.settings = settings;

    // Add the ajax to exposed forms.
    this.$exposed_form = $('form#views-exposed-form-' + settings.view_name.replace(/_/g, '-') + '-' + settings.view_display_id.replace(/_/g, '-'));
    this.$exposed_form.once('exposed-form').each($.proxy(this.attachExposedFormAjax, this));

    // Add the ajax to pagers.
    this.$view
      // Don't attach to nested views. Doing so would attach multiple behaviors
      // to a given element.
      .filter($.proxy(this.filterNestedViews, this))
      .once('ajax-pager').each($.proxy(this.attachPagerAjax, this));

    // Add a trigger to update this view specifically. In order to trigger a
    // refresh use the following code.
    //
    // @code
    // $('.view-name').trigger('RefreshView');
    // @endcode
    var self_settings = $.extend({}, this.element_settings, {
      event: 'RefreshView',
      base: this.selector,
      element: this.$view.get(0)
    });
    console.log('self_settings', self_settings);

    this.refreshViewAjax = Drupal.ajax(self_settings);
    this.refreshViewAjax.execute();

  };

})(jQuery, Drupal, drupalSettings);
