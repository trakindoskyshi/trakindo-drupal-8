<?php
use Drupal\product_importer\Service\ProductService;
use Drupal\product\Service\ProductMedia;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\functionality\Form\JobImporterForm;

function functionality_batch_job_import($job_url, &$context) {
  $job_content = product_getPage($job_url);
  preg_match_all('/<code id="([^"]+)"><!--(.+?(?=--\>))/', $job_content, $job_matches);
  // convert all code values to readable json code
  if ($job_content && isset($job_matches[1])) {
    $jobs = [];
    foreach ($job_matches[1] as $key => $label) {
      $jobs[$label] = json_decode($job_matches[2][$key]);
    }
    
    $job_share = $jobs['shareModule'];
    $job_description = $jobs['jobDescriptionModule']->description;
    $job_title = $job_share->jobPosting->title;
    $job_url = $job_share->pageUrl;
    $decorated_job = $jobs['decoratedJobPostingModule']->decoratedJobPosting;

    $job_function = $decorated_job->formattedJobFunctions;
    $job_field = [];
    foreach ($job_function as $value) {
      $vid = 'job_field';
      $job_field[] = JobImporterForm::saveTerm($vid, $value);
    }
    $job_schedule = $decorated_job->formattedEmploymentStatus;
    $location = $decorated_job->formattedLocation;
    $tmp = explode(',', $location);

    $job_location = null;
    if (count($tmp) > 1) {
      $vid = 'job_location';
      $parent_tid = JobImporterForm::saveTerm($vid, $tmp[1]);
      $job_location = JobImporterForm::saveTerm($vid, $tmp[0], $parent_tid);
    }
    $posting = $decorated_job->formattedListDate;
    $tmp = strtotime($posting);
    $job_posting = date('Y-m-d', $tmp);
    $job_career_level = $decorated_job->formattedExperience;

    if ($job_title) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'job')
        ->condition('field_url', $job_url);
      $entity_ids = $query->execute();
      if (count($entity_ids)) {
        $id = reset($entity_ids);
        $node = Node::load($id);
        $node->body->value = $job_description;
        $node->body->format = 'full_html';
        $node->title = $job_title;
        $node->field_career_level = $job_career_level;
        $node->field_job_field = $job_field;
        $node->field_job_location = $job_location;
        $node->field_job_posting = $job_posting;
        $node->field_url = $job_url;
        $node->field_schedule = $job_schedule;
        $node->save();
      } else {
        $node = Node::create([
          'type' => 'job',
          'title' => $job_title,
          'body' => [
            'value' => $job_description,
            'format' => 'full_html'
          ],
          'field_career_level' => $job_career_level,
          'field_job_field' => $job_field,
          'field_job_location' => $job_location,
          'field_job_posting' => $job_posting,
          'field_url' => $job_url,
          'field_schedule' => $job_schedule
        ]);
        $node->save();
      }
    }
  }
  

  $context['results'][] = $job_url;  
  $context['message'] = 'Job URL: '. $job_url;
}

function functionality_set_product_cache($product_id, &$context) {
  $product = ProductService::productLoad($product_id);
  $marketing_content = $product->get('field_marketing_content')->getValue();
  foreach ($marketing_content as $key => $value) {
    $id = $value['target_id'];

    $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($id);
    $field_content = $fc->get('field_content')->getValue();
    foreach ($field_content as $content) {
      $content_id = $content['target_id'];
      $fc_content = \Drupal\field_collection\Entity\FieldCollectionItem::load($content_id);
      $title = $fc_content->get('field_title')->getValue();
      $title = $title[0]['value'];

      $media = $fc_content->get('field_media_raw')->getValue();
      if ($media) {
        $media = $media[0]['value'];      
        if (strpos($media, '.com') !== FALSE) {
          $http = substr($media, 0,4);
          if($http != 'http'){
            $media = 'http://'. $media;
          }
        }
        $cid = 'functionality:file_info:'. $media;
        if ($cache = ProductMedia::getMediaInfo($media)) {
        } else {
          //$file_info = functionality_getFileInfo($media);
          ProductMedia::setMediaInfo($media);
        }
      }
    }
  }

  $context['results'][] = $product_id;  
  $context['message'] = 'Set Product Cache for ID: '. $product_id;
}

function functionality_batch_finished_callback($success, $results, $operations) {
  if ($success) {
    $message = \Drupal::translation()->formatPlural(
      count($results),
      'One product processed.', '@count products processed.'
    );
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  //$_SESSION['disc_migrate_batch_results'] = $results;
}