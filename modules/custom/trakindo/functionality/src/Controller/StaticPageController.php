<?php
namespace Drupal\functionality\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing;
use LinkedIn\LinkedIn;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Class StaticPageController.
 *
 * @package Drupal\functionality\Controller
 */
class StaticPageController extends ControllerBase {
  
  public function linkedin() {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $configdata = \Drupal::service('config.factory')->getEditable('linkedin.config');
    $api_key = $configdata->get('api_key');
    $api_secret = $configdata->get('api_secret');
    $callback_url = $configdata->get('callback_url');
    if (!$api_key && !$api_secret && !$callback_url) {
      return new TrustedRedirectResponse($host.'/admin/functionality/form/pull_job_linkedin');
    }

    $li = new LinkedIn(
      array(
        'api_key' => $api_key, 
        'api_secret' => $api_secret, 
        'callback_url' => $callback_url
      )
    );

    $token = $li->getAccessToken($_REQUEST['code']);
    //$token_expires = $li->getAccessTokenExpiration();
    $expired_date = date('Y-m-d', strtotime('+1 days'));
    $config = \Drupal::service('config.factory')->getEditable('linkedin.token');
    $config
      ->set('token', $token)
      ->set('token_expires', $expired_date)
      ->save();
  
   return new TrustedRedirectResponse($host.'/admin/functionality/form/pull_job_linkedin');
  }

  public function thankYou() {
    $query = \Drupal::request()->query->all();
    $id = $query['id'];
    
    $form_id = (isset($query['form_id'])) ? $query['form_id'] : '';
    $data = [];
    
    $entity = \Drupal::entityTypeManager()->getStorage('contact_message')->load($id);
    foreach ($entity->toArray() as $key => $value) {
      if (strpos($key, 'field_') !== FALSE ) {
        $key = str_replace('field_', '', $key);
        if (count($value) > 1) {
          $data[$key] = [];
          foreach ($value as $value) {
            $data[$key][] = $value['value'];
          }
        } else {
          if (isset($value[0]['value'])) {
            $data[$key] = $value[0]['value'];
          }
        }   
      }
      if ($key == 'message') {
        if (isset($value[0]['value'])) {
          $data[$key] = $value[0]['value'];
        }
      }
    }

    // set default theme
    $theme = 'thank_you_page';

    if ($entity->bundle() == 'newsletter') {
      // submit to newsletter
      $subscriber = simplenews_subscriber_load_by_mail($data['business_email']);
      if (!empty($subscriber)) {
        $subscriptions = $subscriber->getSubscribedNewsletterIds();
      }
     //dpm($subscriber->subscriptions);
      $confirmed = (empty($subscriptions)) ? 0 : 1;
      // find if user ever confirm this email
      /*$confirmed = FALSE;
      foreach ($subscriber['*values']['subscriptions']['x-default'] as $subscribe) {
        if ($subscribe['status'] == 1) $confirmed = TRUE;
       }*/
      $newsletters = (is_array($data['newsletter_category'])) ? $data['newsletter_category'] : array($data['newsletter_category']);
      $subscription_manager = \Drupal::service('simplenews.subscription_manager');
      foreach ($newsletters as $newsletter) {
        if (!$confirmed) {
          $subscription_manager->subscribe($data['business_email'], $newsletter, 2);
          $theme = 'thank_you_page_newsletter_nonconfirmed';
        } else {
          $subscription_manager->subscribe($data['business_email'], $newsletter, 1);
          $theme = 'thank_you_page_newsletter';
        }
      }
    }

    return array(
      '#theme' => $theme,
      '#form_id' => $form_id,
      '#data' => $data
    );
  }
}
