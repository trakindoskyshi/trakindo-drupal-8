<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use PHPExcel_IOFactory;
use PHPExcel;
use Drupal\node\Entity\Node;
use Drupal\product_importer\Service\ProductService;

/**
 * Class FormRentalData.
 *
 * @package Drupal\functionality\Form
 */
class FormRentalData extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_rental_data';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['location_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Location XLS file'),
      '#upload_validators' => [
        'file_validate_extensions' => [
          'xls xlsx'
        ]
      ]
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    require_once('vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
    require_once('vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');

    $objPHPExcel = new \PHPExcel();
    // Add some data
    $objPHPExcel->setActiveSheetIndex(0);
    $dataArray = [
      [
        '',
        'Not Found',
      ]
    ];
    $objPHPExcel->getActiveSheet()->setTitle('Rental Data XLS');


    $submit = $form_state->getValue('op');
    if ($submit == 'Submit') {
      $location_file = $form_state->getValue('location_file');
      $fid = $location_file[0];
      $file =  \Drupal\file\Entity\File::load($fid);
      $uri = $file->getFileUri();

      $real_path = \Drupal::service('file_system')->realpath($uri);
      $objPHPExcel = PHPExcel_IOFactory::load($real_path);
      foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $rows = $worksheet->toArray();
        $i = 0;
        foreach ($rows as $index => $row) {
          $i++;
          $model = $row[0];
          if (!$model) continue;
            $product = ProductService::productLoadByName($model);
            if ($product) {
              if ($product->get('field_rental_service')->value != 'Yes') {
                $product->field_rental_service = 'Yes';
                $product->save();
              }
            }else{
              if($model != 'Model'){
                $dataArray[] = [
                  '',
                  $model,
                ];
              }
            }
          }
        }
        $objPHPExcel->getActiveSheet()->fromArray($dataArray);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="rental-model.xls"');
        header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
    // If you're serving to IE over SSL, then the following may be needed
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
  }
}
