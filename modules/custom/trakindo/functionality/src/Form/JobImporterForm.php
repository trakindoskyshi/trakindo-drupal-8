<?php
namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use JonnyW\PhantomJs\Client;
use \Drupal\node\Entity\Node;
use \Drupal\file\Entity\File;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class JobImporterForm.
 *
 * @package Drupal\functionality\Form
 */
class JobImporterForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'job_importer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('job_import.settings');

    $form['linkedin_url'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => t('LinkedIn URL'),
      '#description' => t('LinkedIn URL to be crawled'),
      '#required' => TRUE,
      '#default_value' => ($config->get('linkedin_url')) ? $config->get('linkedin_url') : 'https://www.linkedin.com/jobs/search?countryCode=id&locationId=id%3A0&f_C=6077570%2C60359%2C6303872%2C685810%2C685810%2C2846044%2C15302&trk=jobs_jserp_add_facet_company&orig=FCTD'
    );
    $form['save'] = [
        '#type' => 'submit',
        '#value' => t('Save URL Config'),
    ];
    $form['import'] = [
        '#type' => 'submit',
        '#value' => t('Import'),
    ];

    return $form;
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'job_import.settings',
    ];
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  public static function saveTerm($vid, $term_name, $parent_id = null) {
    $term_name = trim($term_name);
    $terms =  taxonomy_term_load_multiple_by_name($term_name, $vid);
    if (count($terms)) {
      $term = reset($terms);
      $term_id = $term->tid->value;
    } else {
      if (strlen($term_name) > 180) {
        $pos=strpos($term_name, ' ', 180);
        if ($pos) {
          $term_name = substr($term_name, 0, $pos );
        }
      }
      $term_data = [
        'name' => $term_name,
        'vid' => $vid
      ];
      if ($parent_id) {
        $term_data['parent'] = $parent_id;
      }
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $term_id = $term->tid->value;
    }
    return $term_id;
  }

  private static function getNextPage($url) {
    $content = product_getPage($url);
    preg_match('/<code id="decoratedJobPostingsModule"><!--(.+?(?=--\>))/', $content, $matches);
    $json = (array) json_decode($matches[1]);
    $paging = $json['paging'];
    $links = $paging->links;

    $data = $json['elements'];
    if ($links->next) {
      $next_url = $links->next;
      $next_data = self::getNextPage("https://www.linkedin.com". $next_url);

      $data = array_merge($data, $next_data);
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = $form_state->getValue('op');
    $url = $form_state->getValue('linkedin_url');
    $this->config('job_import.settings')
      ->set('linkedin_url', $url)
      ->save();

    if ($op == 'Import') {
      //$url = "https://www.linkedin.com/jobs/search?countryCode=id&locationId=id%3A0&f_C=6077570%2C60359%2C6303872%2C685810%2C685810%2C2846044%2C15302&trk=jobs_jserp_add_facet_company&orig=FCTD";
      //$url = "https://www.linkedin.com/jobs/search?f_C=2846044&locationType=Y&orig=FCTD&trk=jobs_jserp_facet_c_id";
      $content = product_getPage($url);
      preg_match('/<code id="decoratedJobPostingsModule"><!--(.+?(?=--\>))/', $content, $matches);

      $json = (array) json_decode($matches[1]);
      $jobs = array();
      $paging = $json['paging'];
      $links = $paging->links;

      $data = $json['elements'];

      if ($links->next) {
        $next_url = $links->next;
        $next_data = self::getNextPage("https://www.linkedin.com". $next_url);
        $data = array_merge($data, $next_data);
      }
      
      foreach ($data as $id => $job) {
        $job_url = $job->viewJobTextUrl;
        
        /*$job_content = product_getPage($job_url);
        preg_match_all('/<code id="([^"]+)"><!--(.+?(?=--\>))/', $job_content, $job_matches);
        // convert all code values to readable json code
        $job_content = product_getPage($job_url);
        preg_match_all('/<code id="([^"]+)"><!--(.+?(?=--\>))/', $job_content, $job_matches);
        // convert all code values to readable json code
        foreach ($job_matches[1] as $key => $label) {
          $jobs[$label] = json_decode($job_matches[2][$key]);
        }
        
        $job_share = $jobs['shareModule'];
        $job_description = $jobs['jobDescriptionModule']->description;
        $job_title = $job_share->jobPosting->title;
        $job_url = $job_share->pageUrl;
        print_r($job_url);
        $decorated_job = $jobs['decoratedJobPostingModule']->decoratedJobPosting;

        $job_function = $decorated_job->formattedJobFunctions;
        $job_field = [];
        foreach ($job_function as $value) {
          $vid = 'job_field';
          $job_field[] = JobImporterForm::saveTerm($vid, $value);
        }
        $job_schedule = $decorated_job->formattedEmploymentStatus;
        $location = $decorated_job->formattedLocation;
        $tmp = explode(',', $location);

        $vid = 'job_location';

        $parent_tid = JobImporterForm::saveTerm($vid, $tmp[1]);
        $job_location = JobImporterForm::saveTerm($vid, $tmp[0], $parent_tid);
        $posting = $decorated_job->formattedListDate;
        $tmp = strtotime($posting);
        $job_posting = date('Y-m-d', $tmp);
        $job_career_level = $decorated_job->formattedExperience;

        $query = \Drupal::entityQuery('node')
          ->condition('type', 'job')
          ->condition('field_url', $job_url);
        $entity_ids = $query->execute();
        if (count($entity_ids)) {
          $id = reset($entity_ids);
          $node = Node::load($id);
          $node->body->value = $job_description;
          $node->body->format = 'full_html';
          $node->title = $job_title;
          $node->field_career_level = $job_career_level;
          $node->field_job_field = $job_field;
          $node->field_job_location = $job_location;
          $node->field_job_posting = $job_posting;
          $node->field_url = $job_url;
          $node->field_schedule = $job_schedule;
          $node->save();
        } else {
          $node = Node::create([
            'type' => 'job',
            'title' => $job_title,
            'body' => [
              'value' => $job_description,
              'format' => 'full_html'
            ],
            'field_career_level' => $job_career_level,
            'field_job_field' => $job_field,
            'field_job_location' => $job_location,
            'field_job_posting' => $job_posting,
            'field_url' => $job_url,
            'field_schedule' => $job_schedule
          ]);
          $node->save();
        }*/

        $operations[] = array('functionality_batch_job_import', array($job_url));
      }

      $batch = array(
        'title' => 'Import Job',
        'operations' => $operations,
        'finished' => 'product_batch_finished_callback',
        'file' => drupal_get_path('module', 'functionality') . '/functionality.batch.inc',
      );
      batch_set($batch);
    }
  }
}
