<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\product_importer\Service\ProductService;

/**
 * Class BrochureAjaxForm.
 *
 * @package Drupal\functionality\Form
 */
class BrochureAjaxForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brochure_ajax_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $id = $path_args[2];

    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    //dpm($view->build_info['query']);
    $original = \Drupal::service('path.alias_manager')->getPathByAlias('/'. $id, $language);
    $tmp = explode('/', $original);
    
    if (count($tmp) == 5) $id = $tmp[4];

    $product = ProductService::productLoad($id);

    $form['company_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Company Name'),
      '#required' => TRUE
    );
    $form['business_email'] = array(
      '#type' => 'email',
      '#title' => t('Business Email'),
      '#description' => t(''),
      '#required' => TRUE
    );
    $form['url'] = array(
      '#type' => 'hidden',
      '#value' => $host.$current_path
    );
    $form['model_name'] = array(
      '#type' => 'hidden',
      '#value' => $product->name->value
    );
    
    $form['submit'] = array(
      '#type' => 'button',
      '#value' => 'Submit',
      '#ajax' => array(
        'callback' => 'Drupal\functionality\Form\BrochureAjaxForm::submitData',
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('Submit the data'),
        ),
      ),
      '#suffix' => '<span class="email-valid-message"></span>', 
      '#field_prefix' => '<div class="form-item">',
      '#field_suffix' => '</div>'
    );

    return $form;
  }
  
  public function submitData(array &$form, FormStateInterface $form_state) {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();
    $business_email = $form_state->getValue('business_email');
    $company_name = $form_state->getValue('company_name');
    $url = $form_state->getValue('url');
    $model_name = $form_state->getValue('model_name');
    $submit = $form_state->getValue('op');
    if ($submit == 'Submit' && $business_email && valid_email_address($business_email)) {
      $message = \Drupal::entityTypeManager()
        ->getStorage('contact_message')
        ->create(array(
          'contact_form' => 'brochure_contact',
          'name' => $business_email,
          'field_business_email' => $business_email,
          'field_company_name' => $company_name,
          'field_model_name' => $model_name,
          'field_url' => $url
        ));
      $message->save();
      // ValCommand does not exist, so we can use InvokeCommand.
      $ajax_response->addCommand(new InvokeCommand('.js-brochure', 'addClass' , ['show']));
    } else {
      $css = ['border' => '1px solid red'];
      $message = t('Email not valid.');
      $ajax_response->addCommand(new InvokeCommand('#edit-business-email', 'css' ,  array('border-color', '#e62600')));
      $ajax_response->addCommand(new InvokeCommand('#edit-business-email', 'css' ,  array('background-color', 'hsla(15, 75%, 97%, 1)')));
      $ajax_response->addCommand(new HtmlCommand('#edit-business-email--description', $message));
    }
      
    return $ajax_response;
    // Return the AjaxResponse Object.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
  }

}
