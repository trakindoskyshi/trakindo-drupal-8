<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BrochureAjaxForm.
 *
 * @package Drupal\functionality\Form
 */
class NewsletterAjaxForm extends FormBase {
  public function submitData(array &$form, FormStateInterface $form_state) {
    // Instantiate an AjaxResponse Object to return.
    $ajax_response = new AjaxResponse();
    $message = 'Data Submitted';
    $ajax_response->addCommand(new HtmlCommand('.newsletter-form-ajax', $message));
      
    return $ajax_response;
    // Return the AjaxResponse Object.
  }
}
