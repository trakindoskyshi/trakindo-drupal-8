<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\product_importer\Service\ProductService;
use Drupal\Core\Entity;
/**
 * Class SetNoRentallForm.
 *
 * @package Drupal\functionality\Form
 */
class SetNoRentallForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'set_no_rentall_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //$vid = 'group';
    //$groups = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1);
    // $options = [];
    // foreach($groups as $group) {
    //   if($group->name !="Forklift"){
    //     if($group->name == "Category"){
    //       $name = "Lift Truck";
    //     }else{
    //       $name = $group->name;
    //     }
    //     $options[$group->tid] = $this->t($name);
    //   }
    // }
    // $form['taxonomy_groups'] = [
    //   '#type' => 'radios',
    //   '#options' => $options,
    //   '#title' => $this->t('No rental'),
    //   '#description' => $this->t('No rental'),
    // ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = \Drupal::entityQuery('products')
            ->condition('field_rental_service','Yes');
    $results = $query->execute();
    $ids = array_keys($results);
    $entities = \Drupal::entityTypeManager()->getStorage('products')->loadMultiple($ids);
    foreach ($entities as $entity) {
      if ($entity->hasField('field_rental_service')) {
        $rental = $entity->get('field_rental_service')->value;
        if ($entity->hasField('field_product_id')) {
          if ($rental == 'Yes') {
            $product_id = $entity->get('field_product_id')->value;
            $product = ProductService::productLoadByProductID($product_id);
            $product->field_rental_service = 'No';
            $product->save();
          }
        }else{
          $name = $entity->get('name')->value;
           if ($rental == 'Yes') {
            $product_name = $entity->get('name')->value;
            $product = ProductService::productLoadByName($product_name);
            $product->field_rental_service = 'No';
            $product->save();
          }
        }
      }
    }
  }
}
