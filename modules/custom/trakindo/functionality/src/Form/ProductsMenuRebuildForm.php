<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\TermStorage;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Core\Menu\MenuLinkTree;

/**
 * Class ProductsMenuRebuildForm.
 *
 * @package Drupal\functionality\Form
 */
class ProductsMenuRebuildForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'products_menu_rebuild_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $product_menu = \Drupal::entityManager()->getStorage('menu_link_content')->load(2);
    $plugin_id = $product_menu->getPluginId();

    $vid = 'group';
    $mapping = [
      'Attachments' => 'Work Tools',
      'Power Systems' => 'Engines',
      'Equipment' => 'Machines',
      'Category' => 'CAT® Lift Trucks'
    ];
    $groups = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1);
    foreach ($groups as $term) {
      $term_value = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);
      $term_name = $term->name;
      if (!isset($mapping[$term_name])) continue;

      $name = $mapping[$term_name];

      $link_name = str_replace(' ', '-', $name);
      $link_name = strtolower($link_name);
      if ($term_name == 'Category') $link_name = 'cat-lift-trucks';


      $query = \Drupal::entityQuery('menu_link_content')
        ->condition('title', $name)
        ->condition('menu_name', 'main')
        ->condition('parent', $plugin_id);
      $results = $query->execute();
      if ($results) {
        $ids = array_keys($results);
        $menu_link = \Drupal::entityManager()->getStorage('menu_link_content')->load(reset($ids));
        $menu_link->title = $name;
        $menu_link->link = ['uri' => 'internal:/products-search/'. $link_name];
        $menu_link->menu_name = 'main';
        $menu_link->parent = $plugin_id;
        $menu_link->expanded = FALSE;
        $menu_link->save();
      } else {
        $menu_link = MenuLinkContent::create([
          'title' => $name,
          'link' => ['uri' => 'internal:/products-search/'. $link_name],
          'menu_name' => 'main',
          'parent' => $plugin_id,
          'expanded' => FALSE,
        ]);
        $menu_link->save();
      }
      /*if ($term_value->hasTranslation('id')) {
        $term_translation = $term_value->getTranslation('id');
        if ($menu_link->hasTranslation('id')) {
          $menu_link_translation = clone $menu_link->getTranslation('id');
        } else {
          $menu_link_translation = clone $menu_link->addTranslation('id');
        }
        $menu_link_translation->title = $term_translation->name->value;
        $menu_link_translation->save();
      }*/

      $parent_plugin_id = $menu_link->getPluginId();
    
      $child_1 = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term->tid);
      foreach ($child_1 as $value1) {
        $child_1_name = $value1->name->value;
        $child_1_tid = $value1->tid->value;

        
        $query = \Drupal::entityQuery('menu_link_content')
          ->condition('title', $child_1_name)
          ->condition('menu_name', 'main')
          ->condition('parent', $parent_plugin_id);
        $results = $query->execute();
        if ($results) {
          $ids = array_keys($results);
          $child_1_menu_link = \Drupal::entityManager()->getStorage('menu_link_content')->load(reset($ids));
          $child_1_menu_link->title = $child_1_name;
          $child_1_menu_link->link = ['uri' => 'internal:/products-search/'. $link_name .'?filter[0]=group:'. $child_1_tid];
          $child_1_menu_link->menu_name = 'main';
          $child_1_menu_link->parent = $parent_plugin_id;
          $child_1_menu_link->expanded = TRUE;
          $child_1_menu_link->save();
        } else {
          $child_1_menu_link = MenuLinkContent::create([
            'title' => $child_1_name,
            'link' => ['uri' => 'internal:/products-search/'. $link_name .'?filter[0]=group:'. $child_1_tid],
            'menu_name' => 'main',
            'parent' => $parent_plugin_id,
            'expanded' => TRUE,
          ]);
          $child_1_menu_link->save();
        }
        /*if ($value1->hasTranslation('id')) {
          $value1_translation = $value1->getTranslation('id');
          if ($child_1_menu_link->hasTranslation('id')) {
            $child_1_menu_link_translation = clone $child_1_menu_link->getTranslation('id');
          } else {
            $child_1_menu_link_translation = clone $child_1_menu_link->addTranslation('id');
          }
          $child_1_menu_link_translation->title = $value1_translation->name->value;
          $child_1_menu_link_translation->save();
        }*/
        $child_1_plugin_id = $child_1_menu_link->getPluginId();

        $child_2 = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($child_1_tid);
        $child_2_tids = [$child_1_tid];
        foreach ($child_2 as $value1) {
          $child_2_name = $value1->name->value;
          $child_2_tid = $value1->tid->value;
          $child_2_tids[] = $child_2_tid;
        }

        if ($child_2_tids) {
          $query = \Drupal::entityQuery('products')
            ->condition('field_group', $child_2_tids, 'IN')
            ->condition('status', 1);
          $results = $query->execute();
          if (!$results) {
            $child_1_menu_link->delete();
          }

          $query = \Drupal::entityQuery('products')
            ->condition('field_group', $child_2_tids, 'IN')
            ->condition('status', 1)
            ->condition('langcode', 'id');
          $results = $query->execute();
          if (!$results) {
            //if ($child_1_menu_link->hasTranslation('id')) $child_1_menu_link->removeTranslation('id');
          }
        } 
      }
    }
    drupal_set_message(t('Menu rebuild success, please clear cache'), 'success');
  }

}
