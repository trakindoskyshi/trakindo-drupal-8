<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class OWAConfig.
 *
 * @package Drupal\functionality\Form
 */
class OWAConfig extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'owa_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['owa_auth_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OWA Auth URL'),
      '#maxlength' => 255,
      '#default_value' => 'https://mail.tmt.co.id/owa/auth.owa',
      '#size' => 64,
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'owa.settings',
    ];
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $url = $form_state->getValue('owa_auth_url');
    $this->config('owa.settings')
      ->set('owa_auth_url', $url)
      ->save();
    drupal_set_message(t('URL has been changed.'), 'status', FALSE);
  }

}
