<?php
namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use PHPExcel_IOFactory;
use PHPExcel;
use Drupal\node\Entity\Node;

/**
 * Class LocationImportForm.
 *
 * @package Drupal\functionality\Form
 */
class LocationImportForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'location_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {    
    $form['location_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Location XLS file'),
      '#upload_validators' => [
        'file_validate_extensions' => [
          'xls xlsx'
        ]
      ]
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  public static function saveTerm($vid, $term_name, $parent_id = null) {
    $term_name = trim($term_name);
    $terms =  taxonomy_term_load_multiple_by_name($term_name, $vid);
    if (count($terms)) {
      $term = reset($terms);
      $term_id = $term->tid->value;
    } else {
      if (strlen($term_name) > 180) {
        $pos=strpos($term_name, ' ', 180);
        if ($pos) {
          $term_name = substr($term_name, 0, $pos );
        }
      }
      $term_data = [
        'name' => $term_name,
        'vid' => $vid
      ];
      if ($parent_id) {
        $term_data['parent'] = $parent_id;
      }
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $term_id = $term->tid->value;
    }
    return $term_id;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $submit = $form_state->getValue('op');
    if ($submit == 'Submit') {
      $location_file = $form_state->getValue('location_file');
      $fid = $location_file[0];
      $level_tids = [
        '1' => '12321',
        '2' => '12322',
        '3' => '14336',
        '4' => '14337'
      ];

      $file =  \Drupal\file\Entity\File::load($fid);
      $uri = $file->getFileUri();

      $real_path = \Drupal::service('file_system')->realpath($uri);
      $objPHPExcel = PHPExcel_IOFactory::load($real_path);
      foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $rows = $worksheet->toArray();
        foreach ($rows as $index => $row) {
          $name = $row[0];
          if (!$name) continue;
          
          $row[1] = trim($row[1]);
          $row[2] = trim($row[2]);
          $lat = ($row[1]) ? (float)$row[1] : NULL;
          $long = ($row[2]) ? (float)$row[2] : NULL;
          $level = $row[5];
          $address = $row[7];
          $phone = $row[8];
          $fax = $row[9];
          $postal_code = $row[6];
          if ($index == 0 || $level == 2) continue;
          
          $company_type = $level;
          if ($company_type == 'Branches Store') $company_type == 'Branches';
          $vid = 'company_type';

          $company_type_tid = NULL;
          if ($company_type) {
            $company_type_tid = $this->saveTerm($vid, $company_type);
          }

          $vid = 'location_area';
          $location_area = $row[11];

          $location_area_tid = NULL;
          if ($location_area) {
            $location_area_tid = $this->saveTerm($vid, $location_area);
          }
          
          $node = Node::create([
            'type' => 'location',
            'title' => $name,
            'field_company_type' => $company_type_tid,
            'field_area' => $location_area_tid,
            'field_postal_code' => $postal_code,
            'field_fax' => $fax,
            'field_location' => [
              'lat' => $lat,
              'lng' => $long
            ],
            'field_phone' => $phone
          ]);
          $node->save();
        }
      }
      
    }

  }

}
