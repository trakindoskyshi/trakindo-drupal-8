<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\product_importer\Service\ProductService;

/**
 * Class ProductCache.
 *
 * @package Drupal\functionality\Form
 */
class ProductCache extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_cache';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $product_type = [
      'equipment',
      'power_systems',
      'attachments'
    ];

    $query = \Drupal::entityQuery('products')
      ->condition('type', $product_type, 'IN');
    $results = $query->execute();
    $ids = array_keys($results);
    foreach ($ids as $id) {
      $operations[] = array('functionality_set_product_cache', array($id));
    }
    $batch = array(
      'title' => 'Set Product Cache',
      'operations' => $operations,
      'finished' => 'functionality_batch_finished_callback',
      'file' => drupal_get_path('module', 'functionality') . '/functionality.batch.inc',
    );
  
    batch_set($batch);
  }
}
