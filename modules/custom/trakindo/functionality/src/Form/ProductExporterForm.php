<?php

namespace Drupal\product_exporter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\TermStorage;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Class ProductExporterForm.
 *
 * @package Drupal\product_exporter\Form
 */
class ProductExporterForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_exporter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $vid = 'group';
    $groups = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1);
    $options = [];
    foreach($groups as $group) {
      $options[$group->tid] = $this->t($group->name);
    }
    $form['taxonomy_groups'] = [
      '#type' => 'radios',
      '#options' => $options,
      '#title' => $this->t('File to Export'),
      '#description' => $this->t('Select XML file to import'),
    ];
    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    require_once('vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
    require_once('vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');
    // Display result.
    // Create new PHPExcel object
    $objPHPExcel = new \PHPExcel();

    // Add some data
    $objPHPExcel->setActiveSheetIndex(0);
    $dataArray = [
      [
        'Product ID',
        'Name',
        'Long Name',
        'Industry'
      ]
    ];

    $objPHPExcel->getActiveSheet()->setTitle('Products XLS');
    $vid = 'group';
    $last_children = [];
    $file_name = [];
    //change to radio button from checkbox
    $tid = $form_state->getValue('taxonomy_groups');
//    foreach ($form_state->getValue('taxonomy_groups') as $tid) {
      if ($tid) {
        $file_name[] = $tid;
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, $tid);
        foreach ($terms as $term) {
          $child = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term->tid);

          if (!count($child)) {
            $last_children[] = $term->tid;
          }
        }
      }
//    }
    $query = \Drupal::entityQuery('products')
      ->condition('field_group', $last_children, 'IN');
    $results = $query->execute();
    $ids = array_keys($results);
    $entities = \Drupal::entityTypeManager()->getStorage('products')->loadMultiple($ids);
    foreach ($entities as $entity) {
      $product_id = $entity->get('field_product_id')->value;
      $name = $entity->get('name')->value;
      $long_name =  $entity->get('field_longname')->value;
      $dataArray[] = [
        $product_id,
        $name,
        $long_name
      ];
    }
    $objPHPExcel->getActiveSheet()->fromArray($dataArray);
    $file = implode('-', $file_name);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="product_export-'. $file. '.xls"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }

}
