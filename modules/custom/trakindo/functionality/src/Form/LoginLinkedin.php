<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use LinkedIn\LinkedIn;

/**
 * Class LoginLinkedin.
 *
 * @package Drupal\functionality\Form
 */
class LoginLinkedin extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_linkedin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = \Drupal::config('linkedin.config');
    $api_key = $config->get('api_key');
    $api_secret = $config->get('api_secret');
    $callback_url = $config->get('callback_url');
    $company_id = $config->get('company_id');

    //  if (is_null($api_key) && is_null($api_secret) && is_null($callback_url) && is_null($company_id) {
    if (!$api_key && !$api_secret && !$callback_url && !$company_id) {
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $form['api_key'] = [
        '#type' => 'textfield',
        '#title' => 'Client ID',
        '#description' => 'Client ID',
        '#placeholder' => 'Client ID',
        '#required' => true,
      ];

      $form['api_secret'] = [
        '#type' => 'textfield',
        '#title' => 'Client Secret',
        '#description' => 'Client Secret',
        '#placeholder' => 'Client Secret',
        '#required' => true,
      ];

      $form['callback_url'] = [
        '#type' => 'textfield',
        '#title' => 'Authorized Redirect URLs',
        '#description' => $host.'/..........',
        '#placeholder' => $host.'/linkedin/auth',
        '#required' => true,
      ];

       $form['company_id'] = [
        '#type' => 'textfield',
        '#title' => 'Company id linkedin',
        '#description' => 'Company id linkedin',
        '#placeholder' => 'Company id linkedin',
        '#required' => true,
      ];
    }

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Login'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $host = \Drupal::request()->getSchemeAndHttpHost();

    $config = \Drupal::service('config.factory')->getEditable('linkedin.config');
    $api_key = $config->get('api_key');
    $api_secret = $config->get('api_secret');
    $callback_url = $config->get('callback_url');
    $company_id = $config->get('company_id');
    //  if (is_null($api_key) && is_null($api_secret) && is_null($callback_url) && is_null($company_id) {
    if (!$api_key && !$api_secret && !$callback_url && !$company_id) {
      $values = $form_state->getValues();
      $api_key = $values['api_key'];
      $api_secret = $values['api_secret'];
      $callback_url = $values['callback_url'];
      $company_id = $values['company_id'];

      $config
          ->set('api_key', $api_key)
          ->set('api_secret', $api_secret)
          ->set('callback_url', $callback_url)
          ->set('company_id', $company_id)
          ->save();
      
      $li = new LinkedIn(
        array(
          'api_key' => $api_key, 
          'api_secret' => $api_secret, 
          'callback_url' => $callback_url
        )
      );
    }else{
      $li = new LinkedIn(
        array(
          'api_key' => $api_key, 
          'api_secret' => $api_secret, 
          'callback_url' => $callback_url
        )
      );
    }
    // Display result.
   
    
    $url = $li->getLoginUrl(
      array(
        LinkedIn::SCOPE_BASIC_PROFILE, 
        LinkedIn::SCOPE_EMAIL_ADDRESS
      )
    );

    $dp = new RedirectResponse($url);
    dpm($dp);
  }

}
