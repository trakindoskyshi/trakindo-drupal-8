<?php

namespace Drupal\functionality\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use LinkedIn\LinkedIn;

/**
 * Class PullJobLinkedin.
 *
 * @package Drupal\functionality\Form
 */
class PullJobLinkedin extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pull_job_linkedin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Pull Job'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    //\Drupal::service('config.factory')->getEditable('linkedin.config')->delete();

    $configdata = \Drupal::service('config.factory')->getEditable('linkedin.config');
    $api_key = $configdata->get('api_key');
    $api_secret = $configdata->get('api_secret');
    $callback_url = $configdata->get('callback_url');
    $company_id = $configdata->get('company_id');
    if (!$api_key && !$api_secret && !$callback_url) {
      dpm('Silahkan isi from untuk login linkedin');
      $form_state->setRedirect('functionality.login_linkedin');
      return;
    }

    $li = new LinkedIn(
      array(
        'api_key' => $api_key, 
        'api_secret' => $api_secret, 
        'callback_url' => $callback_url
      )
    );

    $config = \Drupal::config('linkedin.token');
    $token_expired = $config->get('token_expires');
    $token = $config->get('token');
    $nowtime = date('Y-m-d');
    if ($nowtime < $token_expired) {
      $li->setAccessToken($token);
      $info = $li->get('/people/~:(first-name,last-name,positions)');
      dpm($info);
    }else{
      dpm('Token expired silakan login kembali');
      $form_state->setRedirect('functionality.login_linkedin');
      return;
    }
  }

}
