<?php

namespace Drupal\functionality\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\product_importer\Service\ProductService;
use Drupal\Core\Url;
use Drupal\product\Service\ProductMedia;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("brochuretable_field")
 */
class BrochureTableField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $id = $values->id;

    $entity = ProductService::productLoad($id);
    $marketing_content = $entity->get('field_marketing_content')->getValue();

    $form = [];
    $form['brochure'] = [
      '#type' => 'table',
      '#caption' => 'Brochure',
      '#header' => array('File', 'Format', 'Size')
    ];

    foreach ($marketing_content as $key => $value) {
      $id = $value['target_id'];

      $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($id);
      $field_content = $fc->get('field_content')->getValue();
      foreach ($field_content as $content) {
        $content_id = $content['target_id'];
        $fc_content = \Drupal\field_collection\Entity\FieldCollectionItem::load($content_id);
        $title = $fc_content->get('field_title')->getValue();
        $title = $title[0]['value'];

        $media = $fc_content->get('field_media_raw')->getValue();

        if ($media) {
          $media = $media[0]['value'];      
          if (strpos($media, '.com') !== FALSE) {
            $http = substr($media, 0,4);
            if($http != 'http'){
              $media = 'http://'. $media;
            }
            $form['brochure'][$content_id]['title'] = array(
              '#markup' => \Drupal::l($title, Url::fromUri($media, array('absolute' => TRUE))),
              '#allowed_tags' => ['a'],
            );
            $cid = 'functionality:file_info:'. $media;
            if ($cache = ProductMedia::getMediaInfo($media)) {
              $file_info = $cache;
            } else {
              ProductMedia::setMediaInfo($media);
              $file_info = ProductMedia::getMediaInfo($media);
              /*$file_info = [
                'size' => '-',
                'type' => '~-'
              ];*/
            }

            $form['brochure'][$content_id]['format'] = array(
              '#plain_text' => $file_info['type'],
            );

            $form['brochure'][$content_id]['size'] = array(
              '#markup' => $file_info['size'],
            );
          }
        }
      }
    }

    $renderer = $this->getRenderer();
    return $renderer->render($form);
  }
  
  private function getFileInfo($url) {
    if (strpos($url, 'youtu') !== FALSE) {
      $type = 'video';
      $size = '~';
      return [
        'size' => $size,
        'type' => $type
      ];
    }else{
      $type = '-';
      $size = '~';
      // $ch = curl_init(); 
      // curl_setopt($ch, CURLOPT_HEADER, true); 
      // curl_setopt($ch, CURLOPT_NOBODY, true);
      // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
      // curl_setopt($ch, CURLOPT_URL, $url); //specify the url
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

      // $head = curl_exec($ch);

      // $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
      // $type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
      // $type = str_replace('application/', '', $type);
      // $type = str_replace('image/', '', $type);
      
      // $clen = $size;
      // switch ($clen) {
      //     case $clen < 1024:
      //         $size = $clen .' B'; break;
      //     case $clen < 1048576:
      //         $size = round($clen / 1024, 2) .' KiB'; break;
      //     case $clen < 1073741824:
      //         $size = round($clen / 1048576, 2) . ' MiB'; break;
      //     case $clen < 1099511627776:
      //         $size = round($clen / 1073741824, 2) . ' GiB'; break;
      // }
      // curl_close($ch);
      return [
        'size' => $size,
        'type' => $type
      ];

    }
  }

}
