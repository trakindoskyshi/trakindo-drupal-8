<?php

namespace Drupal\functionality\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\product_importer\Service\ProductService;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("search_description_views_field")
 */
class SearchDescriptionViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return a random text, here you can include your custom logic.
    // Include any namespace required to call the method required to generate
    // the desired output.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
    } else {
      $id = $values->id;
      $entity = ProductService::productLoad($id);
    }
    $entity_type = $entity->get('type')->target_id;
    if ($entity_type == 'used_equipment') {
      $id = $entity->field_product_reference->target_id;
      $entity = ProductService::productLoad($id);
      $entity_type = $entity->get('type')->target_id;
    }

    if ($entity_type == 'cat_lift_trucks') {
      $specifications = $entity->get('field_fork_lift_specification')->getValue();
      $group = $entity->get('field_category')->getValue();
      $field_spec = 'field_fork_lift_spec_detail';
      $field_spec_label = 'field_detail_spec_label';
      $field_spec_data = 'field_detail_value_spec';
    } else {
      $specifications = $entity->get('field_specifications')->getValue();
      $group = $entity->get('field_group')->getValue();
      $field_spec = 'field_spec_list';
      $field_spec_label = 'field_spec_label';
      $field_spec_data = 'field_spec_data';
    }


    $field_display_label = [];
    $term = null;
    if ($group) {
      $tid = $group[0]['target_id'];

      if ($tid) {
        $term = \Drupal\taxonomy\Entity\Term::load($tid);
        $field_display_label = $term->get('field_display_label')->getValue();  
      }
    }    

    $display_label = [];
    if (count($field_display_label)) {
      $display_label = array_map('trim', explode("\n", $field_display_label[0]['value']));
      foreach ($display_label as &$v) {
        $v = str_replace('–', '-', $v);
      }
      foreach ($display_label as &$v) {
        $v = strtolower($v);
      }
      foreach ($display_label as &$v) {
        $tmp = explode('|', $v);
        if ($language == 'en' && count($tmp) > 1) {
          $v = $tmp[0];
        } else if ($language == 'id' && count($tmp) > 1) {
          $v = $tmp[1];
        }
      }
    }

    //dummy content
    $list = [];
    foreach ($specifications as $key => $value) {
      $id = $value['target_id'];

      $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($id);
      $spec_list = $fc->get($field_spec)->getValue();
      foreach ($spec_list as $spec) {
        $spec_id = $spec['target_id'];
        $fc_spec = \Drupal\field_collection\Entity\FieldCollectionItem::load($spec_id);

        $spec_data = $fc_spec->get($field_spec_data)->getValue();
        if ($entity_type == 'cat_lift_trucks') {
          $spec_label = $fc_spec->get($field_spec_label)->getValue();
          $spec_label = $spec_label[0]['value'];

          if (in_array(strtolower($spec_label), $display_label) && count($list) < 4) {
            $tmp = explode(' ', $spec_label);
            if (count($tmp) > 1) {
              $tmp = array_slice($tmp, 0, 2);
              $spec_label = implode(' ', $tmp);
            }
            $list[$spec_label] = [
              '#markup' => '<div class="title">'. $spec_label. '</div><div class="value">'. $spec_data[0]['value']. '</div>'
            ];
          }
        } else {

          foreach ($spec_data as $data) {
            $data_value = $data['value'];
            if ($data_value) {
              foreach ($data_value as $key => $data_label) {
                foreach ($data_label as $label_index => $label) {
                  $label = trim($label);
                  $label = str_replace('–', '-', $label);

                  if (in_array(strtolower($label), $display_label) && count($list) < 4) {
                    $tmp = explode(' ', $label);
                    if (count($tmp) > 1) {
                      $tmp = array_slice($tmp, 0, 2);
                      $label = implode(' ', $tmp);
                    }
                    $current_path = \Drupal::service('path.current')->getPath();
                    $match = \Drupal::service('path.matcher')->matchPath($current_path, '/product-detail/*'. PHP_EOL. '/batch');
                    if ($match) {
                      $list[$label] = [
                        '#markup' => '<div class="title">'. $label. '</div><div class="value">'. $data_value[$key][1]. '</div>'
                      ];  
                    } else {
                      $list[$label] = [
                        '#markup' => '<div class="title">'. $label. '</div><div class="value">'. $data_value[1][$label_index]. '</div>'
                      ];
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (!$list) {
      foreach ($specifications as $key => $value) {
        $id = $value['target_id'];

        $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($id);
        $spec_list = $fc->get($field_spec)->getValue();
        foreach ($spec_list as $spec) {
          $spec_id = $spec['target_id'];
          $fc_spec = \Drupal\field_collection\Entity\FieldCollectionItem::load($spec_id);

          $spec_data = $fc_spec->get($field_spec_data)->getValue();

          if ($entity_type == 'cat_lift_trucks') {
            $spec_label = $fc_spec->get($field_spec_label)->getValue();
            $spec_label = $spec_label[0]['value'];

            if (count($list) < 4) {
              $tmp = explode(' ', $spec_label);
              if (count($tmp) > 1) {
                $tmp = array_slice($tmp, 0, 3);
                $spec_label = implode(' ', $tmp);
              }
              $list[$spec_label] = [
                '#markup' => '<div class="title">'. $spec_label. '</div><div class="value">'. $spec_data[0]['value']. '</div>'
              ];
            }
          } else {
            foreach ($spec_data as $data) {
              $data_value = $data['value'];
              if ($data_value) {
                foreach ($data_value as $key => $data_label) {
                  foreach ($data_label as $label_index => $label) {
                    
                    $label = trim($label);
                    $label = str_replace('–', '-', $label);
                    $current_path = \Drupal::service('path.current')->getPath();
                    $match = \Drupal::service('path.matcher')->matchPath($current_path, '/product-detail/*');
                    $label_value="-";
                    if ($match) {
                      $label_value = $data_value[$key][1];
                    }else{
                      if (!isset($data_value[1][$label_index])) continue;
                       $label_value = $data_value[1][$label_index];
                    }
                    if ($label == $label_value) continue;

                    if (count($list) < 4) {
                      $tmp = explode(' ', $label);
                      if (count($tmp) > 1) {
                        $tmp = array_slice($tmp, 0, 3);
                        $label = implode(' ', $tmp);
                      }
                      $list[$label] = [
                        '#markup' => '<div class="title">'. $label. '</div><div class="value">'. $label_value. '</div>'
                      ];
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    $renderer = $this->getRenderer();
    return $renderer->render($list);
  }

}
