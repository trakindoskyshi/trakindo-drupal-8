<?php

namespace Drupal\functionality\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\product_importer\Service\ProductService;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("promotion_views_field")
 */
class PromotionViewsField extends FieldPluginBase {

  private $promo = [];
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
    $now = date("Y-m-d");
    $query = \Drupal::entityQuery('node')
            ->condition('type', 'promotion')
            ->condition('status', 1)
            ->condition('field_end_date',$now,'>=')
            ->condition('field_start_date',$now,'<=');
    $nids = $query->execute();
    $entities = entity_load_multiple('node', $nids);
    foreach ($entities as $entity) {

      $product_group = $entity->get('field_product_category')->getValue();
      if (count($product_group) > 0) {
        $last_children = [];
        foreach ($product_group as $key => $value) {
          $tid = $value['target_id'];
          $vid = 'group';
           if ($tid) {
            $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, $tid);
            if (count($terms) == 0) {
              if (!in_array($tid, $last_children)){
                $last_children[] = $tid;
              }
            }else{
              foreach ($terms as $term) {
                $child = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term->tid);
                if (!count($child)) {
                  if (!in_array($term->tid, $last_children)){
                    $last_children[] = $term->tid;
                  }
                }
              }
            }

          $query = \Drupal::entityQuery('products')
              ->condition('field_group', $last_children, 'IN');
            $results = $query->execute();
            $ids = array_values($results);
            $entities_product = \Drupal::entityTypeManager()->getStorage('products')->loadMultiple($ids);
            foreach ($entities_product as $entity_product) {
              $id_product = $entity_product->get('id')->value;
              $newdata = [
                'id_product' => $id_product,
                'promo_id' => $entity->id(),
                'field_group' => $tid
              ];
              $key = (string)array_search($id_product, array_column($this->promo, 'id_product'));
              if($key == ''){
                $this->promo[] = $newdata;
              }else{
                $cekParrent = $this->compareTaxonomy($this->promo[(int)$key]['field_group'],$tid);
                //$this->promo[] = $newdata;
                if ($cekParrent) {
                  $this->promo[(int)$key] = $newdata;
                }
              }
            }

          } 
        }
      }

      $product = $entity->get('field_product')->getValue();
      if (count($product) > 0) {
         foreach ($product as $key => $value) {
          $key = (string)array_search($value['target_id'], array_column($this->promo, 'id_product'));
            $newdata = [
                'id_product' => $value['target_id'],
                'promo_id' => $entity->id(),
                'field_group' => 0
            ];
            if($key == ''){
              $this->promo[] = $newdata;
            }else{
              $this->promo[(int)$key] = $newdata;
            }
         }
      }
      
     }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['text'] = array('default' => '');
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return a random text, here you can include your custom logic.
    // Include any namespace required to call the method required to generate
    // the desired output.

    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
    } else {
      $id = $values->id;
      $entity = ProductService::productLoad($id);
    }
    $key = (string)array_search($entity->id->value, array_column($this->promo, 'id_product'),false);

    if($key != ''){
      $data[] = array(
          '#markup' => $this->promo[(int)$key]['promo_id'],
        );
    }
     
    $renderer = $this->getRenderer();
    return $renderer->render($data);
  }

  public function getTaxonomy($tid){
    $last_children=[];
    $vid = 'group';
     if ($tid) {
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, $tid);
      if (count($terms) == 0) {
        if (!in_array($tid, $last_children)){
          $last_children[] = $tid;
        }
      }else{
        foreach ($terms as $term) {
          $child = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadChildren($term->tid);
          if (!count($child)) {
            if (!in_array($term->tid, $last_children)){
              $last_children[] = $term->tid;
            }
          }
        }
      }
    }
    return $last_children;
  }

  public function compareTaxonomy($first,$second){
     $arrFirst = $this->getTaxonomy($first);
     $arrSecond = $this->getTaxonomy($second);
      if (count($arrFirst) >= count($arrSecond)) {
        return true;
      }else{
        return false;
      }
    
  }

}
