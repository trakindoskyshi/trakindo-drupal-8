<?php

namespace Drupal\functionality\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\product_importer\Service\ProductService;
use Drupal\views\ResultRow;
use Drupal\Core\Url;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("rentalusedviews_field")
 */
class RentalUsedViewsField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['text'] = array('default' => '');
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Return a random text, here you can include your custom logic.
    // Include any namespace required to call the method required to generate
    // the desired output.
    if (isset($values->_object)) {
      $entity = $values->_object->getValue();
    } else {
      $id = $values->id;
      $entity = ProductService::productLoad($id);
    }
    $rental = $entity->field_rental_service->value;

    // $entity_type = $entity->get('type')->target_id;
    // $list = [];

    // if ($entity_type != 'used_equipment') {
    //   $rental = $entity->field_rental_service->value;
    //   $reference_id = $entity->id->value;
    //   $query = \Drupal::entityQuery('products')
    //     ->condition('type', 'used_equipment')
    //     ->condition('field_product_reference', $reference_id);
    //   $entity_ids = $query->execute();

    //   if ($entity_ids && $rental == 'Yes') {
    //     $list[] = array(
    //       '#markup' => 'Available for ',
    //     );
    //     $list[] = array(
    //       '#markup' => \Drupal::l(t('used equipment'), Url::fromUri('internal:/products-search/used-equipment/'. $reference_id)),
    //     );
    //     $list[] = array(
    //       '#markup' => ' & ',
    //     );
    //      $list[] = array(
    //       '#markup' => \Drupal::l(t('rental service'), Url::fromUri('internal:/products-search/rental-service')),
    //     );
    //   }elseif ($entity_ids && $rental == 'No') {
    //      $list[] = array(
    //       '#markup' => 'Available for ',
    //     );
    //     $list[] = array(
    //       '#markup' => \Drupal::l(t('used equipment'), Url::fromUri('internal:/products-search/used-equipment/'. $reference_id)),
    //     );
    //   }elseif (!$entity_ids && $rental == 'Yes') {
    //      $list[] = array(
    //       '#markup' => 'Available for ',
    //     );
    //      $list[] = array(
    //       '#markup' => \Drupal::l(t('rental service'), Url::fromUri('internal:/products-search/rental-service')),
    //     );
    //   }

    // }
    if ($rental == 'Yes') {
         $list[] = array(
          '#markup' => 'Available for ',
        );
         $list[] = array(
          '#markup' => \Drupal::l(t('rental service'), Url::fromUri('internal:/products-search/rental-service')),
        );
      }



    $renderer = $this->getRenderer();
    return $renderer->render($list);
  }

}
