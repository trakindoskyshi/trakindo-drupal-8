<?php

namespace Drupal\functionality\Plugin\QueueWorker;

use Drupal\aggregator\FeedInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\product\Service\CPC;
use Drupal\product_importer\Service\ProductService;
use Drupal\functionality\Form\JobImporterForm;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "job_import_queue",
 *   title = @Translation("Job Import"),
 *   cron = {"time" = 500}
 * )
 */
class JobImportQueue extends QueueWorkerBase {
  /**
  * {@inheritdoc}
  */
  public function processItem($data) {
    $job_url = $data['job_url'];

    $job_content = product_getPage($job_url);
    preg_match_all('/<code id="([^"]+)"><!--(.+?(?=--\>))/', $job_content, $job_matches);
    // convert all code values to readable json code
    foreach ($job_matches[1] as $key => $label) {
      $jobs[$label] = json_decode($job_matches[2][$key]);
    }
    
    $job_share = $jobs['shareModule'];
    $job_description = $jobs['jobDescriptionModule']->description;
    $job_title = $job_share->jobPosting->title;
    $job_url = $job_share->pageUrl;
    $decorated_job = $jobs['decoratedJobPostingModule']->decoratedJobPosting;

    $job_function = $decorated_job->formattedJobFunctions;
    $job_field = [];
    foreach ($job_function as $value) {
      $vid = 'job_field';
      $job_field[] = JobImporterForm::saveTerm($vid, $value);
    }
    $job_schedule = $decorated_job->formattedEmploymentStatus;
    $location = $decorated_job->formattedLocation;
    $tmp = explode(',', $location);

    $vid = 'job_location';
    $parent_tid = JobImporterForm::saveTerm($vid, $tmp[1]);
    $job_location = JobImporterForm::saveTerm($vid, $tmp[0], $parent_id);
    $posting = $decorated_job->formattedListDate;
    $tmp = strtotime($posting);
    $job_posting = date('Y-m-d', $tmp);
    $job_career_level = $decorated_job->formattedExperience;

    $query = \Drupal::entityQuery('node')
      ->condition('type', 'job')
      ->condition('field_url', $job_url);
    $entity_ids = $query->execute();
    if (count($entity_ids)) {
      $id = reset($entity_ids);
      $node = Node::load($id);
      $node->body->value = $job_description;
      $node->body->format = 'full_html';
      $node->title = $job_title;
      $node->field_career_level = $job_career_level;
      $node->field_job_field = $job_field;
      $node->field_job_location = $job_location;
      $node->field_job_posting = $job_posting;
      $node->field_url = $job_url;
      $node->field_schedule = $job_schedule;
      $node->save();
    } else {
      $node = Node::create([
        'type' => 'job',
        'title' => $job_title,
        'body' => [
          'value' => $job_description,
          'format' => 'full_html'
        ],
        'field_career_level' => $job_career_level,
        'field_job_field' => $job_field,
        'field_job_location' => $job_location,
        'field_job_posting' => $job_posting,
        'field_url' => $job_url,
        'field_schedule' => $job_schedule
      ]);
      $node->save();
    }

    $context['results'][] = $job_url;  
    $context['message'] = 'Job URL: '. $job_url;

  }
}
