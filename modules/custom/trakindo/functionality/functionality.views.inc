<?php

/**
 * @file
 * Contains functionality\functionality.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Render\Markup;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\system\ActionConfigEntityInterface;

/**
* Implements hook_views_data().
*/
function functionality_views_data() {

    $data['views']['table']['group'] = t('Custom Global');
    $data['views']['table']['join'] = array(
      // #global is a special flag which allows a table to appear all the time.
      '#global' => array(),
    );

    $data['views']['promotion_views_field'] = array(
        'title' => t('Promotion views field'),
        'help' => t('Promotion field'),
        'field' => array(
            'id' => 'promotion_views_field',
        ),
    );


     $data['views']['phpviews_field'] = array(
        'title' => t('PHP Field'),
        'help' => t('Enable to include and run PHP code in your modules as part of a view'),
        'field' => array(
            'id' => 'phpviews_field',
        ),
    );

    $data['views']['rentalusedviews_field'] = array(
        'title' => t('Rental and Used Equipment Views Field'),
        'help' => t('Rental and Used Equipment Views Field'),
        'field' => array(
            'id' => 'rentalusedviews_field',
        ),
    );

    $data['views']['brochuretable_field'] = array(
        'title' => t('Brochure Table Field'),
        'help' => t('Display brochure table'),
        'field' => array(
            'id' => 'brochuretable_field',
        ),
    );

    $data['views']['brochurecontactviews_field'] = array(
        'title' => t('Brochure Contact Views Field'),
        'help' => t('Brochure Contact Views Field'),
        'field' => array(
            'id' => 'brochure_contact_views_field',
        ),
    );

    $data['views']['searchdescriptionviews_field'] = array(
        'title' => t('Search Description Views Field'),
        'help' => t('Search Description Views Field'),
        'field' => array(
            'id' => 'search_description_views_field',
        ),
    );

    $data['views']['productajaxlink_field'] = array(
        'title' => t('Product Compare AJAX Link Field'),
        'help' => t('Product Compare AJAX Link Field'),
        'field' => array(
            'id' => 'product_compare_ajax_field',
        ),
    );

    $data['views']['node_url_alias_views_field'] = array(
        'title' => t('Node URL Alias Field'),
        'help' => t('Node URL Alias Field'),
        'field' => array(
            'id' => 'node_url_alias_views_field',
        ),
    );

    return $data;
}
