<?php

namespace Drupal\product_importer\Plugin\views\field;

use Drupal\system\Plugin\views\field\BulkForm;

/**
 * Defines a product operations bulk form element.
 *
 * @ViewsField("product_bulk_form")
 */
class ProductBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No content selected.');
  }

}
