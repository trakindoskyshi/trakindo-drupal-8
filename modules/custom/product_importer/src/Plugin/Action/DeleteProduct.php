<?php


// namespace Drupal\product_importer\Plugin\Action;

// use Drupal\Core\Action\ActionBase;
// use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
// use Drupal\Core\Session\AccountInterface;
// use Drupal\user\PrivateTempStoreFactory;
// use Symfony\Component\DependencyInjection\ContainerInterface;

// /**
//  * Redirects to a product deletion form.
//  *
//  * @Action(
//  *   id = "product_delete_action",
//  *   label = @Translation("Delete product"),
//  *   type = "products"
//  * )
//  */
// class DeleteProduct extends ActionBase {

//   /**
//    * {@inheritdoc}
//    */
//   public function execute($entity = NULL) {
//     // We need to change at least one value, otherwise the changed timestamp
//     // will not be updated.
//     $entity->changed = 0;
//     $entity->save();
//   }

//   /**
//    * {@inheritdoc}
//    */
//   public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
//     /** @var \Drupal\node\NodeInterface $object */
//     return $object->access('update', $account, $return_as_object);
//   }

// }
