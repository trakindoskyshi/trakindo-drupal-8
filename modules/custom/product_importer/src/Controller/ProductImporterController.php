<?php
namespace Drupal\product_importer\Controller;

use Drupal\facets\Exception\Exception;
use Drupal\product_importer\Service\ProductService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing;
use Sabre\Xml;

/**
 * Class ProductImporterController.
 *
 * @package Drupal\product_importer\Controller
 */
class ProductImporterController extends ControllerBase {
  public function productImportTest() {
    /*$id = 165;
    $query = \Drupal::entityQuery('products')
      ->condition('type', 'used_equipment')
      ->condition('field_product_reference', $id);
    $entity_ids = $query->execute();
    if ($entity_ids) {
      $id = reset($entity_ids);

    }*/
    //exit();

    $parent_id = \Drupal::routeMatch()->getParameter('parent_id');
    /*$term = \Drupal\taxonomy\Entity\Term::load(6307);
    $term->description = 'test';
    $term->save();
    exit();*/
    //$term = \Drupal\taxonomy\Entity\Term::load();
    // $product_id = '1000009563';
        // $tree_id = $parent_id;
    // $product_service = new ProductService();
    // $product_service->setParentId($tree_id);
    // $product_service->setProductId($product_id);
    // $product_service->importProductImage();

    // $query = \Drupal::entityQuery('products')
    //   ->condition('field_product_id', $product_id);
    // $entity_ids = $query->execute();

    //   $id = reset($entity_ids);
    //   //$entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
    //   print $entity->id();
    // print 'ok';
    // exit();


    $file_path = 'public://product_xml/J210/'. $parent_id. 'tree_en.xml';
    $products_service = new ProductService();
    $tree = $products_service->parseXML($file_path, ['elementMap' => '{}product_group']);
    $products_group = $products_service->getProducts($tree);

    $tree = $products_service->getTree();
    $tree_id = $parent_id;
    $product_fields = [];
    foreach ($products_group as $group_id => $groups) {
      if ($group_id != '18260932') continue;
      $group_name = $groups['{}name'];
      //$group_id = '715';

      foreach ($groups['{}listofproducts'] as $product) {
        $product_id = $product['attributes']['id'];
        //$product_id = '18089285';
        $product_id = '1000001191';
        $product_path = 'public://product_xml/en/'. $group_id. '/'. $product_id. '_en.xml';

        $product_service2 = new ProductService();
        $product_service2->setParentId($tree_id);
        $product_service2->setProductId($product_id);
        $product_tree = $tree;
        $product_tree[$group_name][] = $group_name;

        $product_service2->setTree($product_tree[$group_name]);
      
        $product_data = $product_service2->parseXML($product_path);
        $data = $product_service2->prepareData($product_data[0]);
        
        $product_entity = $product_service2->createProduct();
        print 'ok';

        print_r($product_entity->id());
        exit();
        
      }
    }
  }
  public function field() {
    $parent_id = \Drupal::routeMatch()->getParameter('parent_id');
    $file_path = 'public://product_xml/J210/'. $parent_id. 'tree_en.xml';
    $products_service = new ProductService();
    $tree = $products_service->parseXML($file_path, ['elementMap' => '{}product_group']);
    $products_group = $products_service->getProducts($tree);
    $tree_id = $tree[0]['attributes']['id'];
    $product_fields = [];
    foreach ($products_group as $group_id => $groups) {
      foreach ($groups['{}listofproducts'] as $product) {
        $product_id = $product['attributes']['id'];
        $product_path = 'public://product_xml/en/'. $group_id. '/'. $product_id. '_en.xml';
        
        $product_service2 = new ProductService();
        $product_service2->setParentId($tree_id);
        $product_service2->setProductId($product_id);
        
        $product_data = $product_service2->parseXML($product_path);
        foreach ($product_data[0]['value'] as $product_value) {
          $field_key = str_replace('{}', '', $product_value['name']);
          $type = 'no-type';
          $attributes = $product_value['attributes'];
          if ($attributes['type']) {
            $type = $attributes['type'];
          }
          if (is_array($product_value['value'])) {
            $field_type = $product_value['value'][0];
          } else {
            $field_type = 'text';
          }
          $product_fields[$tree_id][$field_key][$type] = $field_type;
        }
      }
    }
    print_r($product_fields);
    exit();
  }
  
  protected function getProducts($groups) {
    $products = [];
    foreach ($groups as $group) {
      $value = $group['value'];
      if (isset($value['{}listofgroups'])) {
        $groups = $value['{}listofgroups'];
        $products = self::getProducts($groups);
      } else {
        $id = null;
        if (isset($group['attributes']['id']))
          $id = $group['attributes']['id'];
        if ($id)
          $products[$id] = $value;
      }
    }
    return $products;
  }
  
  public function productImport() {
    $parent_id = \Drupal::routeMatch()->getParameter('parent_id');
    $parent_path = 'public://product_xml/J210/'. $parent_id. 'tree_en.xml';
    $xml_file = \Drupal::service('file_system')->realpath($parent_path);
    $service = new Xml\Service();
    $service->elementMap = [
      '{}product_group' => 'Sabre\Xml\Deserializer\keyValue',
    ];
    $tree = $service->parse(file_get_contents($xml_file));
    $tree_id = $tree[0]['attributes']['id'];
    $products_group = self::getProducts($tree);
    $operations = [];
    foreach ($products_group as $group_id => $groups) {
      foreach ($groups['{}listofproducts'] as $product) {
        $product_id = $product['attributes']['id'];
        $operations[] = array('product_importer_import_product', array($parent_id, array('id' => $product_id)));
      }
    }
    $batch = array(
      'title' => t('Exporting'),
      'operations' => $operations,
      'finished' => 'product_importer_import_product_finished_callback',
      'file' => drupal_get_path('module', 'product_importer') . '/product_importer.inc',
    );
  
    batch_set($batch);
    return batch_process('user');
  }
  
  public function import() {
    $files = file_scan_directory('public://product_xml/J210', '/[\w]+tree_en\.xml/');
    foreach($files as $file) {
      $xml_file = \Drupal::service('file_system')->realpath($file->uri);
      $service = new Xml\Service();
      $service->elementMap = [
        '{}product_group' => 'Sabre\Xml\Deserializer\keyValue',
      ];
      $tree = $service->parse(file_get_contents($xml_file));
      $tree_id = $tree[0]['attributes']['id'];
      $products_group = self::getProducts($tree);
      $product_fields = [];
      foreach ($products_group as $group_id => $groups) {
        foreach ($groups['{}listofproducts'] as $product) {
          $product_id = $product['attributes']['id'];
          $product_path = 'public://product_xml/en/'. $group_id. '/'. $product_id. '_en.xml';
          $product_file = \Drupal::service('file_system')->realpath($product_path);
          $product_xml_service = new Xml\Service();
          /*$product_xml_service->elementMap = [
            'product' => 'Sabre\Xml\Deserializer\keyValue',
          ];*/
          $product_detail = $product_xml_service->parse(file_get_contents($product_file));
          /*$product_data = new ProductHelper(402, $product_id);
          $data = $product_data->prepareData($product_detail[0]);
          $product_entity = $product_data->createProduct();
          print 'ok';
          print_r($product_entity->id());
          exit();*/
          foreach ($product_detail[0]['value'] as $product_value) {
            $field_key = str_replace('{}', '', $product_value['name']);
            $type = 'no-type';
            $attributes = $product_value['attributes'];
            if ($attributes['type']) {
              $type = $attributes['type'];
            }
            if (is_array($product_value['value'])) {
              $field_type = $product_value['value'];
            } else {
              $field_type = 'text';
            }
            $product_fields[$tree_id][$field_key][$type] = $field_type;
          }
        }
      }
      /*print_r($product_fields);
      exit();*/
    }
    $reader = new Service();
    
    print 'dsdsa';
    exit();
    /*return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: import')
    ];*/
  }

}
