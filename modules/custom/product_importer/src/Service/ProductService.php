<?php
namespace Drupal\product_importer\Service;

use Drupal\Core\Utility\Error;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\field_collection\Plugin\Field\FieldType\FieldCollection;
use Sabre\Xml;

class ProductService {
  protected $parent_id;
  protected $product_id;
  private $product_data;
  private $product_path;
  protected $tree;
  protected $temp_id;
  private $tmp;

  public function __construct() {
  }

  /**
   * @param mixed $parent_id
   */
  public function setParentId($parent_id) {
    $this->parent_id = $parent_id;
  }

  /**
   * @param mixed $product_id
   */
  public function setProductId($product_id) {
    $this->product_id = $product_id;
  }

  /**
   * @return mixed
   */
  public function getTree() {
    return $this->tree;
  }

  /**
   * @param mixed $tree
   */
  public function setTree($tree) {
    $this->tree = $tree;
  }

  public static function productLoad($entity_id) {
    $entity = \Drupal::entityTypeManager()->getStorage('products')->load($entity_id);
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if ($language && $entity && $entity->hasTranslation($language)) {
      $tmp = $entity->getTranslation($language);
      return $tmp;
    }
    return $entity;
  }

   public static function productLoadByName($name) {
    $query = \Drupal::entityQuery('products')
      ->condition('name', $name);
    $entity_ids = $query->execute();
    if (count($entity_ids)) {
      $id = reset($entity_ids);
      $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
      return $entity;
    }
    return FALSE;
  }

  public static function productLoadByProductID($product_id) {
    $query = \Drupal::entityQuery('products')
      ->condition('field_product_id', $product_id);
    $entity_ids = $query->execute();
    if (count($entity_ids)) {
      $id = reset($entity_ids);
      $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
      /*$language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($language && $entity->hasTranslation($language)) {
        return $entity->getTranslation($language);
      }*/
      return $entity;
    }
    return FALSE;
  }

  public function getProducts($groups, $parent_id = null) {
    $products = [];
    if ($parent_id) {
      //$this->temp_id[] = $parent_id;
    } else {
      $group_name = $groups[0]['value']['{}name'];
      $this->temp_id[] = $group_name;
    }
    foreach ($groups as $group) {
      $value = $group['value'];
      if (isset($value['{}listofgroups'])) {
      //if ($group['name'] == '{}product_group') {
        $group_id = $group['attributes']['id'];
        $group_name = $group['value']['{}name'];
        if (!$parent_id) {
          $this->tmp = $group_name;
          $this->parent_id = $group_id;
        } else {
          $this->temp_id[] = $group_name;
        }
        $group_data = $value['{}listofgroups'];
        //preserve array keep
        $products = $products + self::getProducts($group_data, $group_id);
      } else {
        $id = null;
        if (isset($group['attributes']['id'])) {
          $id = $group['attributes']['id'];
        }
        if ($id) {
          $group_name = $group['value']['{}name'];
          $products[$id] = $value;
          $this->tree[$group_name] = $this->temp_id;
        

          //$this->temp_id = [$this->tmp];
          //return $products;
        }
      }
    }
    array_pop($this->temp_id);
    return $products;
  }

  public static function parse($path, $options = array()) {
    $xml_file = \Drupal::service('file_system')->realpath($path);
    $service = new Xml\Service();
    if (isset($options['elementMap'])) {
      $service->elementMap = [
        $options['elementMap'] => 'Sabre\Xml\Deserializer\keyValue',
      ];
    }

    $tree = $service->parse(file_get_contents($xml_file));
    return $tree;
  }

  public function parseXML($path, $options = array()) {
    $this->product_path = $path;
    $xml_file = \Drupal::service('file_system')->realpath($path);
    
    $service = new Xml\Service();
    if (isset($options['elementMap'])) {
      $service->elementMap = [
        $options['elementMap'] => 'Sabre\Xml\Deserializer\keyValue',
      ];
    }

    $tree = $service->parse(file_get_contents($xml_file));
    return $tree;
  }

  public function prepareData($data) {
    $product_data = [];
    if (!$this->parent_id) throw new \Error('Parent ID is empty');
    if ($this->parent_id == 402) {
      $product_data = self::getPowerSystemsObject($data);
    }
    if ($this->parent_id == 406) {
      $product_data = self::getEquipmentObject($data);
    }
    if ($this->parent_id == 405) {
      $product_data = self::getAttachmentsObject($data);
    }
    $this->product_data = $product_data;

    return $this->product_data;
  }

  public static function importProductImage($entity_id, $id = 'entity_id') {
    if ($id == 'entity_id') {
      $entity = self::productLoad($entity_id);
    } else {
      $entity = self::productLoadByProductID($entity_id);
    }
    
    if ($entity) {
      $images = [];
      if ($entity->hasField('field_image_raw')) {
        $images_raw = $entity->get('field_image_raw')->getValue();

        foreach ($images_raw as $image_raw) {
          $value = $image_raw['value'];
          $type = '';
          if (strpos($value, 'yout') !== FALSE) continue;
          if (strpos($value, 'image') !== FALSE) {
            preg_match('/\$(\w)+\$/', $value, $matches);
            if (isset($matches[0])) {
              $type = str_replace('$', '', $matches[0]);
            }
            $file = self::saveImage($value, 'public://product_images', array('type' => $type));
            $images[] = [
              'target_id' => $file->id(),
              'alt' => $type
            ];
          }
        }
        if (count($images)) {
          $entity->field_listofimages = $images;
          \Drupal::logger('product_image_importer')->notice($entity->id());
          $entity->save();
        }
      }
    }
  }

  public function translateProduct() {
    return $this->createProduct(true);
  }

  public function createProduct($add_translate = false) {
    $user = \Drupal::currentUser();
    $tmp = [];
    $product_data = $this->product_data;
    
    $product_data['field_xml_path'] = $this->product_path;
    unset($product_data['optional_equipment']);
    unset($product_data['standard_equipment']);
    if (isset($product_data['field_rental'])) {
      $rental = $product_data['field_rental'];
      if (strtolower($rental) == 'y') {
        $product_data['field_rental_service'] = 'Yes';
      } else {
        $product_data['field_rental_service'] = 'No';
      }
      //override
      //$product_data['field_rental_service'] = 'No';
      unset($product_data['field_rental']);
    }

    if (isset($product_data['field_specifications'])) {
      $tmp['field_specifications'] = $product_data['field_specifications'];
      unset($product_data['field_specifications']);
    }
    if (isset($product_data['field_marketing_content'])) {
      $tmp['field_marketing_content'] = $product_data['field_marketing_content'];
      $marketing_image = [];
      foreach ($product_data['field_marketing_content'] as $field => $marketing_content) {
        if (strpos(strtolower($field), 'image') !== FALSE || strpos(strtolower($field), 'photo') !== FALSE || strpos(strtolower($field), 'gallery') !== FALSE) {
          foreach ($marketing_content as $key => $value) {
            $image = $value['contents'][0]['media'][0];
            if (strpos($image, '/is/image') !== FALSE) {
              $parse = parse_url($image);
              $scheme = $parse['scheme'];
              $host = $parse['host'];
              $path = $parse['path'];
              $image = "$scheme://$host$path";
              
            }
            if (!in_array($image, $marketing_image)) {
              $image = str_replace('_', '', $image);
              $marketing_image[] = $image;
            }
          }
        }
      }
      if (!isset($product_data['field_image_raw'])) {
        $product_data['field_image_raw'] = [];
      }
      $product_data['field_image_raw'] = array_unique(array_merge($marketing_image, $product_data['field_image_raw']));
      unset($product_data['field_marketing_content']);
    }
    if (isset($product_data['field_listofsalesfeatures'])) {
      $tmp['field_listofsalesfeatures'] = $product_data['field_listofsalesfeatures'];
      unset($product_data['field_listofsalesfeatures']);
    }
    if (isset($product_data['field_listoflinks'])) {
      $tmp['field_listoflinks'] = $product_data['field_listoflinks'];
      unset($product_data['field_listoflinks']);
    }
    if (isset($product_data['field_listofprices'])) {
      $tmp['field_listofprices'] = $product_data['field_listofprices'];
      unset($product_data['field_listofprices']);
    }
    if (isset($product_data['field_listofavailabilities'])) {
      $tmp['field_listofavailabilities'] = $product_data['field_listofavailabilities'];
      unset($product_data['field_listofavailabilities']);
    }

    $counter = 0;
    if (isset($product_data['field_image_raw'])) {
      foreach ($product_data['field_image_raw'] as $key => &$url) {
        $url = str_replace('_', '', $url);
        //if ($counter > 3) continue;
        if (strpos($url, '/is/image/') !== FALSE) { 
          //$file = self::saveImage($url, 'public://product_images');
          //$product_data['field_listofimages'][] = $file->id();
          $counter++;
        } else {
          unset($product_data['field_image_raw'][$key]);
        }
      }
    }
    $query = \Drupal::entityQuery('products')
      ->condition('field_product_id', $product_data['field_product_id']);
    $entity_ids = $query->execute();
    
    //unset($product_data['field_listofimages']);

    if (count($entity_ids)) {
      $id = reset($entity_ids);
      
      $entity_tmp = \Drupal::entityTypeManager()->getStorage('products')->load($id);
      $entity_tmp_arr = $entity_tmp->toArray();
      if ($add_translate) {
        if ($entity_tmp->hasTranslation('id')) {
          \Drupal::logger('check product translation')->notice('<pre>get translation : '. $id. '</pre>');
          $entity = clone $entity_tmp->getTranslation('id');
        } else {
          \Drupal::logger('check product translation')->notice('<pre>don\'t have translate, add translation : '. $id. '</pre>');
          $entity = clone $entity_tmp->addTranslation('id');  
          $entity->default_langcode = 0;
          $entity->save();
        }
        $entity->user_id = $user->id();
        $entity->status = 1;
        foreach ($product_data as $key => $value) {
          $entity->{$key} = $value;
        }
        $entity->field_listofimages = $entity_tmp_arr['field_listofimages'];
        $entity->field_image_raw = $entity_tmp_arr['field_image_raw'];
        $entity->field_image_raw = $entity_tmp_arr['field_image_raw'];
        $entity->field_group = $entity_tmp_arr['field_group'];
        $entity->field_rental_service = $entity_tmp_arr['field_rental_service'];
      } else {
        $entity = clone $entity_tmp;
        foreach ($product_data as $key => $value) {
          $entity->{$key} = $value;
        }
        $entity->status = 1;
        $entity->save();        
      }
    } else {
      if ($add_translate) {
        \Drupal::logger('check product translation')->notice('<pre> create new translation but dont have source</pre>');
        return;
      }
      $entity_tmp = \Drupal::entityTypeManager()->getStorage('products')->create($product_data);
      if ($add_translate) {
        if ($entity_tmp->hasTranslation('id')) {
          $entity = clone $entity_tmp->getTranslation('id');
        } else {
          $entity = clone $entity_tmp->addTranslation('id');  
        }
        $entity->user_id = $user->id();
      } else {
        $entity = clone $entity_tmp;
      }
      $entity->status = 1;
      $entity->save();
    }

    if (!$add_translate && isset($tmp['field_specifications']) && $tmp['field_specifications']) {
      $entity->field_specifications = [];
      foreach ($tmp['field_specifications'] as $table_key => $table_data) {
        $fieldCollectionItem = FieldCollectionItem::create(['field_name' => 'field_specifications']);
        $fieldCollectionItem->setHostEntity($entity);
        $fieldCollectionItem->set('field_spec_type', $table_key);
        $fieldCollectionItem->save();
        //$entity->field_specifications[] = $fieldCollectionItem;
        $entity->field_specifications->appendItem($fieldCollectionItem);
        //$entity->field_specifications[] = ['field_specifications' => $fieldCollectionItem];

        foreach ($table_data as $label => $value) {
          $fieldCollectionItem_child = FieldCollectionItem::create(['field_name' => 'field_spec_list']);
          $fieldCollectionItem_child->setHostEntity($fieldCollectionItem);
          $fieldCollectionItem_child->set('field_spec_label', $label);
          $fieldCollectionItem_child->set('field_spec_data', [
            'value' => [
              $value['header'],
              $value['row'],
            ],
            'rebuild' => [
              'count_cols' => count($value['header']),
              'count_rows' => count($value['row']),
              'rebuild' => 'Rebuild Table',
            ],
          ]);
          $fieldCollectionItem_child->save();
          $fieldCollectionItem->field_spec_list->appendItem($fieldCollectionItem_child);
          //$fieldCollectionItem->set('field_spec_list', $fieldCollectionItem_child);
        }
      }
    }
    if (isset($tmp['field_listofsalesfeatures']) && $tmp['field_listofsalesfeatures']) {
      $tmp['field_listofsalesfeatures'] = array_slice($tmp['field_listofsalesfeatures'], 0, 10);
      $entity->field_listofsalesfeatures = [];
      foreach ($tmp['field_listofsalesfeatures'] as $feature) {
        $fieldCollectionItem = FieldCollectionItem::create(['field_name' => 'field_listofsalesfeatures']);
        $fieldCollectionItem->setHostEntity($entity);

        $fieldCollectionItem->set('field_title', $feature['title']);
        $fieldCollectionItem->set('field_text', $feature['text']);
        if (isset($feature['images'])) {
          $fieldCollectionItem->set('field_image', $feature['images']);
        }
        if (isset($feature['field_media_raw'])) {
          $fieldCollectionItem->set('field_media_raw', $feature['images']);
        }
        $fieldCollectionItem->save();
        //$entity->field_listofsalesfeatures[] = $fieldCollectionItem;
        $entity->field_listofsalesfeatures->appendItem($fieldCollectionItem);
        //$entity->field_listofsalesfeatures[] = ['field_listofsalesfeatures' => $fieldCollectionItem];

        foreach ($feature['content'] as $content_feature) {
          $fieldCollectionItem_child = FieldCollectionItem::create(['field_name' => 'field_salesfeature']);
          $fieldCollectionItem_child->setHostEntity($fieldCollectionItem);
          $fieldCollectionItem_child->set('field_title', $content_feature['title']);
          $fieldCollectionItem_child->set('field_text', $content_feature['text']);
          if (isset($content_feature['images'])) {
            $fieldCollectionItem_child->set('field_image', $content_feature['images']);
          }
          if (isset($content_feature['images_raw'])) {
            $fieldCollectionItem_child->set('field_media_raw', $content_feature['images_raw']);
          }
          $fieldCollectionItem_child->save();
          //$fieldCollectionItem->set('field_salesfeature', $fieldCollectionItem_child);
          $fieldCollectionItem->field_salesfeature->appendItem($fieldCollectionItem_child);
        }
      }
    }
    
    $langcode = 'en';
    if ($add_translate) {
      $entity->save();
      $langcode = 'id';
    }
    $queue = \Drupal::queue('product_snapshot');
    $id = $entity->id();
    $data = [ 
      'id' => $id,
      'langcode' => $langcode
    ]; 
    $queue->createItem($data);
    return $entity;
  }

  public function saveTerm($vid, $name, $parent_id = null, $fields = array(), $translate = null) {
    $user = \Drupal::currentUser();

    $query = \Drupal::database()->select('taxonomy_term_field_data', 't');
    $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
    $result = $query
        ->fields('t')
        ->fields('h', array('parent'))
        ->condition('t.vid', $vid)
        ->condition('t.name', $name);

    if ($parent_id) {
      $result = $result->condition('h.parent', $parent_id);
    }
    $result = $result
      ->condition('t.default_langcode', 1)
      ->orderBy('t.weight')
      ->orderBy('t.name')
      ->execute();
    $create_new = TRUE;
    foreach ($result as $term) {
      $term_id = $term->tid;
      $create_new = FALSE;
    }

    if ($create_new) {
      if (strlen($name) > 180) {
        $pos=strpos($name, ' ', 180);
        if ($pos) {
          $name = substr($name, 0, $pos );
        }
      }
      $description = $name;
      $term_data = [
        'name' => $name,
        'vid' => $vid,
        'description' => $description
      ];
      $term_data += $fields;
      if ($parent_id) {
        $term_data['parent'] = $parent_id;
      }
      $term = \Drupal\taxonomy\Entity\Term::create($term_data);
      $term->save();
      $term_id = $term->tid->value;
    }
    if ($translate) {
      $term = \Drupal\taxonomy\Entity\Term::load($term_id);
      if (!$term->hasTranslation('id')) {
        $term_translate = clone $term->addTranslation('id');  
      } else {
        $term_translate = clone $term->getTranslation('id');
      }
      $term_translate->name = $translate;
      $term_translate->description = $translate;
      $term_translate->save();
      $term_id = $term_translate->tid->value;
    }
    return $term_id;
  }

  public static function saveImage($url, $destination, $options = array()) {
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    $tmp = parse_url($url);
    $filename = basename($tmp['path']);
    $tmp = pathinfo($filename);

    if (strlen($filename) > 100) {
      $pos=strpos($filename, ' ', 100);
      if ($pos) {
        $filename = substr($filename, 0, $pos );
      }
    } else {
      $filename = md5(mt_rand());
    }

    if (isset($options['type'])) {
      $filename .= '-'. $options['type'];
    }
    if (!isset($tmp['extension'])) $filename .= '.jpg';

    $data = self::getFileContents($url);
    //$data = file_get_contents($url);

    $file = file_save_data($data, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
    return $file;
  }

  protected static function getFileContents($url) {
    $ch = curl_init ($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
    $raw = curl_exec($ch);
    curl_close ($ch);
    return $raw;
  }

  protected function saveFile($url, $destination) {
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    $tmp = parse_url($url);
    $filename = basename($tmp['path']);

    if ($filename) {
      $pos=strpos($filename, ' ', 100);
      if ($pos) {
        $filename = substr($filename, 0, $pos );
      }
    } else {
      $filename = md5(mt_rand());
    }

    //$data = file_get_contents($url);
    $data = self::getFileContents($url);

    $file = file_save_data($data, $destination. '/'. $filename, FILE_EXISTS_REPLACE);
    return $file;
  }

  private function saveEquipmentTree($groups, $vid, $parent_id = null) {
    $tmp = [];
    return $tmp;

    foreach ($groups as $group) {
      $value = $group['value'];
      if (count($value) == 1) {
        if (is_array($value)) {
          $name = $value[0]['value'];
          if (is_array($name)) {
            $name = $name[0]['value'];
          }
          if ($parent_id) {

            $terms =  taxonomy_term_load_multiple_by_name(end($parent_id), $vid);
            if (count($terms)) {
              $parent_term = reset($terms);
              $term_id = $parent_term->tid->value;
              $childs =  taxonomy_term_load_multiple_by_name($name, $vid);
              if (count($childs)) {
                $child = reset($childs);
                $id = $child->tid->value;
              } else {
                $id = self::saveTerm($vid, $name, $term_id, ['field_description' => $name]);
              }
              $tmp[] = $id;
            }
          } else {
            if ($name && is_string($name)) {
              self::saveTerm($vid, $name, null, ['field_description' => $name]);
            }
          }
        } else {
          
        }
      } else {
        if (!is_array($value[0]['value']) && $value) {
          $name = $value[0]['value'];
          if (!$parent_id) {
            self::saveTerm($vid, $name, null, ['field_description' => $name]);
          } else {
            $terms =  taxonomy_term_load_multiple_by_name(end($parent_id), $vid);
            if (count($terms)) {
              $parent_term = reset($terms);
              $term_id = $parent_term->tid->value;

              self::saveTerm($vid, $name, $term_id, ['field_description' => $name]);
            }
          }
          $parent_id[] = $name;
        }
        $tmp = array_merge($tmp, self::saveEquipmentTree($value, $vid, $parent_id));
        array_pop($parent_id);
      }
    }
    return $tmp;

    /*$tids = [];
    foreach ($group as $parent) {
      foreach ($parent['value'] as $child) {
        if ($child['name'] == '{}listofequipment') {
          $new =  self::saveEquipmentTree($child['value'], $vid, $parent);
          $check = array_search(key($new), reset($tids));
          if ($check) {
            $tids[$parent_id][$check] = $new;
          } else {
            $tids = array_merge($tids, $new);
          }

        } else {
          $parent = $child['value'];
          if (!$parent_id) {
          } else {
            $tids[$parent_id][] = $parent;
          }
          //return $parent_term_id;
        }
      }
    }
    return $tids;*/
  }

  private function getProductListImages($field_value) {
    $data_list = [];
    foreach ($field_value as $v) {
      $image_url = $v['value'][0]['value'];
      $image_type = $v['attributes']['type'];
      if (strpos($image_type, 'pdf') === FALSE) {
        $file = self::saveImage($image_url, 'public://product_images', array('type' => $image_type));
        $data_list[] = $file->id();
      }
    }
    return $data_list;
  }

  private function getProductListRawImages($field_value) {
    $data_list = [];

    foreach ($field_value as $v) {
      $type = $v['attributes']['type'];
      if (strpos($type, 'pdf') === FALSE) {
        $image_url = $v['value'][0]['value'];
        if (strpos($image_url, '/is/image') !== FALSE) {
          $parse = parse_url($image_url);
          $scheme = $parse['scheme'];
          $host = $parse['host'];
          $path = $parse['path'];
          $image_url = "$scheme://$host$path";
          $image_url = str_replace('_', '', $image_url);
        }
        if (!in_array($image_url, $data_list)) {
          $data_list[] = $image_url;
        }
      }
    }
    return $data_list;
  }

  private function getProductSpecification($field_value) {
    $table = [];
    foreach ($field_value as $v) {
      if ($v['attributes']['type'] == 'measurement') {
        foreach($v['value'] as $spec_value) {
          $spec_value['name'] = str_replace('{}', '', $spec_value['name']);
          if ($spec_value['name'] == 'title') {
            $label = $spec_value['value'];
          }
          if ($spec_value['name'] == 'name') {
            $header = $spec_value['value'];
          }
          if ($spec_value['name'] == 'value') {
            $value = $spec_value['value'];
          }
          if ($spec_value['name'] == 'unit') {
            $value .= ' '. $spec_value['value'];
            $type = $spec_value['attributes']['type'];
            $table[$type][$label]['header'][] = $header;
            $table[$type][$label]['row'][] = $value;
          }
        }
      } else {
        foreach($v['value'] as $spec_value) {
          $spec_value['name'] = str_replace('{}', '', $spec_value['name']);
          if ($spec_value['name'] == 'title') {
            $label = $spec_value['value'];
          }
          if ($spec_value['name'] == 'name') {
            $header = $spec_value['value'];
          }
          if ($spec_value['name'] == 'value-text') {
            $value = $spec_value['value'];
            $table['english'][$label]['header'][] = $header;
            $table['english'][$label]['row'][] = $value;

            $table['metric'][$label]['header'][] = $header;
            $table['metric'][$label]['row'][] = $value;
          }
        }
      }
    }
    if (isset($table['english'])) {
      unset($table['english']);
    }
    return $table;
  }

  private function getProductMarketingContent($field_value) {
    $vid = 'marketing_tags';
    $data = [];
    foreach ($field_value as $k => $v) {
      $tags = [];
      $title = '';
      $type = $v['attributes']['type'];
      $media_parent = '';
      foreach($v['value'] as $marketing_value) {
        $marketing_value['name'] = str_replace('{}', '', $marketing_value['name']);
        if ($marketing_value['name'] == 'title') {
          $title = $marketing_value['value'];
        }
        if ($marketing_value['name'] == 'media') {
          $media_parent = $marketing_value['value'][0]['value'];
        }
        if ($marketing_value['name'] == 'metadata') {
          if ($marketing_value['value'][0]['value']) {
            $term_id = self::saveTerm($vid, $marketing_value['value'][0]['value']);
          } else {
            $term_id = self::saveTerm($vid, $marketing_value['attributes']['type']);
          }
          $tags[] = $term_id;
        }
        $contents = [];
        $content_data = [];
        if ($marketing_value['name'] == 'content') {
          foreach($marketing_value['value'] as $content) {
            $content['name'] = str_replace('{}', '', $content['name']);
            if ($content['name'] == 'title') {
              $content_data['title'] = $content['value'];
            }
            if ($content['name'] == 'metadata') {
              if ($content['value'][0]['value']) {
                $term_id = self::saveTerm($vid, $content['value'][0]['value']);
              } else {
                $term_id = self::saveTerm($vid, $content['attributes']['type']);
              }
              $content_data['tags'][] = $term_id;
            }
            if ($content['name'] == 'text') {
              $content_data['text'] = $content['value'];
            }
            if ($content['name'] == 'media') {
              //$file = self::saveFile($content['value'][0]['value'], 'public://product_marketing_content_media');
              //$content_data['media'][] = $file->id();
              $media_raw = $content['value'][0]['value'];
              $media_raw = str_replace('_', '', $media_raw);
              $content_data['media'][] = $media_raw;
            }
          }
          $contents[] = $content_data;
          $data[$title][] = [
            'title' => $title,
            'type' => $type,
            'tags' => $tags,
            'media' => $media_parent,
            'contents' => $contents
          ];
        }
      }
    }
    return $data;
  }

  private function getProductSalesFeature($field_value) {
    $contents = [];
    foreach ($field_value as $v) {
      $images = [];
      foreach($v['value'] as $value) {
        $value['name'] = str_replace('{}', '', $value['name']);
        if ($value['name'] == 'name') {
          $title = $value['value'];
        }
        if ($value['name'] == 'paragraph') {
          $text =  $value['value'];
        }
        if ($value['name'] == 'listofimages') {
          $images = [];
          foreach($value['value'] as $image) {
            $image_url = $image['value'][0]['value'];
            $image_type = $image['attributes']['type'];
            /*$file = self::saveImage($image_url, 'public://product_images', array('type' => $image_type));
            $images[] = $file->id();*/
            $images[] = $image_url;
          }
        }
        $content = [];
        if ($value['name'] == 'listofsalesfeatures') {
          $salescontent = [];
          foreach($value['value'] as $content_value) {
            $salesimages = [];
            foreach($content_value['value'] as $val) {
              $val['name'] = str_replace('{}', '', $val['name']);
              if ($val['name'] == 'name') {
                $salestitle = $val['value'];
              }
              if ($val['name'] == 'paragraph') {
                $salestext =  $val['value'];
              }
              if ($val['name'] == 'listofimages') {
                foreach($val['value'] as $image) {
                  $image_url = $image['value'][0]['value'];
                  $image_type = $image['attributes']['type'];
                  /*$file = self::saveImage($image_url, 'public://product_images', array('type' => $image_type));
                  $salesimages[] = $file->id();*/
                  $salesimages[] = $image_url;
                }
                $salesimage = $salesimages;
              }
            }
            $salescontent[] = [
              'title' => $salestitle,
              'text' => $salestext,
              'images_raw' => $salesimages,
            ];
          }
          $content = $salescontent;
        }

      }
      $contents[] = [
        'title' => $title,
        'text' => $text,
        'images_raw' => $images,
        'content' => $content
      ];
    }
    return $contents;
  }

  private function getProductLinkList($field_value) {
    $data_list = [];
    foreach ($field_value as $v) {
      foreach($v['value'] as $value) {
        $value['name'] = str_replace('{}', '', $value['name']);
        if ($value['name'] == 'link_label') {
          $title = $value['value'];
        }
        if ($value['name'] == 'link_url') {
          $url = $value['value'];
        }
        if ($value['name'] == 'type') {
          $type = $value['value'];
        }
      }

      $data_list[] = [
        'title' => $title,
        'url' => $url,
        'type' => $type
      ];
    }
    return $data_list;
  }

  private function getAttachmentsObject($product_raw) {
    $product_raw_data = $product_raw['value'];
    $product_sort = $product_raw['attributes']['sort'];
    
    $group_vid = 'group';
    $parent_term_id = null;
    
    foreach ($this->tree as $item) {
      $tmp = explode('|', $item);
      $item = $tmp[0];
      $translate = null;
      if (count($tmp) > 1) $translate = $tmp[1];
      //if ($item == 'Attachments') $item = 'Work Tools';
      $parent_term_id = self::saveTerm($group_vid, $item, $parent_term_id, [], $translate);
    }
    
    $product_data = [
      'type' => 'attachments',
      'field_product_id' => $this->product_id,
      'field_group' => $parent_term_id
    ];
    foreach ($product_raw_data as $product_raw_value) {
      $field_name = $product_raw_value['name'];
      $field_value = $product_raw_value['value'];
      $field_name = str_replace('{}', '', $field_name);

      if ($field_name == 'listoftechspecvalues'){
        continue;
      }

      switch ($field_name) {
        case 'name':
          //skip
          break;
        case 'sale_content':
          foreach ($field_value as $value) {
            $value['name'] = str_replace('{}', '', $value['name']);
            if ($value['name'] == 'listofpartnumbers') {
              $product_data['field_listofpartnumbers'] = $value['value'][0]['value'];
            }
            if ($value['name'] == 'listofsellers') {
              $data = $value['value'][0]['value'];
              $product_data['field_listofsellers'] = $data[0]['value'];
            }
            if ($value['name'] == 'listofprices') {
              $data = $value['value'][0]['value'];
              foreach ($data as $v) {
                $v['name'] = str_replace('{}', '', $v['name']);
                if ($v['name'] == 'name') {
                  $title = $v['value'];
                }
                if ($v['name'] == 'label') {
                  $label = $v['value'];
                }
                if ($v['name'] == 'value') {
                  $value = $v['value'];
                }
                if ($v['name'] == 'metadata') {
                  $tag = [];
                  foreach ($v['value'] as $metadata) {
                    $tag[] = self::saveTerm('metadata', $metadata['value']);
                  }
                }
              }
              $product_data['field_listofprices'] = [
                'title' => $title,
                'label' => $label,
                'value' => $value,
                'tag' => $tag
              ];
            }
            /*if (isset($value['name']) && $value['name'] == 'listofavailabilities') {
              $data = $value['value'][0]['value'];
              foreach ($data as $v) {
                $v['name'] = str_replace('{}', '', $v['name']);
                if ($v['name'] == 'name') {
                  $title = $v['value'];
                }
                if ($v['name'] == 'value') {
                  $value = $v['value'];
                }
                if ($v['name'] == 'metadata') {
                  $tag = [];
                  foreach ($v['value'] as $metadata) {
                    $tag[] = self::saveTerm('metadata', $metadata['value']);
                  }
                }
              }
              $product_data['field_listofavailabilities'] = [
                'title' => $title,
                'value' => $value,
                'tag' => $tag
              ];
            }*/
          }
          break;
        case 'listofrelationships':
          $data_list = [];
          foreach ($field_value as $v) {
            $data_list[] = $v['value'][0]['value'];
          }
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofmetadata':
          $vid = 'metadata';
          $data_list = [];
          foreach ($field_value as $v) {
            $parent_name = $v['value'][0]['value'];
            $parent_term_id = self::saveTerm($vid, $parent_name);
            $data_list[] = $parent_term_id;
          }
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofimages':
          $data_list = $this->getProductListRawImages($field_value);

          $field_value = $data_list;
          //$field_name = 'field_'. $field_name;
          $field_name = 'field_image_raw';
          break;
        case 'specifications':
          $data_list = $this->getProductSpecification($field_value);

          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'marketing_content':
          $data_list = $this->getProductMarketingContent($field_value);
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'brand':
          $vid = 'brands';
          $parent_term_id = self::saveTerm($vid, $field_value);

          $field_value = $parent_term_id;
          $field_name = 'field_'. $field_name;
          break;
        case 'property':
          if (in_array($field_name, ['buildandquote', 'competecompare'])) continue;
          $field_name = 'field_'. $product_raw_value['attributes']['type'];
          break;
        case 'last_modified':
        case 'listofmetadata':
          continue;
        default :
          $field_name = 'field_'. $field_name;
      }
      $product_data[$field_name] = $field_value;
    }
    unset($product_data['sale_content']);
    $product_data['field_sort'] = $product_sort;
    return $product_data;
  }

  private function getEquipmentObject($product_raw) {
    $product_raw_data = $product_raw['value'];
    $product_sort = $product_raw['attributes']['sort'];
    
    $group_vid = 'group';
    $parent_term_id = null;
    foreach ($this->tree as $item) {
      $tmp = explode('|', $item);
      $item = $tmp[0];
      $translate = null;
      if (count($tmp) > 1) $translate = $tmp[1];
      //if ($item == 'Equipment') $item = 'Machines';
      $parent_term_id = self::saveTerm($group_vid, $item, $parent_term_id, [], $translate);
    }
    $product_data = [
      'type' => 'equipment',
      'field_product_id' => $this->product_id,
      'field_group' => $parent_term_id
    ];
    foreach ($product_raw_data as $product_raw_value) {
      $field_name = $product_raw_value['name'];
      $field_value = $product_raw_value['value'];
      $field_name = str_replace('{}', '', $field_name);

      if ($field_name == 'listoftechspecvalues'){
        continue;
      }

      switch ($field_name) {
        case 'name':
          //skip
          break;
        case 'standard_equipment':
          $vid = 'standard_equipment';
          $equipments = self::saveEquipmentTree($field_value, $vid);
          
          $field_value = $equipments;
          $field_name = 'field_'. $field_name;
          break;
        case 'optional_equipment':
          $vid = 'optional_equipment';
          $equipments = self::saveEquipmentTree($field_value, $vid);

          $field_value = $equipments;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofimages':
          //$data_list = $this->getProductListImages($field_value);
          $data_list = $this->getProductListRawImages($field_value);

          $field_value = $data_list;
          //$field_name = 'field_'. $field_name;
          $field_name = 'field_image_raw';
          break;
        case 'specifications':
          $data_list = $this->getProductSpecification($field_value);

          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'marketing_content':
          $data_list = $this->getProductMarketingContent($field_value);
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'brand':
          $vid = 'brands';
          $parent_term_id = self::saveTerm($vid, $field_value);

          $field_value = $parent_term_id;
          $field_name = 'field_'. $field_name;
          break;
        case 'property':
          if (in_array($field_name, ['buildandquote', 'competecompare'])) continue;
          $field_name = 'field_'. $product_raw_value['attributes']['type'];
          break;
        case 'listofsalesfeatures':
          $data_list = $this->getProductSalesFeature($field_value);
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'listoflinks':
          $data_list = $this->getProductLinkList($field_value);

          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'last_modified':
        case 'listofmetadata':
          continue;
        default :
          $field_name = 'field_'. $field_name;
      }

      $product_data[$field_name] = $field_value;
    }
    /*if (isset($product_data['field_listoflinks'])) {
      unset($product_data['field_listoflinks']);
    }
    if (isset($product_data['field_listofsalesfeatures'])) {
      unset($product_data['field_listofsalesfeatures']);
    }
    if (isset($product_data['field_marketing_content'])) {
      unset($product_data['field_marketing_content']);
    }
    if (isset($product_data['field_specifications'])) {
      unset($product_data['field_specifications']);
    }*/
    $product_data['field_sort'] = $product_sort;
    return $product_data;
  }

  private function getPowerSystemsObject($product_raw) {
    $product_raw_data = $product_raw['value'];
    $product_sort = $product_raw['attributes']['sort'];

    $group_vid = 'group';
    $parent_term_id = null;
    foreach ($this->tree as $item) {
      $tmp = explode('|', $item);
      $item = $tmp[0];
      $translate = null;
      if (count($tmp) > 1) $translate = $tmp[1];
      //if ($item == 'Power Systems') $item = 'Engines';
      $parent_term_id = self::saveTerm($group_vid, $item, $parent_term_id, [], $translate);
    }
    $product_data = [
      'type' => 'power_systems',
      'field_product_id' => $this->product_id,
      'field_group' => $parent_term_id
    ];
    foreach ($product_raw_data as $product_raw_value) {
      $field_name = $product_raw_value['name'];
      $field_value = $product_raw_value['value'];
      $field_name = str_replace('{}', '', $field_name);

      if ($field_name == 'listoftechspecvalues'){
        continue;
      }

      switch ($field_name) {
        case 'name':
          //skip
          break;
        case 'standard_equipment':
          $vid = 'standard_equipment';
          $equipments = self::saveEquipmentTree($field_value, $vid);
          $field_value = $equipments;
          $field_name = 'field_'. $field_name;
          break;
        case 'optional_equipment':
          $vid = 'optional_equipment';
          $equipments = self::saveEquipmentTree($field_value, $vid);
          
          $field_value = $equipments;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofimages':
          //$data_list = $this->getProductListImages($field_value);
          $data_list = $this->getProductListRawImages($field_value);

          $field_value = $data_list;
          //$field_name = 'field_'. $field_name;
          $field_name = 'field_image_raw';
          break;
        case 'specifications':
          $data_list = $this->getProductSpecification($field_value);

          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofrelationships':
          $data_list = [];
          foreach ($field_value as $v) {
            $data_list[] = $v['value'][0]['value'];
          }
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'marketing_content':
          $data_list = $this->getProductMarketingContent($field_value);
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'listofmetadata':
          $vid = 'metadata';
          $data_list = [];
          /*foreach ($field_value as $v) {
            $parent_name = $v['value'][0]['value'];
            $parent_term_id = self::saveTerm($vid, $parent_name);
            $data_list[] = $parent_term_id;
          }*/
          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'brand':
          $vid = 'brands';
          $parent_term_id = self::saveTerm($vid, $field_value);

          $field_value = $parent_term_id;
          $field_name = 'field_'. $field_name;
          break;
        case 'property':
          if (in_array($field_name, ['buildandquote', 'competecompare'])) continue;
          $field_name = 'field_'. $product_raw_value['attributes']['type'];
          break;
        case 'listoflinks':
          $data_list = $this->getProductLinkList($field_value);

          $field_value = $data_list;
          $field_name = 'field_'. $field_name;
          break;
        case 'last_modified':
        case 'listofmetadata':
          continue;
          break;
        default :
          $field_name = 'field_'. $field_name;
      }

      $product_data[$field_name] = $field_value;
    }
    $product_data['field_sort'] = $product_sort;
    return $product_data;
  }
}
