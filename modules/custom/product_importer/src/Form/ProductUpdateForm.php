<?php

namespace Drupal\product_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Sabre\Xml;
use Drupal\product_importer\Service\ProductService;
/**
 * Class ProductUpdateForm.
 *
 * @package Drupal\product_importer\Form
 */
class ProductUpdateForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    $options = [];
    $vid = 'group';
    $groups = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1);
    foreach ($groups as $term) {
      $term_name = $term->name;
      $options[$term->tid] = $this->t($term_name);
    }
    $form['file_to_import'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Group to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];
    $files = file_scan_directory('public://product_xml/J210/', '/[\w]+tree_en\.xml/');
    $options = [];
    foreach($files as $file) {
      $options[$file->filename] = $this->t($file->filename);
    }
    
    $form['file_to_import'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('File to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];

    $form['action'] = [
      '#type' => 'radios',
      '#options' => [
        'import_image' => $this->t('Import Image'),
        'translate_product' => $this->t('Translate Product'),
        'translate_label_product' => $this->t('Translate Display Label'),
        'rental_service_no' => $this->t('Update Rental Service to: No'),
        'update_sort' => $this->t('Update Sort'),
      ],
      '#default_value' => 'import_product',
      '#title' => $this->t('File to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    // Display result.
    $operations = [];
    $action = $form_state->getValue('action');
    $tids = [];
    $group_cache = [];

    foreach ($form_state->getValue('file_to_import') as $file) {
      if (!$file) continue;

      $level1_file = str_replace('tree_en.xml', '', $file);
      $file_path = 'public://product_xml/J210/'. $file;
      $language = 'en';
      if ($action == 'translate_product') $language = 'id';
      $folder = file_scan_directory('public://product_xml/'. $language. '/'. $level1_file, '/[\w]+_'. $language. '\.xml/');
      foreach ($folder as $product_path => $sub_folder) {
        $drupal_uri = 'public://product_xml/'. $language. '/';
        $tmp = str_replace($drupal_uri, '', $sub_folder->uri);
        $tmp = explode('/', $tmp);
        if (count($tmp) == 4) {
          $tree_id = $tmp[0];
          $group_id = $tmp[2];

          $product_id = array_pop($tmp);
          $product_id = str_replace('_'. $language. '.xml', '', $product_id);
          
          
          $products_service = new ProductService();
          $tree = [];
          $group_folder = $drupal_uri;
          foreach ($tmp as $key => $parent_xml) {
            $xml_file = $group_folder. $parent_xml. '_'. $language. '.xml';
            if (!isset($group_cache[$parent_xml])) {
              $xml = $products_service->parseXML($xml_file, ['elementMap' => '{}product_group']);
              $group_name = $xml[0]['value']['{}name'];
              if ($action == 'translate_product') {
                $xml_file = str_replace('id', 'en', $xml_file);
                $xml = $products_service->parseXML($xml_file, ['elementMap' => '{}product_group']);
                $group_name_tmp = $xml[0]['value']['{}name'];
                $group_name = $group_name_tmp. '|'. $group_name;
              }
              $group_cache[$parent_xml] = $group_name;
            } else {
              $group_name = $group_cache[$parent_xml];
            }
            $tree[] = $group_name;
            $group_folder .= $parent_xml .'/';
          }
          if ($action == 'import_image') {
            $product_entity = ProductService::productLoadByProductID($product_id);
            if ($product_entity) {
              $entity_id = $product_entity->id();
              $operations[] = array('product_importer_import_image', array($entity_id));
            }
          } else if ($action == 'translate_product') {
            //$product_path = str_replace('en', 'id', $product_path);
            $operations[] = array('product_importer_translate_product', array($tree_id, $group_id, $product_id, $product_path, $tree));
          } else if ($action == 'rental_service_no') {
            $operations[] = array('product_importer_rental_service', array($product_id));
          } else if ($action == 'update_sort') {
            $operations[] = array('product_importer_update_sort', array($product_id, $product_path));
          } else if ($action == 'translate_label_product') {
            $_SERVER['translate_label_product'] = [];
            $operations[] = array('product_importer_translate_group_taxonomy', array($product_path, $product_id));
          }
        }
      }
    }

    $batch = array(
      'title' => t($action),
      'operations' => $operations,
      'finished' => 'product_importer_import_product_finished_callback',
      'file' => drupal_get_path('module', 'product_importer') . '/product_importer.inc',
    );
  
    batch_set($batch);
  }

}
