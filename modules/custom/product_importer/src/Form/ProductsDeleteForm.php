<?php

namespace Drupal\product_importer\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Products entities.
 *
 * @ingroup product_importer
 */
class ProductsDeleteForm extends ContentEntityDeleteForm {


}
