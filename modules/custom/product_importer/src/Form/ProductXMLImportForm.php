<?php

namespace Drupal\product_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Sabre\Xml;
use Drupal\product_importer\Service\ProductService;

/**
 * Class ProductXMLImportForm.
 *
 * @package Drupal\product_importer\Form
 */
class ProductXMLImportForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_xml_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    $options = [];
    $vid = 'group';
    $groups = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, 0, 1);
    foreach ($groups as $term) {
      $term_name = $term->name;
      $options[$term->tid] = $this->t($term_name);
    }
    $form['file_to_import'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Group to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];
    /*$form['action'] = [
      '#type' => 'radios',
      '#options' => [
        'import_product' => $this->t('Import Product'),
        'import_image' => $this->t('Import Image'),
        //'update_rental' => $this->t('Update Rental'),
        //'import_image_raw' => $this->t('Import Raw Image'),
        //'translate_id' => $this->t('Translate to ID')
      ],
      '#default_value' => 'import_image',
      '#title' => $this->t('File to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];*/
    $files = file_scan_directory($config->get('import_path'), '/[\w]+tree_en\.xml/');
    $options = [];
    foreach($files as $file) {
      $options[$file->filename] = $this->t($file->filename);
    }
    
    $form['file_to_import'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('File to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];

    $form['action'] = [
      '#type' => 'radios',
      '#options' => [
        'import_product' => $this->t('Import Product')
      ],
      '#default_value' => 'import_product',
      '#title' => $this->t('File to Import'),
      '#description' => $this->t('Select XML file to import'),
    ];

    $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit'),
    ];

    return $form;
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('cron_import.settings');
    // Display result.
    $operations = [];
    $action = $form_state->getValue('action');
    $tids = [];
    $group_cache = [];

    foreach ($form_state->getValue('file_to_import') as $file) {
      if (!$file) continue;

      $level1_file = str_replace('tree_en.xml', '', $file);
      //$file_path = 'public://product_xml/J210/'. $file;
      $file_path = $config->get('import_path'). $file;
      //$folder = file_scan_directory('public://product_xml/en/'. $level1_file, '/[\w]+_en\.xml/');
      $folder = file_scan_directory($config->get('import_path'). '/en/'. $level1_file, '/[\w]+_en\.xml/');
      foreach ($folder as $product_path => $sub_folder) {

        //$drupal_uri = 'public://product_xml/en/';
        $drupal_uri = $config->get('import_path'). '/en/';
        $tmp = str_replace($drupal_uri, '', $sub_folder->uri);
        $tmp = explode('/', $tmp);
        if (count($tmp) == 4) {
          $tree_id = $tmp[0];
          $group_id = $tmp[2];

          $product_id = array_pop($tmp);
          $product_id = str_replace('_en.xml', '', $product_id);
          
          
          $products_service = new ProductService();
          $tree = [];
          $group_folder = $drupal_uri;
          foreach ($tmp as $key => $parent_xml) {
            $xml_file = $group_folder. $parent_xml. '_en.xml';
            if (!isset($group_cache[$parent_xml])) {
              $xml = $products_service->parseXML($xml_file, ['elementMap' => '{}product_group']);
              $group_name = $xml[0]['value']['{}name'];
              $group_cache[$parent_xml] = $group_name;
            } else {
              $group_name = $group_cache[$parent_xml];
            }
            $tree[] = $group_name;
            $group_folder .= $parent_xml .'/';
          }

          if ($action == 'import_product') {
            $operations[] = array('product_importer_import_product', array($tree_id, $group_id, $product_id, $product_path, $tree));
          } else if ($action == 'import_image') {
            $product_entity = ProductService::productLoadByProductID($product_id);
            if ($product_entity) {
              $entity_id = $product_entity->id();
              $operations[] = array('product_importer_import_image', array($entity_id));
            }
          } else if ($action == 'translate_product') {
            //if ($product_id != '18089285') continue;
            $product_path = str_replace('en', 'id', $product_path);
            /*$product_service = new ProductService();
            $product_service->setParentId($tree_id);
            $product_service->setProductId($product_id);
            $product_service->setTree($tree);

            $product_data = $product_service->parseXML($product_path);
            $update = TRUE;
            $message = '';
            $product_service->prepareData($product_data[0]);
            $product_service->translateProduct();

            exit();*/
            
            $operations[] = array('product_importer_translate_product', array($tree_id, $group_id, $product_id, $product_path, $tree));
          } else if ($action == 'rental_service_no') {
            $operations[] = array('product_importer_rental_service', array($product_id));
          } else if ($action == 'translate_label_product') {
            $operations[] = array('product_importer_translate_group_taxonomy', array($product_path, $product_id));
          }
        }
      }
    }
    exit();
    //exit();
    $batch = array(
      'title' => t($action),
      'operations' => $operations,
      'finished' => 'product_importer_import_product_finished_callback',
      'file' => drupal_get_path('module', 'product_importer') . '/product_importer.inc',
    );
  
    batch_set($batch);
  }

}
