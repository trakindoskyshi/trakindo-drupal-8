<?php

namespace Drupal\product_importer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Products type entities.
 */
interface ProductsTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
