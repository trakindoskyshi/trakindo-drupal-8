<?php

namespace Drupal\product_importer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Products type entity.
 *
 * @ConfigEntityType(
 *   id = "products_type",
 *   label = @Translation("Products type"),
 *   handlers = {
 *     "list_builder" = "Drupal\product_importer\ProductsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\product_importer\Form\ProductsTypeForm",
 *       "edit" = "Drupal\product_importer\Form\ProductsTypeForm",
 *       "delete" = "Drupal\product_importer\Form\ProductsTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\product_importer\ProductsTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "products_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "products",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/products_type/{products_type}",
 *     "add-form" = "/admin/structure/products_type/add",
 *     "edit-form" = "/admin/structure/products_type/{products_type}/edit",
 *     "delete-form" = "/admin/structure/products_type/{products_type}/delete",
 *     "collection" = "/admin/structure/products_type"
 *   }
 * )
 */
class ProductsType extends ConfigEntityBundleBase implements ProductsTypeInterface {

  /**
   * The Products type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Products type label.
   *
   * @var string
   */
  protected $label;

}
