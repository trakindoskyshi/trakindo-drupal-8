<?php

namespace Drupal\product_importer\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Products entities.
 */
class ProductsViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['products']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Products'),
      'help' => $this->t('The Products ID.'),
    );

    return $data;
  }

}
