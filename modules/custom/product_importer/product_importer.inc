<?php
use Drupal\product_importer\Service\ProductService;

function product_importer_translate_group_taxonomy($product_path, $product_id, &$context) {
  $product_path_original = $product_path;
  $product_path = str_replace('en', 'id', $product_path);

  $product_service = new ProductService();

  $product_data_original = $product_service->parseXML($product_path_original);
  $product_data = $product_service->parseXML($product_path);
  $product_entity = ProductService::productLoadByProductID($product_id);
  if ($product_entity) {
    $group = $product_entity->get('field_group')->getValue();
    if ($group) {
      $tid = $group[0]['target_id'];
      if ($tid) {
        $term = \Drupal\taxonomy\Entity\Term::load($tid);
        $field_display_label = $term->get('field_display_label')->getValue();  
      }
      $display_label = [];
      if (count($field_display_label)) {
        $display_label = array_map('trim', explode("\n", $field_display_label[0]['value']));
        $display_label_original = $display_label;
        $new_display_label = [];
        foreach ($product_data_original[0]['value'] as $key => $value) {

          $field_name = str_replace('{}', '', $value['name']);
          if ($field_name == 'specifications') {

            $specifications = $value['value'];
            foreach ($specifications as $spec_key => $spec_value) {
              foreach ($spec_value['value'] as $detail_key => $detail_value) {
                $detail_label = $detail_value['value'];
                $detail_label_original = $detail_value['value'];
                $detail_label = str_replace('–', '-', $detail_label);
                $detail_label = strtolower($detail_label);

                foreach ($display_label_original as &$label_value) {
                  $ori = $label_value;
                  $ori_tmp = explode('|', $ori);
                  $tmp = str_replace('–', '-', $label_value);
                  $tmp = strtolower($tmp);
                  $tmp2 = explode('|', $tmp);
                  if ($tmp2[0] == $detail_label) {
                    $translated_detail_label = $product_data[0]['value'][$key]['value'][$spec_key]['value'][$detail_key]['value'];
                    if ($detail_label_original && $translated_detail_label) {
                      $_SERVER['translate_label_product'][$ori_tmp[0]] = $translated_detail_label;
                      //$label_value = $ori_tmp[0]. '|'. $translated_detail_label;
                    }
                  }
                }
              }
            }
          }
        }
        foreach ($display_label_original as &$value) {
          $tmp = explode('|', $value);
          if (isset($_SERVER['translate_label_product'][$tmp[0]])) {
            $value = $tmp[0]. '|'. $_SERVER['translate_label_product'][$tmp[0]];
          }
        }
        $term->field_display_label = implode("\n", $display_label_original);
        $term->save();
      }
    }    
  }
  
  $message =  t('Updating label translation product ID: @product_id', ['@product_id' => $product_id]);
  $context['results'][] = $product_id;
  $context['message'] = $message;
}

function product_importer_rental_service($product_id, &$context) {
  $product = ProductService::productLoadByProductID($product_id);
  $product->field_rental_service = 'No';

  $update = TRUE;

  if ($update) {
    $product->save();
    $message =  t('Updating rental field product ID: @product_id', ['@product_id' => $product_id]);
  }
  $context['results'][] = $product_id;
  $context['message'] = $message;
}

function product_importer_update_sort($product_id, $product_path, &$context) {
  $product = ProductService::productLoadByProductID($product_id);  
  $update = TRUE;

  if ($update && $product) {
    $product_service = new ProductService();
    $product_data = $product_service->parseXML($product_path);
    $product_sort = $product_data[0]['attributes']['sort'];

    $product->field_sort = $product_sort;
    $product->save();
    $message =  t('Updating sort product ID: @product_id', ['@product_id' => $product_id]);
  }
  $context['results'][] = $product_id;
  $context['message'] = $message;
}

function product_importer_import_image($entity_id, &$context) {
  $update = TRUE;
  if ($update) {
    ProductService::importProductImage($entity_id);
    $message =  t('Importing entity ID: @entity_id', ['@entity_id' => $entity_id]);
  }
  $context['results'][] = $entity_id;
  $context['message'] = $message;
}

function product_importer_import_product_image_raw($tree_id, $group_id, $product_id, $product_path, $tree, &$context) {
  // Do heavy coding here...
  $product = ProductService::productLoadByProductID($product_id);
  $product_service = new ProductService();
  $product_service->setParentId($tree_id);
  $product_service->setProductId($product_id);
  $product_service->setTree($tree);

  $product_data = $product_service->parseXML($product_path);
  $product_data = $product_data[0]['value'];
  $update = TRUE;
  $message = '';

  foreach ($product_data as $product_raw_value) {
    $field_name = $product_raw_value['name'];
    $field_value = $product_raw_value['value'];
    $field_name = str_replace('{}', '', $field_name);

    switch ($field_name) {
      case 'listofimages':
        $data_list = [];

        foreach ($field_value as $v) {
          $type = $v['attributes']['type'];
          $image_url = $v['value'][0]['value'];
          $data_list[] = $type. '::' .$image_url;
        }

        $product->field_image_raw = $data_list;
        break;
    }
  }

  if ($update) {
    \Drupal::logger('product_importer')->notice($tree_id. ' : '. $product_id);
    \Drupal::logger('product_importer field_image_raw')->notice('<pre>'.print_r($data_list, true). '</pre>');
    $product->save();
    $message .=  t('Processing tree @tree_id : product ID: @product_id', ['@tree_id' => $tree_id, '@product_id' => $product_id]);
  }
  $context['results'][] = $product_id;
  $context['message'] = $message;
}

function product_importer_import_product($tree_id, $group_id, $product_id, $product_path, $tree, &$context) {
  $product_service = new ProductService();
  $product_service->setParentId($tree_id);
  $product_service->setProductId($product_id);
  $product_service->setTree($tree);

  $product_data = $product_service->parseXML($product_path);
  $update = TRUE;
  $message = '';
  if ($update) {
    \Drupal::logger('product_importer')->notice($tree_id. ' : '. $product_id);
    \Drupal::logger('product_importer tree')->notice('<pre>'.print_r($tree, true). '</pre>');
    $product_service->prepareData($product_data[0]);
    $product_service->createProduct();
    $message .=  t('Processing tree @tree_id : product ID: @product_id', ['@tree_id' => $tree_id, '@product_id' => $product_id]);
    drupal_unlink($product_path);
  }
  $context['results'][] = $product_id;  
  $context['message'] = $message;
}

function product_importer_translate_product($tree_id, $group_id, $product_id, $product_path, $tree, &$context) {
  $product_service = new ProductService();
  $product_service->setParentId($tree_id);
  $product_service->setProductId($product_id);
  $product_service->setTree($tree);

  $product_data = $product_service->parseXML($product_path);
  $update = TRUE;
  $message = '';
  if ($update) {
    \Drupal::logger('product_importer')->notice($tree_id. ' : '. $product_id);
    \Drupal::logger('product_importer tree')->notice('<pre>'.print_r($tree, true). '</pre>');
    $product_service->prepareData($product_data[0]);
    $product_service->translateProduct();
    $message .=  t('Processing tree @tree_id : product ID: @product_id', ['@tree_id' => $tree_id, '@product_id' => $product_id]);
  }
  $context['results'][] = $product_id;  
  $context['message'] = $message;
}

function product_importer_import_product_finished_callback($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $message = \Drupal::translation()->formatPlural(
      count($results),
      'One product processed.', '@count products processed.'
    );
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  //$_SESSION['disc_migrate_batch_results'] = $results;
}