<?php
use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\product_importer\Service\ProductService;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\field_collection\Plugin\Field\FieldType\FieldCollection;

$autoloader = require_once 'autoload.php';

$kernel = new DrupalKernel('prod', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$node = node_load(444);
$node->moderation_state->target_id = 'published';
$node->revision = 1;
$node->status = 1;
$node->save();

$node_arr = $node->toArray();
//$published_state = $node->moderation_state->entity->isPublishedState();

print_r($node_arr);
exit();