<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\product_importer\Service\ProductService;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\field_collection\Plugin\Field\FieldType\FieldCollection;

$autoloader = require_once 'autoload.php';

$kernel = new DrupalKernel('prod', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$page = (isset($_GET['page'])) ? $_GET['page'] : 0;

$import_url = 'http://dev.trakindo.labs.skyshi.com/cat_lift_exporter.php?page='. $page;

try {
  $response2 = \Drupal::httpClient()->get($import_url, array('headers' => array('Accept' => 'application/json')));
  $body = (string) $response2->getBody();
  $body_obj = json_decode($body);
  if (count($body_obj)) {
    $host = \Drupal::request()->getHost();
    $page++;
    $redirect_url = "http://$host/cat_lift_importer.php?page=$page";
    //print_r($body);
    print 'processing';
    print '<meta http-equiv="refresh" content="0; url='. $redirect_url. '" />';
  } else {
    print 'done';
  }
  
  foreach ($body_obj as $data) {
    $category = $data->field_category;
    $group_vid = 'group';
    $category_tid = ProductService::saveTerm($group_vid, $category);

    $listofimages = [];
    foreach ($data->field_listofimages as $url) {
      $file = ProductService::saveImage($url, 'public://product_images');
      $listofimages[] = $file->id();
    }
    

    $product_data = [
      'type' => $data->type,
      'name' => $data->name,
      'field_model' => $data->field_model,
      'field_category' => $category_tid,
      'field_rental_service' => $data->field_rental_service,
      'field_description' => [
        'value' => $data->field_description,
        'format' => 'full_html'
      ],
      'field_listofimages' => $listofimages,
    ];

    $query = \Drupal::entityQuery('products')
      ->condition('type', 'cat_lift_trucks')
      ->condition('name', $product_data['name']);
    $entity_ids = $query->execute();
    if ($entity_ids) {
      $id = reset($entity_ids);
      $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
      foreach ($product_data as $field => $value) {
        if ($field == 'type') continue;
        $entity->{$field} = $value;
      }
    } else {
      $entity = \Drupal::entityTypeManager()->getStorage('products')->create($product_data);
      $entity->save();
    }


    $entity->field_fork_lift_specification = [];
    foreach ($data->field_fork_lift_specification as $fc_data) {
      $fieldCollectionItem = FieldCollectionItem::create(['field_name' => 'field_fork_lift_specification']);
      $fieldCollectionItem->setHostEntity($entity);
      $fieldCollectionItem->set('field_spec_category', $fc_data->field_spec_category);
      $fieldCollectionItem->save();

      $entity->field_fork_lift_specification->appendItem($fieldCollectionItem);

      foreach ($fc_data->field_fork_lift_spec_detail as $fc_data2) {
        $fieldCollectionItem_child = FieldCollectionItem::create(['field_name' => 'field_fork_lift_spec_detail']);
        $fieldCollectionItem_child->setHostEntity($fieldCollectionItem);
        $fieldCollectionItem_child->set('field_detail_spec_label', $fc_data2->field_detail_spec_label);
        $fieldCollectionItem_child->set('field_detail_value_spec', $fc_data2->field_detail_value_spec);
        $fieldCollectionItem_child->save();
        $fieldCollectionItem->field_fork_lift_spec_detail->appendItem($fieldCollectionItem_child);
      }
    }
    $entity->field_list_description = [];
    foreach ($data->field_list_description as $fc_data) {
      $fieldCollectionItem = FieldCollectionItem::create(['field_name' => 'field_list_description']);
      $fieldCollectionItem->setHostEntity($entity);
      $fieldCollectionItem->set('field_label', $fc_data->field_label);
      $fieldCollectionItem->set('field_value_data', $fc_data->field_value_data);
      $fieldCollectionItem->save();

      $entity->field_list_description->appendItem($fieldCollectionItem);
    }
    $entity->save();
    print '<br/>'. $entity->id(). '<br/>';
  }
}
catch (RequestException $e) {
  return FALSE;
}

exit();