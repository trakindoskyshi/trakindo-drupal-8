<?php

/**
 * @file
 * The PHP page that serves all page requests on a Drupal installation.
 *
 * All Drupal code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt files in the "core" directory.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$autoloader = require_once 'autoload.php';

$kernel = new DrupalKernel('prod', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);

$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
$length = 2;
$start = $length * $page;
$query = \Drupal::entityQuery('products')
  ->condition('type', 'cat_lift_trucks')
  ->range($start, $length);
$entity_ids = $query->execute();

$outputs = [];
foreach ($entity_ids as $id) {
  $entity = \Drupal::entityTypeManager()->getStorage('products')->load($id);
  $entity_arr = $entity->toArray();
  $output = [];
  
  $field_fork_lift_specification = $entity_arr['field_fork_lift_specification'];
  $fork_lift_specification = [];
  foreach ($field_fork_lift_specification as $value) {
    $target_id = $value['target_id'];
    $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($target_id);
    $fc_arr = $fc->toArray();
    $field_spec_category = $fc_arr['field_spec_category'][0]['value'];

    $fc_details = [];
    
    foreach ($fc_arr['field_fork_lift_spec_detail'] as $fc_val) {
      $target_id = $fc_val['target_id'];
      $fc_detail = \Drupal\field_collection\Entity\FieldCollectionItem::load($target_id);
      $fc_detail_arr = $fc_detail->toArray();
      $fc_details[] = [
        'field_detail_spec_label' => $fc_detail_arr['field_detail_spec_label'][0]['value'],
        'field_detail_value_spec' => $fc_detail_arr['field_detail_value_spec'][0]['value'],
      ];
    }
    $fork_lift_specification[] = [
      'field_spec_category' => $field_spec_category,
      'field_fork_lift_spec_detail' => $fc_details,
    ];
  }

  $field_category = $entity_arr['field_category'][0]['target_id'];
  $category = taxonomy_term_load($field_category);

  $field_list_description = $entity_arr['field_list_description'];
  $list_description = [];
  foreach ($field_list_description as $value) {
    $target_id = $value['target_id'];
    $fc = \Drupal\field_collection\Entity\FieldCollectionItem::load($target_id);
    $fc_arr = $fc->toArray();
    $list_description[] = [
      'field_label' => $fc_arr['field_label'][0]['value'],
      'field_value_data' => $fc_arr['field_value_data'][0]['value']
    ];
  }

  $field_listofimages = $entity_arr['field_listofimages'];
  $listofimages = [];
  foreach ($field_listofimages as $value) {
    $target_id = $value['target_id'];
    $file = file_load($target_id);
    $listofimages[] = file_create_url($file->uri->value);
  }

  $output = [
    'type' => $entity_arr['type'][0]['target_id'],
    'name' => $entity_arr['name'][0]['value'],
    'field_fork_lift_specification' => $fork_lift_specification,
    'field_category' => $category->name->value,
    'field_description' => $entity_arr['field_description'][0]['value'],
    'field_listofimages' => $listofimages,
    'field_list_description' => $list_description,
    'field_model' => $entity_arr['field_model'][0]['value'],
    'field_rental_service' => $entity_arr['field_rental_service'][0]['value'],
  ];
  $outputs[] = $output;
}
header('Content-Type: application/json');

$return = json_encode($outputs);
print $return;
exit();