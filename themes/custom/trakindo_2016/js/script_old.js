(function($) {
  $('.js-home-slider, .js-post-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });
  $('.js-training-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });
  $('.js-product-slider').owlCarousel({
    nav: false,
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
  });

    //slide menu on click
    $('button.btn-menu-mobile').click(function(){
      $('body').toggleClass('slide');
      $('header').toggleClass('slide');
      $('#dl-menu').toggleClass('slide');
      $('button.dl-trigger').trigger("click");
      return false;
    });
    $(window).click(function() {
      $('body').removeClass('slide');
      $('header').removeClass('slide');
      $('#dl-menu').removeClass('slide');
    });

    //menu mobile
    $( '#dl-menu' ).dlmenu({
      animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
    });

    // $('.btn-menu-mobile').click(function(){
    //   $('body').toggleClass('slide');
    //   $('#mobileMenu').toggleClass('slide');

    //   // var x = $('#mobileMenu');
    //   // if($('body').attr('class') === "front slide") {
    //   //   $(document).not(x).click(function(){
    //   //   console.log(x);
    //   //       $('body').removeClass('slide');
    //   //       $('#mobileMenu').removeClass('slide');
    //   //   });
    //   // }
    //  return false;
    // });
    
    //padding top body
    // $(window).load(function(){
    // var top = $('header').height();
    //   $('body').css('padding-top', top);
    //   $(window).resize(function(){
    //     $('body').css('padding-top', top);
    //   });
    // });

    // Filter Product Checkbox
    // $("[data-attr=all]").click(function(){
    // 	var className = $(this).attr("class");
    // 	if( $(this).is(":checked") == true ){
    // 		$("." + className).prop("checked", true);
    // 		$("." + className).parent().addClass("active");
    // 	}
    // 	else {
    // 		$("." + className).prop("checked", false);
    // 		$("." + className).parent().removeClass("active");
    // 	}
    // });
    $("[data-attr=all]").click(function(){
        if( $(this).is(":checked") == true ){
            $(this).parent().parent().find('.facet-item ').addClass('active');
            $(this).parent().parent().find('.facet-item ').find('input').prop("checked", true);
        } 
        else {
            $(this).parent().parent().find('.facet-item ').removeClass('active');
            $(this).parent().parent().find('.facet-item ').find('input').prop("checked", false);
        }
    });
    $(".filter .facet-item label").click(function(){
       var className = $(this).parent().parent()
        $(this).parent('.facet-item').toggleClass("active");
        $(this).parent('.facet-item ').find('input').trigger('click');
    });
    $(".filter .facet-item input").change(function(){
    	if( $(this).is(":checked") == true ){
    		$(this).parent().addClass("active");
    	} 
    	else {
    		$(this).parent().removeClass("active");
    	}
    });
    $(".product-item").hover(function(){
    	$(this).addClass("active");
    	$(this).find(".btn-primary").removeClass("hidden");
    }, function(){
    	$(this).removeClass("active");
    	$(this).find(".btn-primary").addClass("hidden");
    });

    $(window).scroll(function( e ){
      $(".right-contact").stop().animate({
        "marginTop": ($(window).scrollTop()) + "px", 
        "marginLeft":($(window).scrollLeft()) + "px"
      }, "slow" );
    });
    // var totaltext = "";
    // for (var i = 0; i < 100; i++) {
    //   totaltext += "scroll!<br />";
    // }
    //$("#div2").html(totaltext);
    topRightContact();
    feauredWidth();
    slideFeatured(); 
    $(window).load(function(){
      topRightContact();
      feauredWidth();
      slideFeatured();
    });
    $(window).resize(function(){
      topRightContact();
      feauredWidth();
      slideFeatured();
    });

    // scroll btn scroll
    $(window).scroll(function() {
        var dhdr = $('.detail-header').outerHeight() + 300;
        var dth = $('.detail-menu');
        var mns = "main-nav-scrolled";

      if ($(window).scrollTop() >= 50 ) {

        $('.landing-scroll').fadeOut();

      } else {
        $('.landing-scroll').fadeIn();
      }

     if ( $(this).scrollTop() >= dhdr) {
        $('.prod-header').fadeOut();
        dth.addClass(mns);
       //return;
     } else {
        $('.prod-header').fadeIn();
         dth.removeClass(mns);
        //return;
     }
    });
    function topRightContact() {
      var winHeight = $(window).height();
      var rcHeight = $(".right-contact").height();
      var top = winHeight - rcHeight;
      $(".right-contact").css('top', top + 1);
    }

    function feauredWidth() {
      var winWidth = $(window).width();
      var fWidth = $(".right-contact").width();
      var width = winWidth - fWidth;
      $(".featured").css('width', width);
    }

    function slideFeatured() {
      if($(window).width() <= 1024 && $(window).width() >= 768) {
        $('#featuredSlider').addClass('js-featured-slider');
        $('.js-featured-slider').owlCarousel({
          nav: false,
          items:3,
          loop:true,
          autoplay:false,
          autoplayTimeout:3000,
          autoplayHoverPause:true,
          responsive:{
            1024:{
              items:3,
              loop:false
            },
            767:{
              items:3,
            }
          }
        });
      }
      if($(window).width() <= 767) {
        // $('.js-featured-slider').data('owlCarousel').destroy();
      }
      if($(window).width() >= 1025) {
        $('#featuredSlider').removeClass('js-featured-slider');
      }
      return;
    }

    $(".call-center a").hover(function(){
        $(".popup-call-center").toggle("slow");
    }, function(){
        // $(".popup-call-center").toggle("slow");
    });
    $("body").click(function(){
      if( $(".popup-call-center").is(":visible") ) {
        $(".popup-call-center").toggle("slow");
      }
    });

    var searchWidth = $(".search-text").width();
    $("#btn-search-close").click(function(){
      $(".search-text").animate({
        "width": "0px",
      },"fast");
      $(".search-box").hide();
    });

    $("#btn-search").click(function(){
      $(".search-box").show();
      $(".search-text").animate({
        "width": searchWidth + "%",
      },"fast");
    });

  })(jQuery);

