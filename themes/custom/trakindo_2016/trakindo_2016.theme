<?php
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Render\Markup;

function trakindo_2016_preprocess_views_view_field(&$variables) {

  if ($variables['field']->field == 'nothing') {
    $variables['output'] = Markup::create(html_entity_decode($variables['output']));
  }
}

function trakindo_2016_preprocess_html(&$variables) {
    // dinamic META description for static pages, landing pages, landing pages custom
    $node = \Drupal::routeMatch()->getParameter('node');// Load the node entity from current route
    if ($node) {
        if ($node->getType() === 'pages' || $node->getType() === 'pages_corporate') {
          if($node->get('field_content_top_yellow')->value) {
            $desc = $node->get('field_content_top_yellow')->value;
          } else if($node->get('field_content_top_grey')->value) {
            $desc = $node->get('field_content_top_grey')->value;
          } else if($node->get('field_content_top_bg')->value) {
            $desc = $node->get('field_content_top_bg')->value;
          } else {
            $desc = $node->get('body')->value;
          }
          
          $strip_tags = strip_tags($desc);
          if(strlen($strip_tags) > 150) {
            $content = substr($strip_tags, 0, strpos($strip_tags, ' ', 150));
          }
          else {
            $content = $strip_tags;
          }

          $description = [
              '#tag' => 'meta',
              '#attributes' => [
                  'name' => 'description',
                  'content' => $content,
              ],
          ];
          $variables['page']['#attached']['html_head'][] = [$description, 'description'];
        }
        if ($node->getType() === 'landing_pages' || $node->getType() === 'landing_pages_marketing') {
          if($node->get('field_second_text')->value) {
            $desc = $node->get('field_second_text')->value;
            if($node->get('field_landing_short_description')->value) {
              $desc2 = strip_tags($node->get('field_landing_short_description')->value);
              $desc = $desc.'. '.$desc2;
            }
          }

          $strip_tags = strip_tags($desc);
          if(strlen($strip_tags) > 150) {
            $content = substr($strip_tags, 0, strpos($strip_tags, ' ', 150));
          }
          else {
            $content = $strip_tags;
          }

          $description = [
              '#tag' => 'meta',
              '#attributes' => [
                  'name' => 'description',
                  'content' => $content,
              ],
          ];
          $variables['page']['#attached']['html_head'][] = [$description, 'description'];
        }
        if ($node->getType() === 'pages_custom' || $node->getType() === 'pages_custom_marketing') {
          if($node->get('field_second_text')->value) {
            $desc = $node->get('field_second_text')->value;
            if($node->get('field_short_description')->value) {
              $desc2 = strip_tags($node->get('field_short_description')->value);
              $desc = $desc.'. '.$desc2;
            }
          }

          $strip_tags = strip_tags($desc);
          if(strlen($strip_tags) > 150) {
            $content = substr($strip_tags, 0, strpos($strip_tags, ' ', 150));
          }
          else {
            $content = $strip_tags;
          }

          $description = [
              '#tag' => 'meta',
              '#attributes' => [
                  'name' => 'description',
                  'content' => $content,
              ],
          ];
          $variables['page']['#attached']['html_head'][] = [$description, 'description'];
        }
    }
}

function trakindo_2016_preprocess_page(&$variables){
  $variables['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
  // if ($node = \Drupal::routeMatch()->getParameter('node')) {
  //   if (is_string($node)) {
  //     $node = \Drupal\node\Entity\Node::load($node);
  //   }
  // }
  $current_path = \Drupal::service('path.current')->getPath();
  $alias = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
  $types = explode('/', $alias);
  $variables['alias'] = $types;

  if(isset($types) && $types[1] == 'product-detail') {
    $variables['#attached']['library'][] = 'trakindo_2016/shareThis';
  }

  if(isset($types) && isset($types[2])) {
    if(($types[1] == 'about-us' || $types[1] == 'tentang-kami') && ($types[2] == 'history' || $types[2] == 'sejarah')) {
      $variables['#attached']['library'][] = 'trakindo_2016/flexslider';
    }
    if(($types[1] == 'about-us' || $types[1] == 'tentang-kami') && ($types[2] == 'management' || $types[2] == 'manajemen')) {
      $variables['#attached']['library'][] = 'trakindo_2016/imageMaps';
    }
  }
}

function trakindo_2016_preprocess_node(&$variables){
  $variables['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['node_id'] = $variables['node']->id();

  // Getting the node creation time stamp from the node object.
  $date = $variables['node']->getCreatedTime();
  // Here you can use drupal's format_date() function, or some custom PHP date formatting.
  $variables['formatted_date'] = date('j M Y', $date);
}

function trakindo_2016_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  $alias = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
  $types = explode('/', $alias);
  $type = str_replace('-', '_', $types[1]);
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    if (is_string($node)) {
      $node = \Drupal\node\Entity\Node::load($node);
      $content_type = $node->bundle();
    } else {
      $content_type = $node->bundle();
    }
    if($content_type) {
      $suggestions[] = 'page__'.$content_type;
    }
    if(isset($types) && isset($types[2])) {
      if($types[2] == 'management' || $types[2] == 'manajemen') {
        $suggestions[] = 'page__management';
      }
      if($types[2] == 'career-opportunities' || $types[2] == 'kesempatan-berkarir') {
        $suggestions[] = 'page__career_opportunities';
      }
    }
  }
}

function trakindo_2016_theme_suggestions_node_alter(array &$suggestions, array $variables) {
    $current_path = \Drupal::service('path.current')->getPath();
    $alias = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
    $types = explode('/', $alias);
    $type = str_replace('-', '_', $types[1]);
    if(($types[1] == 'about-us' || $types[1] == 'tentang-kami') && empty($types[2])) {
      $suggestions[] = 'node__about_us';
    }
    if(isset($types) && isset($types[2])) {
      if($types[2] == 'management' || $types[2] == 'manajemen') {
        $suggestions[] = 'node__management';
      }
      if($types[2] == 'career-opportunities' || $types[2] == 'kesempatan-berkarir') {
        $suggestions[] = 'node__career_opportunities';
      }
    }
    
}

function trakindo_2016_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  $types = explode('/', $current_path);
  $type = str_replace('-', '_', $types[1]);
    $suggestions[] = 'form__'.$type;
}

function trakindo_2016_theme_suggestions_table_alter(array &$suggestions, array $variables) {
	$current_path = \Drupal::service('path.current')->getPath();
	$types = explode('/', $current_path);
	$type = str_replace('-', '_', $types[1]);
    $suggestions[] = 'table__'.$type;
}

function trakindo_2016_theme_suggestions_input__radio_alter(array &$suggestions, array $variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  $types = explode('/', $current_path);
  $type = str_replace('-', '_', $types[1]);
    $suggestions[] = 'input_radio__'.$type;
}

function trakindo_2016_theme_suggestions_item_list_alter(array &$suggestions, array $variables) {
	$current_path = \Drupal::service('path.current')->getPath();
	$types = explode('/', $current_path);
	$type = str_replace('-', '_', $types[1]);
  if(isset($types) && isset($types[2]) && $types[2] == 'location' ) {
    $type = $type.'-'.$types[2];
  }
  $suggestions[] = 'item_list__'.$type;
}

function trakindo_2016_preprocess_superfish_menu_items(array &$variables) {
  $element = $variables['element'];

  // Keep $sfsettings untouched as we need to pass it to the child menus.
  $settings = $sfsettings = $element['#settings'];
  $multicolumn = $multicolumn_below = $settings['multicolumn'];

  $variables['menu_items'] = array();

  $menu = $element['#tree'];

  // sfTouchscreen.
  // Adding cloned parent to the sub-menu tree.
  // Note, it is always false if it's not a sub-menu.
  if ($element['#cloned_parent'] !== FALSE) {
    array_unshift($menu, $element['#cloned_parent']);
  }

  $active_trails = \Drupal::service('menu.active_trail')->getActiveTrailIds($element['#menu_name']);

  foreach ($menu as $key => $menu_item) {

    if (null !== $menu_item->link && !($menu_item->link instanceof InaccessibleMenuLink)) {

      $item_class = $link_class = array();
      $multicolumn_wrapper = $multicolumn_column = $multicolumn_content = FALSE;
      // Menu link properties.
      $link = $menu_item->link->getPluginDefinition();
      if(strpos($menu_item->link->getTitle(),'®') !== false) {
        //$title = str_replace('®', '<sup>®</sup>', $menu_item->link->getTitle());
        $titles = explode('®',$menu_item->link->getTitle());
        $title_text = '@text1<sup>®</sup>@text2';
        $title_text_replace = array('@text1' => $titles[0], '@text2' => $titles[1]);
        $title = SafeMarkup::format($title_text, $title_text_replace);
      }
      else {
        $title = $menu_item->link->getTitle();
      }
      //print_r($menu_item->link->getUrlObject());
      $item = array(
        'id'            => $link['id'],
        'text'          => $title,
        'description'   => $menu_item->link->getDescription(),
        'url'           => $menu_item->link->getUrlObject(),
        'enabled'       => $link['enabled'],
        'expanded'      => $sfsettings['expanded'] ? $link['expanded'] : TRUE,
        'options'       => $link['options'],
        'subtree'       => $menu_item->subtree,
        'depth'         => $menu_item->depth,
        'hasChildren'   => $menu_item->hasChildren,
        'inActiveTrail' => $menu_item->inActiveTrail
      );

      if (!isset($item['options']['external'])) {
        // Adding the "is-active" class.
        $host = \Drupal::request()->getHttpHost();
        $request_uri = \Drupal::request()->getRequestUri();
        $current_url = Url::fromRoute('<current>');
        $current_path = $current_url->toString();
        $link_url = $item['url']->toString();
        if ($link_url == $current_path || $link_url == $request_uri || $link_url == $host . $request_uri) {
          $link_class[] = 'is-active';
        }

        // Adding the necessary "active-trail" class.
        if (($menu_item->link->getUrlObject()->getRouteName() == '<front>' && \Drupal::service('path.matcher')->isFrontPage()) || array_key_exists($item['id'], $active_trails) || $item['inActiveTrail']) {
          $item_class[] = 'active-trail';
        }
      }
      $link_url = $item['url']->toString();
      if (strpos($link_url,'http') !== false) {
        $item['options']['attributes'] = array('target' => '_blank');
      }

      // Add menu link depth classes to the <li> element and its link.
      $link_class[] = $settings['itemdepth'] ? 'sf-depth-' . $item['depth'] : '';
      $item_class[] = $settings['itemdepth'] ? 'sf-depth-' . $item['depth'] : '';
      // Indicates a cloned parent link, i.e. does not exist in the actual menu tree.
      $item_class[] = $element['#cloned_parent'] ? 'sf-clone-parent' : '';

      // Adding custom <li> classes.
      if (strpos($settings['liclass'], ' ') !== FALSE) {
        $l = explode(' ', $settings['liclass']);
        foreach ($l as $c) {
          $item_class[] = Html::cleanCssIdentifier($c);
        }
      }
      else {
        $item_class[] = Html::cleanCssIdentifier($settings['liclass']);
      }

      // Adding custom link classes.
      if (strpos($settings['hlclass'], ' ') !== FALSE) {
        $l = explode(' ', $settings['hlclass']);
        foreach ($l as $c) {
          $link_class[] = Html::cleanCssIdentifier($c);
        }
      }
      else {
        $link_class[] = Html::cleanCssIdentifier($settings['hlclass']);
      }

      // Add a class to external links.
      $link_class[] = isset($item['options']['external']) ? 'sf-external' : '';

      // Inserting link description (the "title" attribute) into the text.
      if ($settings['add_linkdescription'] && !empty($item['description'])) {
        $link_text = '@text <span class="sf-description">@description</span>';
        $link_text_replace = array('@text' => $item['text'], '@description' => $item['description']);
      } else {
        $link_text = '@text';
        $link_text_replace = array('@text' => $item['text']);
      }
       $item['options']['attributes']['title'] = $item['text'];

      // Sub-menu.
      if ($item['hasChildren'] && $item['subtree'] && $item['expanded']) {

        // Multi-column sub-menus.
        if ($settings['multicolumn']) {
          $multicolumn_wrapper = ($item['depth'] == $settings['multicolumn_depth']) ? TRUE : FALSE;
          $multicolumn_column = ($item['depth'] == $settings['multicolumn_depth'] + 1) ? TRUE : FALSE;
          $multicolumn_content = ($item['depth'] >= $settings['multicolumn_depth'] && $item['depth'] <= $settings['multicolumn_levels']) ? TRUE : FALSE;
        }

        // sfTouchscreen.
        // Preparing the cloned parent links to be added to the sub-menus.
        if ($settings['clone_parent'] && $item['subtree']) {
          $cloned_parent = $menu_item;
          $cloned_parent->subtree = [];
        }
        else {
          $cloned_parent = FALSE;
        }

        // Render the sub-menu.
        $children = array(
          '#theme'         => 'superfish_menu_items',
          '#menu_name'     => $element['#menu_name'],
          '#tree'          => $item['subtree'],
          '#settings'      => $sfsettings,
          '#cloned_parent' => $cloned_parent
        );

        if ($item['subtree']) {
          // Adding some more classes.
          $item_class[] = $multicolumn_column ? 'sf-multicolumn-column' : '';
          $item_class[] = $link_class[] = 'menuparent';
        }
      }
      else {
        $children = '';
        $item_class[] = 'sf-no-children';
      }

      // Preparing <li> classes for the theme.
      $item_class = implode(' ', superfish_array_remove_empty($item_class));

      // Merging link classes.
      if (isset($item['options']['attributes']['class'])) {
        $link_class_current = $item['options']['attributes']['class'];
        if (!is_array($link_class_current)) {
          $link_class_current = array($link_class_current);
        }
        $link_class = array_merge($link_class_current, superfish_array_remove_empty($link_class));
      }
      $item['options']['attributes']['class'] = superfish_array_remove_empty($link_class);

      // Dirty fix! to only add a "menuparent" class.
      $item['options_menuparent'] = $item['options'];
      $item['options_menuparent']['attributes']['class'][] = 'menuparent';
      $link_element = array(
        '#type' => 'link',
        '#title' => SafeMarkup::format($link_text, $link_text_replace),
        '#url' => $item['url'],
        '#options' => $item['options']
      );
      $link_element_menuparent = array(
        '#type' => 'link',
        '#title' => SafeMarkup::format($link_text, $link_text_replace),
        '#url' => $item['url'],
        '#options' => $item['options_menuparent']
      );

      $variables['menu_items'][] = array(
        'id'                  => Html::getUniqueId($element['#menu_name'] .'-'. $item['id']),
        'item_class'          => $item_class,
        'link'                => $link_element,
        'link_menuparent'     => $link_element_menuparent,
        'children'            => $children,
        'multicolumn_wrapper' => $multicolumn_wrapper,
        'multicolumn_content' => $multicolumn_content,
        'multicolumn_column'  => $multicolumn_column
      );
    }
  }
}
function trakindo_2016_preprocess_input(&$variables) {
  $element = $variables['element'];
  // Remove name attribute if empty, for W3C compliance.
  if (isset($variables['attributes']['name']) && empty((string) $variables['attributes']['name'])) {
    unset($variables['attributes']['name']);
  }
  if($element['#name'] == 'files[field_cv_0]') {
    $variables['element']['#required'] = TRUE;
    $variables['element']['#attributes']['class'][] = 'testaa';
  }
  $variables['children'] = $element['#children'];
}
?>
