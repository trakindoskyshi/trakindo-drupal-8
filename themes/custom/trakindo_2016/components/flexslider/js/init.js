(function($) {
  // main slider
  $('.js-main-history-slide').flexslider({
    animation: 'slide',
    slideshow: false,
    startAt: 4,
    manualControls: '.js-main-history-pager li'
  });


  if( $(window).width() <= 768 ) {
    $(".js-main-history-pager").owlCarousel({
      items: 3,
      navigation: true,
      navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
    });
    $(".owl-nav .owl-prev").html("<i class='fa fa-angle-left'></i>");
    $(".owl-nav .owl-next").html("<i class='fa fa-angle-right'></i>");
  }

  // slider for every decades
  if( $(window).width() > 768 ) {
    $(document).ready(function() {
      var carouselCount = 0;
      $(".js-decade-slide").each(function () {
        $(this).attr("id", "js-decade-slide-" + carouselCount);
        $('#js-decade-slide-' + carouselCount).owlCarousel({
          items: 1,
          dots: false,
         URLhashListener: true,
          mouseDrag: false,
          touchDrag: false,
          autoWidth: true,
          startPosition: '6'
        });
        carouselCount++;
      });
    });
  }

  $('.js-decade-pager li a').on('click', function(){
    $('.js-decade-pager li a.active').removeClass('active');
    $(this).addClass('active');
  });
})(jQuery);
