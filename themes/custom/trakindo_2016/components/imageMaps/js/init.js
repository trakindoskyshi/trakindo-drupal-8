(function($) {
	$(document).ready(function(e) {
		$('img[usemap]').rwdImageMaps();
		
		$('area').on('mouseover', function() {
			var map_id = $(this).attr('id');
			$(".management-name-container div#" + map_id).show();
		});
		$('area').on('mouseleave', function() {
			var map_id = $(this).attr('id');
			$(".management-name-container div#" + map_id).hide();
		});
		$('#managementMap area').bind('click', function(event) {
	       var anchor = $(this);
	       $('html, body').stop().animate({
	           scrollTop: ($(anchor.attr('href')).offset().top - 150)
	       }, 1000, 'linear');
	   });
	});
})(jQuery);
